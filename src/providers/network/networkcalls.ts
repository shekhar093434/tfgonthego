import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { Constants } from '../../utilities/constants';
import { Network } from '@ionic-native/network';
import { Utills } from '../../utilities/common';

/*
  Generated class for the NetworkProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NetworkProvider {

  constructor(public constants: Constants, public http: Http, public network: Network, public utils: Utills) {
    //console.log('Hello NetworkProvider Provider');
  }

  postData(data, type) {
    //console.log('Data : '+JSON.stringify(data));
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Accept', 'application/json');
      headers.append('X-Requested-With', 'com.zensar.zencmo');
      this.http.post(this.constants.DEVBASEURL + type, JSON.stringify(data), { headers: headers }).
        subscribe(res => {
          resolve(res.json());
        }, (err) => {
          reject(err);
        });
    });

  }

  getAuthToken() {
    return new Promise((resolve, reject) => {
      this.http.get(this.constants.TOKEN_URL).
        subscribe(res => {
          //console.log('Auth Data : '+JSON.stringify(res));
          resolve(res.json());
        }, (err) => {
          //console.log('Error : '+JSON.stringify(err));
          reject(err);
        });
    });
  }

  getSecretKey() {
    return new Promise((resolve, reject) => {
      this.http.get(this.constants.SECRETKEY_URL).
        subscribe(res => {
          //console.log('Secret key Data : '+JSON.stringify(res));
          resolve(res.json());
        }, (err) => {
          //console.log('Error : '+JSON.stringify(err));
          reject(err);
        });
    });
  }

  authenticateUser(data, authToken) {
    //console.log('Data : '+JSON.stringify(data));
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Accept', 'application/json');
      headers.append('Authorization', authToken);
      headers.append('Ocp-Apim-Subscription-Key', 'beb3691c5aa449338085e3b055cfd2e7');
      this.http.post(this.constants.LOGINURL, JSON.stringify(data), { headers: headers }).
        subscribe(res => {
          resolve(res.json());
        }, (err) => {
          reject(err);
        });
    });
  }

  getPayslipStaticData() {
    return new Promise((resolve, reject) => {
      this.http.get('assets/data/payslipdata.json').subscribe(res => {
        //console.log('Auth Data : '+JSON.stringify(res));
        resolve(res.json());
      }, (err) => {
        console.log('Error : ' + JSON.stringify(err));
        reject(err);
      });
    });
  }

  getProfileStaticData() {
    return new Promise((resolve, reject) => {
      this.http.get('assets/data/profiledata.json').subscribe(res => {
        //console.log('Profile Data : '+JSON.stringify(res));
        resolve(res.json());
      }, (err) => {
        console.log('Error : ' + JSON.stringify(err));
        reject(err);
      });
    });
  }

  getPayslipData(data, authToken) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Accept', 'application/json');
      headers.append('Authorization', authToken);
      headers.append('Ocp-Apim-Subscription-Key', 'beb3691c5aa449338085e3b055cfd2e7');
      //console.log("Payslip List Header"+JSON.stringify(headers));
      //console.log("Payslip List Body"+JSON.stringify(data));
      this.http.post(this.constants.GET_PAYSLIP_DATA_URL, JSON.stringify(data), { headers: headers }).
        subscribe(res => {
          //console.log("Payslip List Data"+JSON.stringify(res));
          resolve(res.json());
        }, (err) => {
          console.log("Payslip List Error" + JSON.stringify(err));
          reject(err);
        });
    });
  }

  getProfileDataUsingPost(data, authToken) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Accept', 'application/json');
      headers.append('Authorization', authToken);
      headers.append('Ocp-Apim-Subscription-Key', 'beb3691c5aa449338085e3b055cfd2e7');
      this.http.post(this.constants.GET_PROFILE_DATA_POST_URL, JSON.stringify(data), { headers: headers }).
        subscribe(res => {
          resolve(res.json());
        }, (err) => {
          reject(err);
        });
    });
  }

  getPersonID(data, authToken) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Accept', 'application/json');
      headers.append('Authorization', authToken);
      headers.append('Ocp-Apim-Subscription-Key', 'beb3691c5aa449338085e3b055cfd2e7');
      this.http.post(this.constants.GET_PERSON_ID_URL, JSON.stringify(data), { headers: headers }).
        subscribe(res => {
          resolve(res.json());
        }, (err) => {
          reject(err);
        });
    });
  }

  getProfileData(personId, authToken) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      //headers.append('Content-Type', 'application/json');
      //headers.append('Accept', 'application/json');
      headers.set('Authorization', authToken);
      //headers.append('Ocp-Apim-Subscription-Key', 'beb3691c5aa449338085e3b055cfd2e7');
      var url = this.constants.GET_PROFILE_DATA_URL + "getPersonDirectoryDetails/?subscription-key=beb3691c5aa449338085e3b055cfd2e7&personId=" + personId + "&ctx_responsibility=GLOBAL_HRMS_MANAGER&ctx_respapplication=PER&ctx_securitygroup=STANDARD&ctx_nlslanguage=AMERICAN&ctx_language=AMERICAN&ctx_orgid=81";
      this.http.get(url, { headers }).
        subscribe(res => {
          //console.log('Profile Data : '+JSON.stringify(res));
          resolve(res.json());
        }, (err) => {
          //console.log('Profile Data Error : '+JSON.stringify(err));
          reject(err);
        });
    });
  }

  getPayslipBlobData(data, authToken) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Accept', 'application/json');
      headers.append('Authorization', authToken);
      headers.append('Ocp-Apim-Subscription-Key', 'beb3691c5aa449338085e3b055cfd2e7');
      this.http.post(this.constants.GET_PAYSLIP_PDF_URL, JSON.stringify(data), { headers: headers }).
        subscribe(res => {
          resolve(res.json());
        }, (err) => {
          reject(err);
        });
    });
  }

  postResponse(data, type) {
    //console.log('Data : '+data);
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Accept', 'application/json');
      headers.append('X-Requested-With', 'com.zensar.zencmo');
      this.http.post(this.constants.DEVBASEURL + type, data, { headers: headers }).
        subscribe(res => {
          resolve(res.json());
        }, (err) => {
          reject(err);
        });
    });

  }

  chatQuery(serviceUrl, data) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Accept', 'application/json');
      this.http.post(this.constants.CHATBOTURL + serviceUrl, data, { headers: headers })
        .subscribe(res => {
          if (res.status == 200) {
            resolve(res.json());
            // this.responsedata=JSON.stringify(res);
          } else if (res.status == 400) {
            this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.UNKNOWN_ERROR, this.constants.COMMON_APP_MESSAGE.ERROR);
          } else if (res.status == 500) {
            this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.SERVER_ERROR, this.constants.COMMON_APP_MESSAGE.ERROR);
          } else
            this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.SERVER_DATA_ERROR, this.constants.COMMON_APP_MESSAGE.ERROR);
        }, (err) => {
          reject(err);
        });
    });
  }

  isNetworkConnectionAvailable(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      try {
        if (this.network.type == "none") {
          resolve(false);
        }
        else {
          resolve(true);
        }
      }
      catch (e) {

        reject(e);
      }
    });
  }
  
  
  getLeaveBalances() {
    return new Promise((resolve, reject) => {
      this.http.get("http://demo8254660.mockable.io/leaves").
        subscribe(res => {
          console.log('Leave balances : ' + JSON.stringify(res));
          resolve(res.json());
        }, (err) => {
          console.log('Error : ' + JSON.stringify(err));
          reject(err);
        });
    });
  }

}
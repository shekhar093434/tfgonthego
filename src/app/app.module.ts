import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler, ViewController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
import { Login } from '../pages/login/login';
import { Signup } from '../pages/signup/signup';
import { HomePage } from '../pages/home/home';
import { ChatbotPage } from '../pages/chatbot/chatbot';
import { ProfilePage } from '../pages/profile/profile';
import { LeavesdetailsPage } from '../pages/leavesdetails/leavesdetails';
import { CalendarPage } from '../pages/calendar/calendar';
import { DurationCalPage } from '../pages/duration-cal/duration-cal';
import { ApplyLeavePage } from '../pages/apply-leave/apply-leave';
import { WorklistPage } from '../pages/worklist/worklist';
import {LeaveRequestPage} from '../pages/leave-request/leave-request';
import {SummarylistPage} from '../pages/summarylist/summarylist';
import { Network } from '@ionic-native/network';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NetworkProvider } from '../providers/network/networkcalls';
import { Constants } from '../utilities/constants';
import { Utills } from '../utilities/common';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { IonicStorageModule } from '@ionic/storage';
import { Crashlytics } from '@ionic-native/fabric';
import { Device } from '@ionic-native/device';
import { TouchID } from '@ionic-native/touch-id';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';
import { SpeechRecognition } from '@ionic-native/speech-recognition';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { TextToSpeech } from '@ionic-native/text-to-speech';
import { PayslipPage } from '../pages/payslip/payslip';
import { AppVersion } from '@ionic-native/app-version';
import { File } from '@ionic-native/file';
import { Keyboard } from '@ionic-native/keyboard';
import { FileOpener } from '@ionic-native/file-opener';
import { DatePickerModule } from 'ionic-calendar-date-picker';

@NgModule({
  declarations: [
    MyApp,
    Login,
    Signup,
    HomePage,
    ChatbotPage,
    PayslipPage,
    ProfilePage,
    LeavesdetailsPage,
    CalendarPage,
    DurationCalPage,
    ApplyLeavePage,
    WorklistPage,
    LeaveRequestPage,
    SummarylistPage
	
  ],
  imports: [
    BrowserModule,
    HttpModule,
    DatePickerModule,
    IonicModule.forRoot(MyApp, {swipeBackEnabled: false,scrollPadding: false,scrollAssist: false,autoFocusAssist: false}),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Login,
    Signup,
    HomePage,
    ChatbotPage,
    PayslipPage,
    ProfilePage,
    LeavesdetailsPage,
    CalendarPage,
    DurationCalPage,
    ApplyLeavePage,
    WorklistPage,
    LeaveRequestPage,
    SummarylistPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AppVersion,
    SpeechRecognition,
    AndroidPermissions,
    TextToSpeech,
    InAppBrowser,
    File,
    Keyboard,
    FileOpener,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    NetworkProvider, Constants, Utills, Network, ScreenOrientation, Crashlytics, Device, TouchID, FingerprintAIO
  ]
})
export class AppModule {}

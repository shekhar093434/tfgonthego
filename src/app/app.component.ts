import { Component, ViewChild } from '@angular/core';
import { Platform, ModalController, App, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NetworkProvider } from '../providers/network/networkcalls';
import { Utills } from '../utilities/common';
import { Device } from '@ionic-native/device';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Login } from '../pages/login/login';
import { HomePage } from '../pages/home/home';

@Component({
  templateUrl: 'app.html',
  providers: [NetworkProvider],
})
export class MyApp {
  rootPage:any;
  @ViewChild(Nav) nav: Nav;
  constructor(public platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, modalCtrl: ModalController, public utils: Utills,
                public device: Device, public screenorientation: ScreenOrientation) {
    /*if((localStorage.getItem('Authenticated') != null)){
      if(localStorage.getItem('Authenticated') === '1'){
        this.rootPage = HomePage;
      } else {
        this.rootPage = Login;
      }
    } else {
      this.rootPage = Login;
    }*/
    this.rootPage = Login;
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      if (this.platform.is('android')) {
        console.log("running on Android device!");
        console.log("Details- "+this.device.version+"--"+this.device.platform+"--"+this.screenorientation.type+"--"+this.platform.width()+"--"+this.platform.height())
      }
      if (this.platform.is('ios')) {
          console.log("running on iOS device!");
      }

      if (this.platform.is('tablet')) {
        console.log("running on tablet device!");
      }

      if (this.platform.is('ipad')) {
        console.log("running on ipad device!");
      } 

      let object = utils.updateDeviceDetails();
      this.utils.checkBiometricAvailable();
      this.utils.saveDeviceConfigurationToLocalStorage();
      localStorage.setItem("BiometricCancelled","0");
      /*platform.resume.subscribe(() => {
        this.utils.verifyFingerPrint();
      });*/ 

      platform.registerBackButtonAction(() => {
        let view = this.nav.getActive();
        if(view.component.name === 'Login'){
          platform.exitApp();
        }else if(view.component.name === 'HomePage'){
          platform.exitApp();
        }else if(view.component.name === 'ChatbotPage'){
          this.nav.pop({});
        }else if(view.component.name === 'PayslipPage'){
          this.nav.pop({});
        } else if(view.component.name === 'ProfilePage'){
          this.nav.pop({});
        } else if(view.component.name === 'LeavesdetailsPage'){
          this.nav.pop({});
        } else if(view.component.name === 'CalendarPage'){
          this.nav.pop({});
        } else if(view.component.name === 'ApplyLeavePage'){
          this.nav.pop({});
        }  else if(view.component.name === 'WorklistPage'){
          this.nav.pop({});
        } else if(view.component.name === 'Signup'){
          platform.exitApp();
        } else{
          this.nav.pop({});
        }
      },5);

    });
  }

}

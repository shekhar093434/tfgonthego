//Constants for getting type references
import { Injectable } from '@angular/core';

@Injectable()
export class Constants {
    constructor(){

    }
     /*
    *** Stage and production URL
     */
    private PORT_NO =''; //Regular Instance
    private PRODUCTION_URL=''+this.PORT_NO+'';
    private STAGE_URL='';
    public DEVBASEURL=this.STAGE_URL;
    public CHATBOTURL="https://3.209.127.229:8443/v1/";
    public LOGINURL = "https://core-apps-dev-api-management.azure-api.net/HR/Login/validate_login/";
    public TOKEN_URL = "https://3.209.127.229:8443/v1/credential";
    public SECRETKEY_URL = "https://3.209.127.229:8443/v1/secret";
    public GET_PAYSLIP_DATA_URL = "https://core-apps-dev-api-management.azure-api.net/HR/Payslip/getPayslipListDetails/";
    public GET_PAYSLIP_PDF_URL = "https://core-apps-dev-api-management.azure-api.net/HR/Payslip/getPayslipPDFBlob/";
    public GET_PROFILE_DATA_URL = "https://core-apps-dev-api-management.azure-api.net/HR/Person/";
    public GET_PROFILE_DATA_POST_URL = "https://core-apps-dev-api-management.azure-api.net/HR/Person/getPersonDirectoryDetails/";
    public GET_PERSON_ID_URL = "https://core-apps-dev-api-management.azure-api.net/HR/PersonData/getUserDetails/";

    /*public ApiUrl={
        LOGINURL:this.DEVBASEURL+'login'
    
    }*/

    public COMMON_APP_MESSAGE = {
        APP_TITLE: "TFGNextStage",
        TIMEOUT_IN_MILLISECOND:3000,
        LOADING_LOADER: "Loading",
        LOGIN_ERROR: "Unable to login on server, please contact system administrator",
        UNABLE_TO_CONNECT: "Unable to connect, please try later",
        SERVICE_NOT_FOUND: "Unable to connect, please contact system administrator",
        TIMEOUT_ERROR: "Request Timeout, please try again later",
        UNKNOWN_ERROR: "Something went wrong, please try again later",
        SERVER_ERROR: "Server error occurred, please try later",
        WEBSERVICE_FAILED: "Please check your internet connection or contact system administrator",
        SERVER_DATA_ERROR: "Unable to get valid data from server, please contact system administrator",
        SERVER_DATA_CHATBOT_ERROR: "There might be some network connection issue. Please try after some time or contact system administrator",
        STAFFID_DATA_ERROR: "Unable to get valid staff id from server, please contact system administrator",
        INVALID_AUTHORIZATION_TOKEN: "Authorization failed. Please try again.",
        NO_NETWORK_MESSAGE: "Network unavailable, please check your internet connection",
        NO_NETWORK_TITLE: "No Internet Connection",
        NETWORK_ONLINE: "You are now online",
        NETWORK_OFFLINE: "You are now offline",
        NO_NETWORK_MESSAGE_AND_OFFLINE_WORK: "You are currently offline, for latest data please check your internet connection and re-launch application",
        CONFIRM_EXIT_TITLE: 'Confirm Exit',
        CONFIRM_EXIT_MESSAGE: 'Do you want to exit the App?',
        INVALID_CREDENTIALS: "Invalid Employee Number/Password.",
        INVALID_CREDENTIALS_AFTR_BIOMETRIC: "Invalid Employee Number/Password. Login using Employee Number and password.",
        EMPTY_CREDENTIALS: "Please Enter Valid Employee Number and Password.",
        EMPTY_USERNAME: "Employee Number cannot be empty.",
        EMPTY_PASSWORD: "Password cannot be empty.",
        LOGOUT_MESSAGE: 'Are you sure you wish to logout?',
        CONFIRM_TEXT: 'Confirm',
        YES: "Yes",
        NO: "No",
        PROCEED_BTN_TEXT: 'Proceed',
        USERNAME_PLACEHOLDER: 'Username',
        PASSWORD_PLACEHOLDER: 'Password',
        LOGIN_BTN_TEXT:'Login',
        SECTION_TEXT: 'Sections',
        BACK_TEXT: 'Back',
        SUBMIT_BTN_TEXT: "Submit",
        CANCEL_BTN_TEXT: "Cancel",
        ALERT: 'Alert!',
        ERROR: 'Error'
    }

    public VALIDATION_ERROR={
        LOGIN:{
            USERNAME_REQUIRED: 'Username is mandatory',
            PASSWORD_REQUIRED: 'Password is mandatory'
        }
    }
}

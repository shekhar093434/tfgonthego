import { Injectable } from '@angular/core';
import { App, AlertController, ToastController, LoadingController, Loading, Platform, ViewController } from 'ionic-angular';
import { Constants } from '../utilities/constants';
import { Device } from '@ionic-native/device';
import { DeviceConfiguration } from '../models/DeviceConfiguration';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { TouchID } from '@ionic-native/touch-id';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';
import { UserdataModel } from '../models/UserdataModel';
import { Storage } from '@ionic/storage';
import { Login } from '../pages/login/login';


declare var cordova; 
@Injectable()
export class Utills {
    private displayLogsForPages: Array<String> = [];
    private isDisplayLog: boolean = true;
    private loading: Loading;
    devicetype: string;
    _screenOrientation: string;
    public userInfoData;
    public deivceData;
    public loader: any;
    constructor(
        private alertCtrl: AlertController,
        private app: App,
        public loadingController: LoadingController,
        public constants: Constants,
        public platform: Platform,
        private device: Device,
        public screenorientation: ScreenOrientation,
        public touchId: TouchID,
        private toastCtrl:ToastController,
        public loadingCtrl: LoadingController,
        //public viewCtrl: ViewController,
        public faio: FingerprintAIO, private storage: Storage
    ) {
        
    }

    presentLoading(){
        this.loader = this.loadingCtrl.create({content: "Please wait ...", cssClass: "loadingCssClass"})
        this.loader.present();
       }
     
     
    closeLoading(){
       this.loader.dismiss();
       }

    goToPage(toPageName, props?) {
        try {
            this.app.getActiveNav().push(toPageName, { props: (props !== null || props !== "" || props !== undefined) ? props : null });
        } catch (error) {
            error.catchError = true;
            error.pageName = "Utills";
            error.message = "goToPage : " + error.message;
            this.errorDetails(error);
        }
    }

    goToBack() {
        try {
            this.app.getActiveNav().pop();
        } catch (error) {
            error.catchError = true;
            error.pageName = "Utills";
            error.message = "goToBack : " + error.message;
            this.errorDetails(error);
        }
    }

    goToRoot() {
        try {
            this.app.getActiveNav().popToRoot();
        } catch (error) {
            error.catchError = true;
            error.pageName = "Utills";
            error.message = "goToRoot : " + error.message;
            this.errorDetails(error);
        }
    }

    showLoader(message: string = "") {
        if (this.loading == null) {
            this.loading = this.loadingController.create({
                content: message,
                enableBackdropDismiss: false
                //dismissOnPageChange: true
            });
            this.loading.present();
        } else {
            this.loading.data.content = message;
        }
    }

    hideLoader() {
        return new Promise((resolve, reject) => {
            try {
                if (this.loading != null) {
                    this.loading.dismiss()
                        .then(() => {
                            resolve();
                        })
                        .catch((error) => {
                            reject(error);
                        });
                    this.loading = null;
                }
            }
            catch (e) {
               
                reject(e);
            }
        });
    }

    showAlert(message: string, title: string) {
        try {
            //alert(message);
            let alert = this.alertCtrl.create({
                title: title,
                subTitle: message,
                buttons: ['OK'],
                enableBackdropDismiss: false
            });
            //alert.present(prompt);

            alert.present();

        } catch (error) {
            error.catchError = true;
            error.pageName = "Utills";
            error.message = "showAlert : " + error.message;
            this.errorDetails(error);
        }
    }

    errorDetails(error) {
        try {
            this.hideLoader();

            if (!this.isEmpty(error)) {

                this.log(error.pageName, "Error data : " + JSON.stringify(error));

                if (!this.isEmpty(error.status)) {
                    switch (error.status) {
                        case 408:
                            this.showAlert(this.constants.COMMON_APP_MESSAGE.TIMEOUT_ERROR, "Error");
                            break;
                        case 404:
                            this.showAlert(this.constants.COMMON_APP_MESSAGE.SERVICE_NOT_FOUND, "Error");
                            break;
                        default:
                            this.showAlert(error.statusText, "Error");
                            break;
                    }
                }
                else if (error.catchError == true) {
                    this.log("Utills", error.message);
                    this.showAlert(this.constants.COMMON_APP_MESSAGE.UNKNOWN_ERROR, "Error");
                }
                else if (error.sqlError == true) {
                    this.showAlert(this.constants.COMMON_APP_MESSAGE.UNKNOWN_ERROR, "Error");
                }
                else if (error.serverDataError == true) {
                    this.showAlert(this.constants.COMMON_APP_MESSAGE.SERVER_DATA_ERROR, "Error");
                }
                else if (error.customError == true) {
                    if (error.message == this.constants.COMMON_APP_MESSAGE.NO_NETWORK_MESSAGE)
                        this.showAlert(error.message, this.constants.COMMON_APP_MESSAGE.NO_NETWORK_TITLE);
                    else
                        this.showAlert(error.message, "Error");
                }
                else if (error.name == "TimeoutError") {
                    this.showAlert(this.constants.COMMON_APP_MESSAGE.TIMEOUT_ERROR, "Error");
                }
                else {
                    this.log("Utills", "Error : " + error.message)
                    this.showAlert(this.constants.COMMON_APP_MESSAGE.UNKNOWN_ERROR, "Error");
                }
            } else {
                this.showAlert(this.constants.COMMON_APP_MESSAGE.UNKNOWN_ERROR, "Error");
            }
        } catch (error) {
            error.message = "errorDetails : " + error.message;
            error.pageName = "Utills";
            error.catchError = true;
            this.showAlert(this.constants.COMMON_APP_MESSAGE.UNKNOWN_ERROR, "Error");
        }
    }

    log(pageName: String, message: String) {
        try {
            if (this.isDisplayLog) {
                var index: number;
                index = this.displayLogsForPages.indexOf(pageName);

                if (index > -1) {
                    console.log(pageName + " -> " + message);
                }
            }
        } catch (error) {
            console.log(pageName + " -> " + message);
        }
    }

    isEmpty(variable: any) {
        try {
            if ((variable == null) || (variable == undefined) || (variable == "null") || (variable == "undefined")) {
                return true;
            }
            else if (typeof variable == 'object' && (Object.keys(variable).length === 0 && variable.constructor === Object)) {
                return true;
            }
            else if (variable == '') {
                return true;
            }
            else
                return false;
        } catch (error) {
            error.message = "isEmpty : " + error.message;
            error.pageName = "Utills";
            error.catchError = true;
            this.errorDetails(error);
        }
    }

    isEmptyObject(obj: any) {
        try {
            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                    return false;
                }
            }
            return JSON.stringify(obj) === JSON.stringify({});
        } catch (error) {
            error.message = "isEmptyObject : " + error.message;
            error.pageName = "Utills";
            error.catchError = true;
            this.errorDetails(error);
        }
    }

    public updateDeviceDetails(): DeviceConfiguration{
        
        if (this.platform.is('tablet')) {
            this.devicetype = 'tablet';
          }
        else if (this.platform.is('ipad')) {
            this.devicetype = 'ipad';
          } 
        else {
            this.devicetype = 'phone';
        }
        if(this.screenorientation.type.includes("landscape")){
            this._screenOrientation = "landscape";
        } else {
            this._screenOrientation = "portrait";
        }
        return DeviceConfiguration.updateDeviceConfiguration(this.device.version, this.device.platform,  this._screenOrientation, this.devicetype, this.platform.width(), this.platform.height());
        //console.log('Device version is: ' + this.device.version);
        //console.log('Device platform is: ' + this.device.platform);
    }

    public checkTouchIDAvailability(){
        /*this.touchId.isAvailable()
            .then(
                res => console.log('TouchID is available!'),
                err => console.error('TouchID is not available', err)
            );*/
        this.touchId.isAvailable()
            .then(
                (res) => {
                    this.showAlert("TouchID is available!","Alert");
                    DeviceConfiguration.setTouchIDAvailabilityStatus(true);
                },
                (err) => { 
                    this.showAlert("TouchID is not available!","Alert");
                    DeviceConfiguration.setTouchIDAvailabilityStatus(false);
            });
    }

    public verifyFingerPrint(){
        this.touchId.verifyFingerprint('Scan your fingerprint to unlock')
            .then((res) => {
                //this.viewCtrl.dismiss();
            }, (err) => { console.error('Error', err);
        });

    }

    public checkBiometricAvailable(){
        //Check if Fingerprint or Face  is available
        this.faio.isAvailable()
        .then((result) => {
            if(result === "finger"){
                //Fingerprint Auth is available
                DeviceConfiguration.setTouchIDAvailabilityStatus(true);
            } else {
                DeviceConfiguration.setTouchIDAvailabilityStatus(false);
            } 
            if(result === "face"){
                //Face Auth is available
                DeviceConfiguration.setFaceIDAvailabilityStatus(true);
            } else {
                DeviceConfiguration.setFaceIDAvailabilityStatus(false);
            }
            /*else {
                //Fingerprint or Face Auth is not available
                this.utility.presentAlert("Fingerprint/Face Auth is not available   on this device!");
            }*/
        },
        (err) => { 
            //Fingerprint authentication not ready
            DeviceConfiguration.setTouchIDAvailabilityStatus(false);
            DeviceConfiguration.setFaceIDAvailabilityStatus(false);
         });
    }

    public verifyBiometric() {
        let status: boolean;
        this.faio.show({
            clientId: 'TFGNextStep',
            clientSecret: 'tfgNextStep', //Only necessary for Android(optional)
            disableBackup: true, //Only for Android(optional)
            localizedFallbackTitle: 'Use Pin', //Only for iOS(optional)
            localizedReason: 'Please Authenticate' //Only for iOS(optional)
        })
        .then((result: any) => {
            console.log("FingerPrint - "+ JSON.stringify(result));
            if(result.withFingerprint != null && result.withFingerprint != undefined){
            //if(result == "OK" || result == "Success"){
                //Fingerprint/Face was successfully verified
                //Go to dashboard
                //this.setAndGet.UserName = this.data.userName;
                //this.navCtrl.push("DashboardPage")
                status =  true;
            }
            else {
                //Fingerprint/Face was not successfully verified
                //this.showAlert("Fingerprint/Face was not successfully verified", "ALERT");
                status = false;
            }
        })
        .catch((error: any) => {
                //Fingerprint/Face was not successfully verified
                //this.utility.presentAlert(error);
                //this.showAlert(error, "ERROR");
                status = false;
        });

    }

    public saveUserInfoToLocalStorage(){
        let userDataObj = UserdataModel.getUserInfo();
         this.userInfoData={
            UserName:userDataObj.UserName,
            Password:userDataObj.Password,
            Authenticated:userDataObj.Authenticated,
            isOracleUser:userDataObj.isOracleUser,
            isAutoLogin:userDataObj.isAutoLogin,
            Staffid:userDataObj.Staffid,
            SecretKey:userDataObj.SecretKey
        }
        this.storage.set("UserInfoObject",this.userInfoData).then((successData)=>{
            //console.log(successData);
            localStorage.setItem('Authenticated', "1");
            //localStorage.setItem('Username', userDataObj.UserName);
            //localStorage.setItem('Password', userDataObj.Password);
        });
    }

    public retriveUserInfoFromLocalStorage(): UserdataModel{
        let object: any;
        this.storage.get("UserInfoObject").then((_userData)=>{
          //object = UserdataModel.fromJSON(_userData);
          object = _userData;
        })
        return object;
      }

      public saveDeviceConfigurationToLocalStorage(){
        let deviceDataObj = DeviceConfiguration.getDeviceConfiguration();
         this.deivceData={

            version: deviceDataObj.version,
            platform: deviceDataObj.platform,
            screenorientation: deviceDataObj.screenorientation,
            devicetype: deviceDataObj.devicetype,
            deviceWidth: deviceDataObj.deviceWidth,
            deviceHeight: deviceDataObj.deviceHeight,
            isTouchIDEnabled: deviceDataObj.isTouchIDEnabled,
            isFaceIDAvailable: deviceDataObj.isFaceIDAvailable
        }
        this.storage.set("DeviceDataObject",this.deivceData).then((successData)=>{
            //console.log(successData);
        })
    }

    public retriveDeviceConfigurationFromLocalStorage(): DeviceConfiguration{
        let object;
        this.storage.get("DeviceDataObject").then((_deviceData)=>{
          //object = DeviceConfiguration.fromJSON(_deviceData);
          object = _deviceData;
        })
        return object;
      }

      presentToast(msg) {
        let toast = this.toastCtrl.create({
          message: msg,
          duration: 2000
        });
        toast.present();
      }

    formatPayslipTimestamp(timestamp){
        let str: string;
        var d = new Date(timestamp);
        var n = d.getMonth();
        var y = "'"+(""+timestamp).slice(2,4);
        switch (n) {
            case 0:
                str = "January"+y;
                break;
            case 1:
                str = "February"+y;
                break;
            case 2:
                str = "March"+y;
                break;
            case 3:
                str = "April"+y;
                break;
            case 4:
                str = "May"+y;
                break;
            case 5:
                str = "June"+y;
                break;
            case 6:
                str = "July"+y;
                break;
            case 7:
                str = "August"+y;
                break;
            case 8:
                str = "September"+y;
                break;
            case 9:
                str = "October"+y;
                break;
            case 10:
                str = "November"+y;
                break;
            case 11:
                str = "December"+y;
                break;

        }
        return str;
    }

    formatPayslipTimestampForPayslipFilter(timestamp){
        let str: string;
        var d = new Date(timestamp);
        var n = d.getMonth();
        var y = ","+(""+timestamp).slice(0,4);
        switch (n) {
            case 0:
                str = "January"+y;
                break;
            case 1:
                str = "February"+y;
                break;
            case 2:
                str = "March"+y;
                break;
            case 3:
                str = "April"+y;
                break;
            case 4:
                str = "May"+y;
                break;
            case 5:
                str = "June"+y;
                break;
            case 6:
                str = "July"+y;
                break;
            case 7:
                str = "August"+y;
                break;
            case 8:
                str = "September"+y;
                break;
            case 9:
                str = "October"+y;
                break;
            case 10:
                str = "November"+y;
                break;
            case 11:
                str = "December"+y;
                break;

        }
        return str;
    }
    
}
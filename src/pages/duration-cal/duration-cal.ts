import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Utills } from '../../utilities/common';
/**
 * Generated class for the DurationCalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-duration-cal',
  templateUrl: 'duration-cal.html',
})
export class DurationCalPage {
  currentDate: string;
  callback : any = this.navParams.get("callback");
  fromDateClicked: boolean = true;
  toDateClicked: boolean = false;
  stat : boolean = false;
  fromDate: any;
  toDate: any;
  fromDateDisplay: string;
  toDateDisplay: string;
  constructor(public utils: Utills, public navCtrl: NavController, public navParams: NavParams) {
    this.formatDate();
    this.fromDateDisplay = this.currentDate;
    this.toDateDisplay = this.currentDate;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CalendarPage');
  }

  fromClicked(){
    this.fromDateClicked = true;
    this.toDateClicked = false;
    this.stat = false;
  }

  toClicked(){
    this.fromDateClicked = false;
    this.toDateClicked = true;
    this.stat = false;
  }

  dateSelected(selectedDate){
    var date = ""+selectedDate;
    if(this.fromDateClicked){
      this.fromDate = selectedDate;
      this.fromDateDisplay = date.slice(8,10)+" "+date.slice(0,3)+", "+date.slice(4,7)+"'"+date.slice(13,15);
    }
    if(this.toDateClicked){
      if(selectedDate >= this.fromDate){
        this.toDate = selectedDate;
        this.toDateDisplay = date.slice(8,10)+" "+date.slice(0,3)+", "+date.slice(4,7)+"'"+date.slice(13,15);
        this.stat = true;
      } else
        this.utils.showAlert("End date must not be before Start date", "Validation Error");
    }
  }

  formatDate(){
    var today = new Date();
    this.currentDate = today.getDate()+" "+(+1)+" "+today.getFullYear();
    var day = today.getDay();
    var daylist = ["Sun","Mon","Tues","Wed","Thur","Fri","Sat"];
    switch (today.getMonth()) {
      case 0:
          this.currentDate = today.getDate()+" "+daylist[day]+", Jan"+"'"+(""+today.getFullYear()).slice(2,4);
          break;
      case 1:
          this.currentDate = today.getDate()+" "+daylist[day]+", Feb"+"'"+(""+today.getFullYear()).slice(2,4);
          break;
      case 2:
          this.currentDate = today.getDate()+" "+daylist[day]+", Mar"+"'"+(""+today.getFullYear()).slice(2,4);
          break;
      case 3:
          this.currentDate = today.getDate()+" "+daylist[day]+", Apr"+"'"+(""+today.getFullYear()).slice(2,4);
          break;
      case 4:
          this.currentDate = today.getDate()+" "+daylist[day]+", May"+"'"+(""+today.getFullYear()).slice(2,4);
          break;
      case 5:
          this.currentDate = today.getDate()+" "+daylist[day]+", Jun"+"'"+(""+today.getFullYear()).slice(2,4);
          break;
      case 6:
          this.currentDate = today.getDate()+" "+daylist[day]+", Jul"+"'"+(""+today.getFullYear()).slice(2,4);
          break;
      case 7:
          this.currentDate = today.getDate()+" "+daylist[day]+", Aug"+"'"+(""+today.getFullYear()).slice(2,4);
          break;
      case 8:
          this.currentDate = today.getDate()+" "+daylist[day]+", Sep"+"'"+(""+today.getFullYear()).slice(2,4);
          break;
      case 9:
          this.currentDate = today.getDate()+" "+daylist[day]+", Oct"+"'"+(""+today.getFullYear()).slice(2,4);
          break;
      case 10:
          this.currentDate = today.getDate()+" "+daylist[day]+", Nov"+"'"+(""+today.getFullYear()).slice(2,4);
          break;
      case 11:
          this.currentDate = today.getDate()+" "+daylist[day]+", Dec"+"'"+(""+today.getFullYear()).slice(2,4);
          break;

  }

  }

  doneClick(){
    if(this.stat){
      if(this.toDate >= this.fromDate){
        this.navCtrl.getPrevious().data.fromdate = this.fromDateDisplay;
        this.navCtrl.getPrevious().data.todate = this.toDateDisplay;
        this.navCtrl.getPrevious().data.numberOfDays = ((this.toDate - this.fromDate)/ (1000 * 3600 * 24))+1;
        this.navCtrl.pop();
      } else 
        this.utils.showAlert("End date must not be before Start date", "Validation Error");
    } else
      this.utils.showAlert("Select End date", "Validation Error");
  }
  public ionViewWillLeave(){
    
  }
  closeClick(){
    this.navCtrl.pop();
  }

}

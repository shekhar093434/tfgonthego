import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DurationCalPage } from './duration-cal';

@NgModule({
  declarations: [
    DurationCalPage,
  ],
  imports: [
    IonicPageModule.forChild(DurationCalPage),
  ],
})
export class DurationCalPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DurationCalPage } from '../duration-cal/duration-cal';
import { Utills } from '../../utilities/common';
import { Constants } from '../../utilities/constants';
/**
 * Generated class for the ApplyLeavePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-apply-leave',
    templateUrl: 'apply-leave.html',
})
export class ApplyLeavePage {
    currentDate: string;
    startDate: string;
    endDate: string;
    _startDate: string;
    _endDate: string;
    _days: string;
    days: string = "01";
    constructor(public navCtrl: NavController, public navParams: NavParams, public utils: Utills, public constants: Constants) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ApplyLeavePage');
    }

    selectDuration() {
        this.navCtrl.push(DurationCalPage);
    }
    closeClick() {
        this.navCtrl.pop();
    }

    searchClick(){
        this.utils.showAlert("Work in progress",  this.constants.COMMON_APP_MESSAGE.ALERT);
      }
    public ionViewWillEnter() {
        this._startDate = this.navParams.get("fromdate");
        this._endDate = this.navParams.get("todate");
        this._days = "" + this.navParams.get("numberOfDays");
        if (this._startDate != null && this._startDate != undefined && this._startDate.length > 0) {
            this.startDate = this._startDate;
            if (this._endDate != null && this._endDate != undefined && this._endDate.length > 0) {
                this.endDate = this._endDate;
            }
        } else
            this.formatDate();
        if (this._days != null && this._days != "undefined" && this._days.length > 0) {
            this.days = this._days;
        } else
            this.days = "01";

    }

    formatDate() {
        var today = new Date();
        this.currentDate = today.getDate() + " " + (+1) + " " + today.getFullYear();
        var day = today.getDay();
        var daylist = ["Sun", "Mon", "Tues", "Wed", "Thur", "Fri", "Sat"];
        switch (today.getMonth()) {
            case 0:
                this.currentDate = today.getDate() + " " + daylist[day] + ", Jan" + "'" + ("" + today.getFullYear()).slice(2, 4);
                break;
            case 1:
                this.currentDate = today.getDate() + " " + daylist[day] + ", Feb" + "'" + ("" + today.getFullYear()).slice(2, 4);
                break;
            case 2:
                this.currentDate = today.getDate() + " " + daylist[day] + ", Mar" + "'" + ("" + today.getFullYear()).slice(2, 4);
                break;
            case 3:
                this.currentDate = today.getDate() + " " + daylist[day] + ", Apr" + "'" + ("" + today.getFullYear()).slice(2, 4);
                break;
            case 4:
                this.currentDate = today.getDate() + " " + daylist[day] + ", May" + "'" + ("" + today.getFullYear()).slice(2, 4);
                break;
            case 5:
                this.currentDate = today.getDate() + " " + daylist[day] + ", Jun" + "'" + ("" + today.getFullYear()).slice(2, 4);
                break;
            case 6:
                this.currentDate = today.getDate() + " " + daylist[day] + ", Jul" + "'" + ("" + today.getFullYear()).slice(2, 4);
                break;
            case 7:
                this.currentDate = today.getDate() + " " + daylist[day] + ", Aug" + "'" + ("" + today.getFullYear()).slice(2, 4);
                break;
            case 8:
                this.currentDate = today.getDate() + " " + daylist[day] + ", Sep" + "'" + ("" + today.getFullYear()).slice(2, 4);
                break;
            case 9:
                this.currentDate = today.getDate() + " " + daylist[day] + ", Oct" + "'" + ("" + today.getFullYear()).slice(2, 4);
                break;
            case 10:
                this.currentDate = today.getDate() + " " + daylist[day] + ", Nov" + "'" + ("" + today.getFullYear()).slice(2, 4);
                break;
            case 11:
                this.currentDate = today.getDate() + " " + daylist[day] + ", Dec" + "'" + ("" + today.getFullYear()).slice(2, 4);
                break;

        }
        this.startDate = this.currentDate;
        this.endDate = this.currentDate;
    }
}

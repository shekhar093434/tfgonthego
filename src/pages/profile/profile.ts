import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NetworkProvider } from '../../providers/network/networkcalls';
import { UserProfileData } from '../../models/UserProfileData';
import { Utills } from '../../utilities/common';
import { Constants } from '../../utilities/constants';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  profileObjs: any= [];
  name: string= "";
  language: string= "";
  work_phone: string= "";
  email_address: string= "";
  location: string= "";
  organization: string= "";
  supervisor_name: string= "";
  location_address: string= "";
  constructor(public navCtrl: NavController, public navParams: NavParams, public networkService: NetworkProvider, public utils: Utills, public constants: Constants) {
    //this.getProfileLocalData();
    
  }

  public ionViewWillEnter()
  {
    setTimeout(() => {
      this.profileObjs = UserProfileData.getProfileData();
      //console.log('Profile Data : '+JSON.stringify(this.profileObjs));
      this.name = this.profileObjs[0].FirstName +" "+ this.profileObjs[0].LastName;
      //this.name = this.profileObjs[0].FirstName;
      this.language = this.profileObjs[0].Language;
      this.work_phone = this.profileObjs[0].WorkPhone;
      this.email_address = this.profileObjs[0].EmailAddress;
      this.location = this.profileObjs[0].Location;
      this.organization = this.profileObjs[0].Organization;
      this.supervisor_name = this.profileObjs[0].SupervisorFirstName+ " "+ this.profileObjs[0].SupervisorLastName;
      //this.supervisor_name = this.profileObjs[0].SupervisorFirstName;
      this.location_address = this.profileObjs[0].LocationAddress;
    }, 500);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  goBack(){
    this.navCtrl.pop();
  }

  /*getProfileLocalData(){
    this.networkService.getProfileStaticData().then((profileData) => {
      var _data = JSON.parse(JSON.stringify(profileData));
      UserProfileData.fromJSON(_data.getPersonDirectoryDetails_Output.OutputParameters.Output);
      this.profileObjs = UserProfileData.getProfileData();
      this.name = this.profileObjs[0].FirstName +" "+ this.profileObjs[0].LastName;
      this.language = this.profileObjs[0].Language;
      this.work_phone = this.profileObjs[0].WorkPhone;
      this.email_address = this.profileObjs[0].EmailAddress;
      this.location = this.profileObjs[0].Location;
      this.organization = this.profileObjs[0].Organization;
      this.supervisor_name = this.profileObjs[0].SupervisorFirstName+ " "+ this.profileObjs[0].SupervisorLastName;
      this.location_address = this.profileObjs[0].LocationAddress;

    }, (err) => {
      console.log(err);
      this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.SERVER_DATA_ERROR, this.constants.COMMON_APP_MESSAGE.ERROR);
    });
  }*/


}

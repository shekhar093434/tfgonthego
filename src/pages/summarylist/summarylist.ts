import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {WorklistPage} from '../worklist/worklist';

/**
 * Generated class for the SummarylistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-summarylist',
  templateUrl: 'summarylist.html',
})
export class SummarylistPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SummarylistPage');
  }

  Worklist(){
    this.navCtrl.push(WorklistPage);
  }
  goBack(){
    this.navCtrl.popToRoot();
  }
  
}

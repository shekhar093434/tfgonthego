import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SummarylistPage } from './summarylist';

@NgModule({
  declarations: [
    SummarylistPage,
  ],
  imports: [
    IonicPageModule.forChild(SummarylistPage),
  ],
})
export class SummarylistPageModule {}

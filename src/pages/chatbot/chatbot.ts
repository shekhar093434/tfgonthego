import { Component, ViewChild, ChangeDetectorRef, NgZone } from '@angular/core';
import { Navbar, Keyboard, ToastController,IonicPage, ViewController, NavController, NavParams, Content, Platform, Modal, Events, ModalController, App, AlertController, LoadingController  } from 'ionic-angular';
import { SpeechRecognition } from '@ionic-native/speech-recognition';
import { Utills } from '../../utilities/common';
import { NetworkProvider } from '../../providers/network/networkcalls';
import { Constants } from '../../utilities/constants'
import { Storage } from '@ionic/storage';
import { ChatbotResponseModel } from '../../models/ChatbotResponseModel';
import { TextToSpeech } from '@ionic-native/text-to-speech';
import { DeviceConfiguration } from '../../models/DeviceConfiguration';
import { UserProfileData } from '../../models/UserProfileData';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/interval';

/**
 * Generated class for the ChatbotPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chatbot',
  templateUrl: 'chatbot.html',
})
export class ChatbotPage {
  @ViewChild(Navbar) navBar: Navbar;
  @ViewChild(Content) content: Content;
  timeout: any;
  keyBoardOpen: boolean = false;
  msgReceived: boolean = null;
  sendBtnVisibility: boolean = false;
  animationstart: boolean = false;
  gifloading: boolean = false;
  speaking : boolean = false;
  mute : boolean = true;
  private chatMsg: any="";
  private userName: any;
  options: any;
  rawList: any = [];
  matches: String[];
  chatMessages: any = [];
  isRecording = false;
  responseData: any;
  requestChatMessage: string;
  selectChange: any;
  isEnabled: boolean;
  welcomeMsg: any;
  userWelcomeMsg: any;
  isForm: any;
  uniqueId: any;
  userResponse: any;
  //responseForm: any = [];
  //policyNumber: any;
  imagesrc: any;
  //likeDislike: any;
  tempValue: any;
  likeData: any;
  myLikeImage: any;
  myDisLikeImage: any;
  myDate: String;
  //policyNo: any;
  //policyDate: any;
  //responseCause: any;
  disabledBtn: boolean;
  disabled: any;
  // isenabled: boolean = true;
  claimStatus: any;
  image: any;
  loader: any;
  //userId: any;
  loaderForm: any;
  //pushNotify: any;
  btnCarousel: any;
  btnActive: any;
  btnFlag: boolean;
  //carouselFlag: boolean;
  //notifyFlag:any;
  element1:any;
  locale:any;
  rate:any;
  deviceDataObj: any;
  sub: any;
  backbutton: any;
  profileObjs: any= [];
  name: string= "";
  constructor(private keyboard: Keyboard, private tts: TextToSpeech, public toastCtrl: ToastController, public constants: Constants, private storage: Storage, public navCtrl: NavController,public ngzone:NgZone,public viewCtrl: ViewController, public events: Events, public cd: ChangeDetectorRef, public navParams: NavParams, public alertCtrl: AlertController, public loadingController: LoadingController, private speechRecognition: SpeechRecognition, public app: App, private modalCtrl: ModalController, private platform: Platform, private utils: Utills, public networkService: NetworkProvider ) {
    this.deviceDataObj = DeviceConfiguration.getDeviceConfiguration();
    //console.log("Device Platform - "+this.deviceDataObj.platform);

    //this.locale = 'e-nUS';
    this.locale = 'en-ZA';
    if (this.deviceDataObj.platform === 'iOS') 
      this.rate = 12;
    else 
      this.rate = 6;
    //this.pushNotify = localStorage.getItem("notify");//navParams.get("secondPassed");
    this.isEnabled = true;
    //this.notifyFlag=false;
    //this.userId = localStorage.getItem("userId");
    this.myLikeImage = "assets/imgs/like.svg";
    this.myDisLikeImage = "assets/imgs/dislike.svg";
    this.myDate = new Date().toISOString();
    this.disabledBtn = false;
    //this.pushMsg();

     this.loader = this.loadingController.create({
      content: "Logging Out",
      spinner: 'bubbles'
    });

    this.loaderForm = this.loadingController.create({
      content: "Loading.."
    });

    if (this.deviceDataObj.platform === 'iOS') {
      // This will only print when on iOS
      //console.log('I am an iOS device!');
      this.isEnabled = false;
    } else if (this.deviceDataObj.platform === 'android') {
      // This will only print when on Android
      //console.log('I am an android device!');
      this.isEnabled = false;
    } else if (this.deviceDataObj.platform === 'Android') {
      // This will only print when on Android
      //console.log('I am an android device!');
      this.isEnabled = false;
    } else {
      //console.log('I am on Web!');
      this.isEnabled = true;
    }

    this.platform.pause.subscribe(() => {
        if(this.speaking){
        this.tts.speak({text: ''});  // <<< speak an empty string to interrupt.
        this.speaking = false;
        return;
      }
    });

    this.sub = Observable.interval(100)
    .subscribe((val) => {
      //console.log("Keyboard status" + this.keyboard.isOpen())
      if(!this.keyboard.isOpen()){
        this.keyBoardOpen = false;
      }else if(this.keyboard.isOpen()){
        this.keyBoardOpen = true;
      }
      this.scrollBottom();
     });

  }

  openhideKeyboard(){
    if(this.keyboard.isOpen())
      this.keyBoardOpen = true;
    else
      this.keyBoardOpen = true;
  }

  toggleMute(){
    if(this.mute){
      this.mute = false;
    }
    else {
      this.mute = true;
      if(this.speaking){
        this.tts.speak({text: ''});  // <<< speak an empty string to interrupt.
        this.speaking = false;
        return;
      }
    }
  }

  startAnimationAudio(){
    this.animationstart = true;
    this.sendBtnVisibility = true;
    this.startRecording(); 
  }

  startAnimationText(){
    this.animationstart = true;
    setTimeout(() => {
      this.sendBtnVisibility = true;
     }, 10);
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Please Say Something',
      duration: 3000,
      cssClass: "toastMessageClass",
    });
    toast.present();
  }

  closeListening(){
    this.isRecording = false;
    this.gifloading = false;
    this.sendBtnVisibility = false;
    this.stopListening();
  }

  public ionViewWillEnter()
    {
      this.profileObjs = UserProfileData.getProfileData();
      this.name = this.profileObjs[0].FirstName;
        this.storage.get("UserInfoObject").then((_userData)=>{
        if(_userData != null){
          //object = UserdataModel.fromJSON(_userData);
          this.userName = _userData.UserName;
          this.welcomeMsg = "Hello " + this.name + ". This is your virtual assistant Kiara. Welcome to Employee Engagement Application.I can help you with following queries.\n1) Leave\n2) Time and Attendance\nFor assistance please use the voice prompt or type in your query."
          localStorage.setItem("welcomeMsg", this.welcomeMsg);
          this.userWelcomeMsg = localStorage.getItem("welcomeMsg");
          //console.log("UserName >>" + this.userName + "  Message >>>" + this.welcomeMsg); 
        }
      });

    }

  /*pushMsg(){
    if(this.pushNotify.length>0){
      this.notifyFlag=true;
      }
        localStorage.setItem("notify","");
  }*/
  
  scrollBottom() {
    setTimeout(() => {
      this.content.scrollToBottom(100);
    })
  }

  startRecording() {
    if(!this.isRecording) {
      //this.presentToast();
      this.gifloading = true;
      this.animationstart = false;
      if (this.deviceDataObj.platform === 'iOS') {
        this.getPermission();
      }
      if (this.isEnabled == false) {
        this.startListening();
      } else {
        //console.log("Mic ios");
        this.getPermission();
      }
    }
  }

  stopListening() {
    this.speechRecognition.stopListening().then(() => {
      this.isRecording = false;
    });
  }

  startListening() {
    if (this.platform.is('android' || 'Android')) {
      this.getPermission();
    }
    if (this.deviceDataObj.platform === 'iOS') {
      //console.log("Mic3 ios");
    }
    //console.log("Mic active");
    // let options = {
    //   language: 'en-US'
    // }

    this.timeout = setTimeout(() => {
      this.closeListening();
     }, 10000);

    this.speechRecognition.startListening({language: this.locale , showPopup: false}).subscribe(matches => {
      this.ngzone.run(()=>{
      
      this.matches = matches;
      this.cd.detectChanges();
      this.chatMsg = matches[0];
      if (this.chatMsg.length > 0) {
        clearTimeout(this.timeout);
        this.closeListening();
        this.sendMessages();
      }
       });
    },
    onerror => { 
      //console.log('Speech Error:', onerror);  
      this.msgReceived = true;
      clearTimeout(this.timeout);
      this.closeListening();
    }
  );
    this.isRecording = true;
  }

  buttonRequest(formTitle, indexVal, selectedBtn) {
    localStorage.setItem("formindex", selectedBtn);
    this.chatMsg = formTitle;
    this.sendMessages();
  }

  buttonFormRequest(formTitle, indexVal, selectedBtn) {
    localStorage.setItem("formindex", selectedBtn);
    //console.log("Temp Values" + JSON.stringify(this.tempValue));
    this.loaderForm.present();
    //this.formRequest(this.userId);
  }

  /*formRequest(dataId) {
    this.dataService.isFormPresent("fbId=", dataId).then((formData) => {
      this.responseForm = formData;
      console.log("Response form " + JSON.stringify(this.responseForm));
      this.responseForm = JSON.stringify(formData);
      localStorage.setItem('policyDetails', this.responseForm);

      if (this.responseForm) {
        this.loaderForm.dismiss();
        var modalPage = this.modalCtrl.create('ModalPage', "");
        modalPage.onDidDismiss(data => {
          console.log(data);
        });
        modalPage.present();
      }
    }, (err) => {
      console.log(err);
      this.loaderForm.dismiss();
    });
  }*/

  goBack(){
    if(this.speaking){
      this.tts.speak({text: ''});  // <<< speak an empty string to interrupt.
      this.speaking = false;
      //return;
    }
    this.navCtrl.pop();
  }

  sendMessages() {
    this.keyBoardOpen = false;
    this.keyboard.close();
    if ((this.chatMsg != null || this.chatMsg != '') && this.chatMsg.length > 0) {
      this.msgReceived = false;
      localStorage.setItem('chatMsg', this.chatMsg);
      this.chatMessages.push({
        "request": localStorage.getItem('chatMsg'),
        "old_message": false
      });
      this.scrollBottom();
      this.options = {
        "queryInput": {
            "text": this.chatMsg,
            "language": "en"
        },
        "userAttributes":{
                    "username": this.userName,
                    "session":this.userName,
                    "channel":"mobile"
        }
    }
    this.chatMsg = "";
      this.networkService.isNetworkConnectionAvailable()
        .then((isOnline: boolean) => {
          if (isOnline) {
            this.networkService.chatQuery("query", this.options).then((userInfo) => {
              if(this.gifloading){
                this.gifloading = false;
                this.sendBtnVisibility = false;
              }
              this.msgReceived = true;
              var _data = JSON.parse(JSON.stringify(userInfo));
              this.responseData = ChatbotResponseModel.fromJSON(_data.queryResult);
              //console.log("FulfillmentText - "+this.responseData.fulfillmentText);
              var element = null;
              element = {
                "request": this.chatMessages[this.chatMessages.length - 1].request,
                "response": (this.responseData.fulfillmentText).replace(new RegExp('\n', 'g'), "<br />"),
                "old_message": true,
                "flagStatus": "1"
              }
              this.chatMessages[this.chatMessages.length - 1] = element;
              if(!this.mute)
                this.playText(this.responseData.fulfillmentText);
            this.scrollBottom();
            this.chatMsg = "";
            }, (err) => {
              this.msgReceived = true;
              if(this.gifloading){
                this.gifloading = false;
                this.sendBtnVisibility = false;
              }
              console.log(err);
              this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.SERVER_DATA_CHATBOT_ERROR, this.constants.COMMON_APP_MESSAGE.ERROR);
              // this.utilPro.presentToast(JSON.stringify(err.name));
              this.loader.dismiss();
            });
          }
          else {
            this.msgReceived = true;
            this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.NO_NETWORK_TITLE,  this.constants.COMMON_APP_MESSAGE.NO_NETWORK_MESSAGE);
            this.loader.dismiss();
          }
        });

    }
  }

  playText(test) {

    if(this.speaking){
      this.tts.speak({text: ''});  // <<< speak an empty string to interrupt.
      this.speaking = false;
      return;
    }
    this.speaking = true;
    this.tts.speak({
      text: test,
      rate: this.rate / 6,
      locale: this.locale
    })
      .then((val) => {console.log('Success'); this.speaking = false;})
      .catch((reason: any) => { console.log(reason), this.speaking = false; });
  }

  ionViewWillLeave(){
    this.sub.unsubscribe();
    if(this.speaking){
      this.tts.speak({text: ''});  // <<< speak an empty string to interrupt.
      this.speaking = false;
      return;
    }
  }

  moreDetails(payValue) {
    let data = {
      enableBackdropDismiss: false
    };
    if (this.isForm == "true") {
      var claimPage = this.modalCtrl.create('ClaimPage', data);
      claimPage.present();
      this.claimStatus = localStorage.getItem("claimStatus");
    } else {
      this.chatMsg = payValue;
      this.sendMessages();
    }
  }

  likedislike(likeResponse, value, position) {
    if (value == "like") {
      /*this.likeData = {
        "userId": this.userId,
        "botResponse": likeResponse,
        "channelId": 2,
        "like": 1
      }*/
    } else {
      /*this.likeData = {
        "userId": this.userId,
        "botResponse": likeResponse,
        "channelId": 2,
        "dislike": 1
      }*/
    }

    /*this.dataService.isNetworkConnectionAvailable()
      .then((isOnline: boolean) => {
        if (isOnline) {
          this.dataService.likeService("likedislike", this.likeData).then((userInfo) => {
            this.responseData = userInfo;
            console.log("Response Like >>" + JSON.stringify(this.responseData));
            console.log("Response Like Status >>" + this.responseData.status);
          }, (err) => {
            console.log(err);
            this.loader.dismiss();
          });
        }
        else {
          this.utilPro.presentToast('No Internet');
          this.loader.dismiss();
        }
      });*/
  }

  getPermission() {
    this.speechRecognition.hasPermission()
      .then((hasPermission: boolean) => {
        if (!hasPermission) {
          this.speechRecognition.requestPermission()
          .then(
            () => this.startListening() ,
            () => this.utils.showAlert("Permission Denied by user", this.constants.COMMON_APP_MESSAGE.ERROR)
         )
        }
      });
  }

  /*getKeyboardHeight(){
    var height;
    if(this.keyBoardOpen)
      height = this._keyboardHeight+'px !important';
    else
      height = '7px !important'
    let styles = {
      'bottom': height
    };
    return styles;
  }*/

}

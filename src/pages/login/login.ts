import { Component } from '@angular/core';
import { IonicPage, Keyboard, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Platform } from 'ionic-angular';
import { Signup } from '../signup/signup';
import { Network } from '@ionic-native/network';
import { Subscription} from 'rxjs/Subscription';
import { Storage } from '@ionic/storage';
import { Crashlytics } from '@ionic-native/fabric';
import { Utills } from '../../utilities/common';
import { DeviceConfiguration } from '../../models/DeviceConfiguration';
import { Constants } from '../../utilities/constants'
import { UserdataModel } from '../../models/UserdataModel';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';
import { NetworkProvider } from '../../providers/network/networkcalls';
import { UserProfileData } from '../../models/UserProfileData';
import { PayslipListModels } from '../../models/PayslipListModel';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/interval';
//import { Keyboard } from '@ionic-native/keyboard';

/**
 * Generated class for the Login page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {
  _authToken: String = "";
  connected: Subscription;
  disconnected: Subscription;
  authForm : FormGroup;
  userData = {"userName":"", "password":""};
  faceIDEnabled : boolean = false;
  touchIDEnabled : boolean = false; 
  nonclickableFaceID : boolean;
  nonclickableTouchID : boolean;
  keyBoardOpen: boolean = false;
  //key_status: boolean = false;
  __status: boolean;
  _loginCaptchaStatus: boolean = false;
  object: any;
  options: any;
  public captcha: string ="";
  public _captcha: string ="";
  public _username: string;
  public _staffid: string;
  public _secretkey: string;
  public _password: string;
  public dataToStore;
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';
  sub: any;
  constructor(public keyboard: Keyboard, public faio: FingerprintAIO, public networkService: NetworkProvider, public constants: Constants, platform: Platform, private crashlytics: Crashlytics, private toast: ToastController, private network: Network, private storage: Storage, private utils: Utills, public navCtrl: NavController, public navParams: NavParams, private fb: FormBuilder, private alertCtrl: AlertController) {
    this.dataToStore={
      userName:'Guestuser',
      password:'Temp@1234'
    }
    //this.key_status = false;
    this.authForm = fb.group({
            'username' : [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(255)])],
            'password': [null, Validators.compose([Validators.required, Validators.minLength(8) ])]
          });

    try {
        throw new Error("this is javascriptError");
      }
      catch (e) {
        this.crashlytics.addLog("Error while loading data");
        this.crashlytics.sendNonFatalCrash(e.message || e);
      }
      if(DeviceConfiguration.getFaceIDAvailabilityStatus() === "true")
        this.faceIDEnabled = true;
      else
        this.faceIDEnabled = false;
      if(DeviceConfiguration.getTouchIDAvailabilityStatus() === "true")
        this.touchIDEnabled = true;
      else
        this.touchIDEnabled = false;
      
      if(this.faceIDEnabled)
        this.nonclickableFaceID = true;
      else
        this.nonclickableFaceID = false;
      if(this.touchIDEnabled)
        this.nonclickableTouchID = true;
      else
        this.nonclickableTouchID = false;
      this.storage.get("UserInfoObject").then((_userData)=>{
        if(_userData != null){
            //object = UserdataModel.fromJSON(_userData);
            this._username = _userData.UserName;
            this._password = _userData.Password;
            this._staffid = _userData.Staffid;
            this._secretkey = _userData.SecretKey;
            if(this._username == null || this._username == undefined){
              this.touchIDEnabled = false;
              this.faceIDEnabled = false;
              this.nonclickableFaceID = false;
              this.nonclickableTouchID = false;
            }
        }
      });
  
      /*window.addEventListener('native.keyboardshow', keyboardShowHandler);
      function keyboardShowHandler(e) { // fired when keyboard enabled
         var height = e.keyboardHeight;
         if(height != undefined)
          localStorage.setItem("KeyboardHeight", height);
         console.log("LoginPage Keyboard Height - "+ height);
      }*/
      //this.keyboard.disableScroll(true);

      this.sub = Observable.interval(200)
    .subscribe((val) => {
      //console.log("Keyboard status" + this.keyboard.isOpen());
      setTimeout(() => {
        if(!this.keyboard.isOpen()){
        this.keyBoardOpen = false;
      }else if(this.keyboard.isOpen()){
        this.keyBoardOpen = true;
      }
       }, 2000);
     });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Login');
  }

  ionViewDidEnter() {
    this.getLocalStorageData();
    this.connected = this.network.onConnect().subscribe(data => {
      //console.log(data)
      this.displayNetworkUpdate(data.type);
    }, error => console.error(error));
   
    this.disconnected = this.network.onDisconnect().subscribe(data => {
      //console.log(data)
      this.displayNetworkUpdate(data.type);
    }, error => console.error(error));

  }

  public ionViewWillEnter(){
    UserProfileData.resetProfileData();
    PayslipListModels.resetPayslipDataList();
    if((localStorage.getItem('Authenticated') != null)){
      if(localStorage.getItem('Authenticated') === '1'){
        if((localStorage.getItem('isAutoLogin') != null)){
          if(localStorage.getItem('isAutoLogin') === "true"){
            if(DeviceConfiguration.getTouchIDAvailabilityStatus() === "true"){
              this.nonclickableTouchID = true;
              setTimeout(() => {
                this.callBiometric('2');
               }, 500);
            } else if(DeviceConfiguration.getFaceIDAvailabilityStatus() === "true"){
              this.nonclickableFaceID = true;
              setTimeout(() => {
                this.callBiometric('1');
               }, 500);
            } else {
              /*let currentIndex = this.navCtrl.getActive().index;
                      this.navCtrl.push(HomePage).then(() => {
                        this.navCtrl.remove(currentIndex);
                  });*/
                localStorage.removeItem('Authenticated');
                UserdataModel.setAutoLogin(false);
                localStorage.setItem("isAutoLogin", "false");
            }
          }
        }
      }
    }
   if(localStorage.getItem('isAutoLogin') === "false"){
     this.disableBiometric();
   }
  }

  getLocalStorageData(){
    this.storage.get("UserInfoObject").then((_userData)=>{
      if(_userData != null){
        this._username = _userData.UserName;
        this._password = _userData.Password;
        this._staffid = _userData.Staffid;
        this._secretkey = _userData.SecretKey;
      }
    });
  }

  disableBiometric(){
    this.touchIDEnabled = false;
    this.faceIDEnabled = false;
    this.nonclickableFaceID = false;
    this.nonclickableTouchID = false;
  }

  ionViewWillLeave(){
    this.sub.unsubscribe();
    this.connected.unsubscribe();
    this.disconnected.unsubscribe();
    this._loginCaptchaStatus = false;
  }

  generateCaptcha(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  
    for (var i = 0; i < length; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
  
    return text;
  }

  login(){
      /*if(!this.key_status)
        this.presentPrompt();
      else {
        this.callLoginService();
      }*/
      this.callLoginService();
    }

    getPersonIDFromServer(){
      var dt = new Date();
      var date = dt.getFullYear() + "-" + (dt.getMonth() + 1) + "-" + dt.getDate();
      this.options = {
        "RESTHeader" : {
          "xmlns" : "http://xmlns.oracle.com/apps/fnd/rest/header",
          "Responsibility":"GLOBAL_HRMS_MANAGER",
            "RespApplication":"PER",
            "SecurityGroup":"STANDARD",
            "NLSLanguage":"AMERICAN",
          "Org_Id" : 81
        },
        "InputParameters" : {
           "userName": this._username,
            "effectiveDate": date
        }
      }
      console.log("Username "+ this._username);
        this.networkService.isNetworkConnectionAvailable()
        .then((isOnline: boolean) => {
            if (isOnline) {
              this.utils.presentLoading();
              this.networkService.getPersonID(this.options, this._authToken).then((_data) => {
                var data = JSON.parse(JSON.stringify(_data));
                this.utils.closeLoading();
                this._staffid = ""+data.getUserDetails_Output.OutputParameters.Output.UserDetailsBean[0].loginPersonId;
                console.log("Staff id - "+ this._staffid);
                if(""+this._staffid === "0"){
                  this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.STAFFID_DATA_ERROR, this.constants.COMMON_APP_MESSAGE.ERROR);
                  UserdataModel.updateUserInfo(true, this.userData.userName, this.userData.password, false, "", "");
                } else
                  UserdataModel.updateUserInfo(true, this.userData.userName, this.userData.password, false, this._staffid, this._secretkey);
                this.utils.saveUserInfoToLocalStorage();
                let currentIndex = this.navCtrl.getActive().index;
                  this.navCtrl.push(HomePage, {AuthToken: this._authToken}).then(() => {
                  this.navCtrl.remove(currentIndex);
                });
                }, (err) => {
                  console.log(err);
                  this.utils.closeLoading();
                  this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.STAFFID_DATA_ERROR, this.constants.COMMON_APP_MESSAGE.ERROR);
                  UserdataModel.updateUserInfo(true, this.userData.userName, this.userData.password, false, "", "");
                  this.utils.saveUserInfoToLocalStorage();
                });
            } else {
              this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.NO_NETWORK_TITLE,  this.constants.COMMON_APP_MESSAGE.NO_NETWORK_MESSAGE);
              }
        });
    }

  callLoginService(){
    if(this._loginCaptchaStatus){
      if(this.captcha.length > 0){
        if(this.captcha === this._captcha){
         this._loginCaptchaStatus = false;
        } else {
          this.utils.showAlert("Invalid Captcha. Try again with new captcha", "ERROR");
          //this.utils.presentToast("Invalid Captcha");
          this._loginCaptchaStatus = true;
          this._captcha = this.generateCaptcha(8);
          this.captcha="";
        }
      } else {
        this.utils.presentToast("Captcha is empty");
      }
    }
    
    if(this._loginCaptchaStatus)
      return;
    else if(this.utils.isEmpty(this.userData.userName)){
      this._loginCaptchaStatus = true;
        this.utils.presentToast(this.constants.COMMON_APP_MESSAGE.EMPTY_USERNAME);
    }
    else if(this.utils.isEmpty(this.userData.password)){
      this._loginCaptchaStatus = true;
        this.utils.presentToast(this.constants.COMMON_APP_MESSAGE.EMPTY_PASSWORD);
    }
    else if(this.userData.userName && this.userData.password){

      this.options = {
        "RESTHeader" : {
          "xmlns" : "http://xmlns.oracle.com/apps/fnd/rest/header",
          "Responsibility":"GLOBAL_HRMS_MANAGER",
            "RespApplication":"PER",
            "SecurityGroup":"STANDARD",
            "NLSLanguage":"AMERICAN",
          "Org_Id" : 81
        },
        "InputParameters" : {
           "P_USER": this.userData.userName,
           "P_PWD": this.userData.password
        }
      }

      this.networkService.isNetworkConnectionAvailable()
      .then((isOnline: boolean) => {
        if (isOnline) {
           /*  Guest user start */
          if(this.userData.userName === this.dataToStore.userName || (this.userData.password === this.dataToStore.password)){
              if((this.userData.userName === this.dataToStore.userName) && (this.userData.password === this.dataToStore.password))
              {
                UserdataModel.updateUserInfo(true, this.userData.userName, this.userData.password, false, this._staffid, this._secretkey);
                this.utils.saveUserInfoToLocalStorage();
                if(DeviceConfiguration.getFaceIDAvailabilityStatus() === "true")
                  this.faceIDEnabled = true;
                else
                  this.faceIDEnabled = false;
                if(DeviceConfiguration.getTouchIDAvailabilityStatus() === "true")
                  this.touchIDEnabled = true;
                else
                  this.touchIDEnabled = false;
                console.log("TouchID status - "+ this.touchIDEnabled);
                //Api connections
                if((this.touchIDEnabled || this.faceIDEnabled) && localStorage.getItem("BiometricCancelled") === "0") {
                this.faio.show({
                    clientId: 'TFGNextStep',
                    clientSecret: 'tfgNextStep', //Only necessary for Android(optional)
                    disableBackup: false, //Only for Android(optional)
                    localizedFallbackTitle: 'Use Pin', //Only for iOS(optional)
                    localizedReason: 'Please Authenticate' //Only for iOS(optional)
                })
                .then((result: any) => {
                    console.log("FingerPrint - "+ JSON.stringify(result));
                    if((result === "Success") || (result.withFingerprint != null && result.withFingerprint != undefined) || result.withPassword || result.withBackup){
                      let currentIndex = this.navCtrl.getActive().index;
                      this.navCtrl.push(HomePage).then(() => {
                        this.navCtrl.remove(currentIndex);
                      });
                      if(DeviceConfiguration.getTouchIDAvailabilityStatus() === "true")
                        this.touchIDEnabled = true;
                      else
                        this.touchIDEnabled = false;
                      if(this.touchIDEnabled)
                        this.nonclickableTouchID = true;
                      else
                        this.nonclickableTouchID = false;
                      if(DeviceConfiguration.getFaceIDAvailabilityStatus() === "true")
                        this.faceIDEnabled = true;
                      else
                        this.faceIDEnabled = false;
                      if(this.faceIDEnabled)
                        this.nonclickableFaceID = true;
                      else
                        this.nonclickableFaceID = false;
                    }
                    else {
                        //Fingerprint/Face was not successfully verified
                        //this.showAlert("Fingerprint/Face was not successfully verified", "ALERT");
                        this.utils.showAlert("Verification failed",  this.constants.COMMON_APP_MESSAGE.ALERT);
                        localStorage.removeItem('Authenticated');
                    }
                })
                .catch((error: any) => {
                  this.disableBiometric();
                        //Fingerprint/Face was not successfully verified
                        //this.utility.presentAlert(error);
                        //this.showAlert(error, "ERROR");
                        if(error === "Cancelled"){
                            localStorage.setItem("BiometricCancelled","1");
                            let currentIndex = this.navCtrl.getActive().index;
                            this.navCtrl.push(HomePage).then(() => {
                            this.navCtrl.remove(currentIndex);
                          });
                        } else {
                          this.utils.showAlert("Verification failed",  this.constants.COMMON_APP_MESSAGE.ALERT);
                          localStorage.removeItem('Authenticated');
                        }
                });
                } else {
                  let currentIndex = this.navCtrl.getActive().index;
                  this.navCtrl.push(HomePage).then(() => {
                    this.navCtrl.remove(currentIndex);
                  });
                }
            } else {
                this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.INVALID_CREDENTIALS, this.constants.COMMON_APP_MESSAGE.ALERT);
            }
          } else {
             /*  Guest user stop */
            this.utils.presentLoading();
            this.networkService.getAuthToken().then((tokenData) => {
              var _tokenData = JSON.parse(JSON.stringify(tokenData));
              this._authToken = "Basic "+_tokenData.networkCredentials.authorization;
          if(this._authToken.length > 0){    
          this.networkService.authenticateUser(this.options, this._authToken).then((userInfo) => {
            var _data = JSON.parse(JSON.stringify(userInfo));
            if(_data.OutputParameters.VALIDATE_LOGIN === "Y"){
              this._username = this.userData.userName;
              this.utils.closeLoading();
              //UserdataModel.updateUserInfo(true, this.userData.userName, this.userData.password, false, this._staffid, this._secretkey);
              //this.utils.saveUserInfoToLocalStorage();
              if(DeviceConfiguration.getFaceIDAvailabilityStatus() === "true")
                this.faceIDEnabled = true;
              else
                this.faceIDEnabled = false;
              if(DeviceConfiguration.getTouchIDAvailabilityStatus() === "true")
                this.touchIDEnabled = true;
              else
                this.touchIDEnabled = false;
              console.log("TouchID status - "+ this.touchIDEnabled);
              //Api connections
              if((this.touchIDEnabled || this.faceIDEnabled) && localStorage.getItem("BiometricCancelled") === "0") {
              this.faio.show({
                  clientId: 'TFGNextStep',
                  clientSecret: 'tfgNextStep', //Only necessary for Android(optional)
                  disableBackup: false, //Only for Android(optional)
                  localizedFallbackTitle: 'Use Pin', //Only for iOS(optional)
                  localizedReason: 'Please Authenticate' //Only for iOS(optional)
              })
              .then((result: any) => {
                  console.log("FingerPrint - "+ JSON.stringify(result));
                  this.__status = false;
                  if(result === "Success")
                    this.__status = true;
                  else if(result.withFingerprint != null && result.withFingerprint != undefined)
                    this.__status = true;
                  else if( result.withPassword)
                    this.__status = true;
                  else if( result.withBackup)
                    this.__status = true;
                  else
                    this.__status = false;
                  if(this.__status ){
                    this.getPersonIDFromServer();
                    /*let currentIndex = this.navCtrl.getActive().index;
                    this.navCtrl.push(HomePage, {AuthToken: this._authToken}).then(() => {
                      this.navCtrl.remove(currentIndex);
                    });*/
                    if(DeviceConfiguration.getTouchIDAvailabilityStatus() === "true")
                      this.touchIDEnabled = true;
                    else
                      this.touchIDEnabled = false;
                    if(this.touchIDEnabled)
                      this.nonclickableTouchID = true;
                    else
                      this.nonclickableTouchID = false;
                    if(DeviceConfiguration.getFaceIDAvailabilityStatus() === "true")
                      this.faceIDEnabled = true;
                    else
                      this.faceIDEnabled = false;
                    if(this.faceIDEnabled)
                      this.nonclickableFaceID = true;
                    else
                      this.nonclickableFaceID = false;
                  }
                  else {
                      //Fingerprint/Face was not successfully verified
                      //this.showAlert("Fingerprint/Face was not successfully verified", "ALERT");
                      this.utils.showAlert("Verification failed",  this.constants.COMMON_APP_MESSAGE.ALERT);
                      localStorage.removeItem('Authenticated');
                  }
              })
              .catch((error: any) => {
                this.disableBiometric();
                      //Fingerprint/Face was not successfully verified
                      //this.utility.presentAlert(error);
                      //this.showAlert(error, "ERROR");
                      if(error === "Cancelled"){
                          localStorage.setItem("BiometricCancelled","1");
                          this.getPersonIDFromServer();
                          /*let currentIndex = this.navCtrl.getActive().index;
                          this.navCtrl.push(HomePage, {AuthToken: this._authToken}).then(() => {
                          this.navCtrl.remove(currentIndex);
                        });*/
                      } else {
                        this.utils.showAlert("Verification failed",  this.constants.COMMON_APP_MESSAGE.ALERT);
                        localStorage.removeItem('Authenticated');
                      }
              });
              } else {
                this.getPersonIDFromServer();
                /*let currentIndex = this.navCtrl.getActive().index;
                this.navCtrl.push(HomePage, {AuthToken: this._authToken}).then(() => {
                  this.navCtrl.remove(currentIndex);
                });*/
              }
            } else {
              this.utils.closeLoading();
              this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.INVALID_CREDENTIALS, this.constants.COMMON_APP_MESSAGE.ALERT);
              this._loginCaptchaStatus = true;
              this._captcha = this.generateCaptcha(8);
              this.userData.userName="";
              this.userData.password="";
              this.captcha="";
            }
          }, (err) => {
            console.log(err);
            this.utils.closeLoading();
            this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.SERVER_DATA_ERROR, this.constants.COMMON_APP_MESSAGE.ERROR);
          });
        } else {
          this._authToken= "";
          this.utils.closeLoading();
          this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.INVALID_AUTHORIZATION_TOKEN, this.constants.COMMON_APP_MESSAGE.ERROR);
        } }, (err) => {
          this._authToken= "";
          console.log(err);
          this.utils.closeLoading();
          this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.INVALID_AUTHORIZATION_TOKEN, this.constants.COMMON_APP_MESSAGE.ERROR);
        });
      }
        }
        else {
          this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.NO_NETWORK_TITLE,  this.constants.COMMON_APP_MESSAGE.NO_NETWORK_MESSAGE);
        }
      });
      }
      else{
        this.utils.presentToast(this.constants.COMMON_APP_MESSAGE.EMPTY_CREDENTIALS);
      }
  }

    displayNetworkUpdate(connectionState: string){
      let networkType = this.network.type;
      this.toast.create({
        message: `You are now ${connectionState} via ${networkType}`,
        duration: 3000
      }).present();
    }

    hideShowPassword() {
      this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
      this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  callBiometric(val) {
    if(val === '2' && this.nonclickableTouchID) {
    //if(this.nonclickableTouchID || this.nonclickableFaceID) {
      this.faio.show({
        clientId: 'TFGNextStep',
        clientSecret: 'tfgNextStep', //Only necessary for Android(optional)
        disableBackup: false, //Only for Android(optional)
        localizedFallbackTitle: 'Use Pin', //Only for iOS(optional)
        localizedReason: 'Please Authenticate' //Only for iOS(optional)
      })
    .then((result: any) => {
        console.log("CallBiometric FingerPrint - "+ JSON.stringify(result));
        this.__status = false;
        if(result === "Success")
          this.__status = true;
        else if(result.withFingerprint != null && result.withFingerprint != undefined)
          this.__status = true;
        else if( result.withPassword)
          this.__status = true;
        else if( result.withBackup)
          this.__status = true;
        else
          this.__status = false;
        if(this.__status ){
          /*  Guest user start */
          if(this._username === this.dataToStore.userName) {
            if((this._username === this.dataToStore.userName) && (this._password === this.dataToStore.password))
            {
              UserdataModel.updateUserInfo(true, this._username, this._password, false, this._staffid, this._secretkey);
              this.utils.saveUserInfoToLocalStorage();
              this.callHomePage();
            }else {
                this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.INVALID_CREDENTIALS_AFTR_BIOMETRIC, this.constants.COMMON_APP_MESSAGE.ALERT);
            }
          } else {
             /*  Guest user stop */
            this.getLocalStorageData();
            setTimeout(() => {
            this.utils.presentLoading();
            this.options = {
              "RESTHeader" : {
                "xmlns" : "http://xmlns.oracle.com/apps/fnd/rest/header",
                "Responsibility":"GLOBAL_HRMS_MANAGER",
                  "RespApplication":"PER",
                  "SecurityGroup":"STANDARD",
                  "NLSLanguage":"AMERICAN",
                "Org_Id" : 81
              },
              "InputParameters" : {
                 "P_USER": this._username,
                 "P_PWD": this._password
              }
            }
            console.log("Credentials "+this._username+ " "+ this._password);
            this.networkService.getAuthToken().then((tokenData) => {
              var _tokenData = JSON.parse(JSON.stringify(tokenData));
              this._authToken = "Basic "+_tokenData.networkCredentials.authorization;
          if(this._authToken.length > 0){
            this.networkService.authenticateUser(this.options, this._authToken).then((userInfo) => {
            var _data = JSON.parse(JSON.stringify(userInfo));
            if(_data.OutputParameters.VALIDATE_LOGIN === "Y"){
              this.utils.closeLoading();
              //UserdataModel.updateUserInfo(true, this._username, this._password, false, this._staffid, this._secretkey);
              //this.utils.saveUserInfoToLocalStorage();
              this.callHomePage(); 
            } else {
              this.disableBiometric();
              this.utils.closeLoading();
              this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.INVALID_CREDENTIALS_AFTR_BIOMETRIC, this.constants.COMMON_APP_MESSAGE.ALERT);
            }
          }, (err) => {
            console.log(err);
            this.utils.closeLoading();
            this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.SERVER_DATA_ERROR, this.constants.COMMON_APP_MESSAGE.ERROR);
          });
        } else {
          this._authToken= "";
          this.utils.closeLoading();
          this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.INVALID_AUTHORIZATION_TOKEN, this.constants.COMMON_APP_MESSAGE.ERROR);
        } }, (err) => {
          this._authToken= "";
          console.log(err);
          this.utils.closeLoading();
          this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.INVALID_AUTHORIZATION_TOKEN, this.constants.COMMON_APP_MESSAGE.ERROR);
        });
      }, 500);
          }
        }
        else {
            //Fingerprint/Face was not successfully verified
            //this.showAlert("Fingerprint/Face was not successfully verified", "ALERT");
            this.utils.showAlert("Verification failed",  this.constants.COMMON_APP_MESSAGE.ALERT);
            localStorage.removeItem('Authenticated');
        }
    })
    .catch((error: any) => {
      this.disableBiometric();
            //Fingerprint/Face was not successfully verified
            //this.utility.presentAlert(error);
            if(error === "Cancelled"){
              localStorage.setItem("BiometricCancelled","1");
              this.utils.showAlert("Cancelled by user",  this.constants.COMMON_APP_MESSAGE.ALERT);
            } else 
              this.utils.showAlert("Verification failed",  this.constants.COMMON_APP_MESSAGE.ALERT);
            localStorage.removeItem('Authenticated');
    });
    
    } else  if(val === '1' && this.nonclickableFaceID) {
      //if(this.nonclickableTouchID || this.nonclickableFaceID) {
        this.faio.show({
          clientId: 'TFGNextStep',
          clientSecret: 'tfgNextStep', //Only necessary for Android(optional)
          disableBackup: false, //Only for Android(optional)
          localizedFallbackTitle: 'Use Pin', //Only for iOS(optional)
          localizedReason: 'Please Authenticate' //Only for iOS(optional)
        })
      .then((result: any) => {
          console.log("CallBiometric FingerPrint - "+ JSON.stringify(result));
          this.__status = false;
          if(result === "Success")
            this.__status = true;
          else if(result.withFingerprint != null && result.withFingerprint != undefined)
            this.__status = true;
          else if( result.withPassword)
            this.__status = true;
          else if( result.withBackup)
            this.__status = true;
          else
            this.__status = false;
          if(this.__status ){
             /*  Guest user start */
            if(this._username === this.dataToStore.userName) {
              if((this._username === this.dataToStore.userName) && (this._password === this.dataToStore.password))
              {
                UserdataModel.updateUserInfo(true, this._username, this._password, false, this._staffid, this._secretkey);
                this.utils.saveUserInfoToLocalStorage();
                this.callHomePage();
              }else {
                  this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.INVALID_CREDENTIALS_AFTR_BIOMETRIC, this.constants.COMMON_APP_MESSAGE.ALERT);
              }
            } else {
               /*  Guest user stop */
              this.getLocalStorageData();
              setTimeout(() => {
              this.utils.presentLoading();
              this.options = {
                "RESTHeader" : {
                  "xmlns" : "http://xmlns.oracle.com/apps/fnd/rest/header",
                  "Responsibility":"GLOBAL_HRMS_MANAGER",
                    "RespApplication":"PER",
                    "SecurityGroup":"STANDARD",
                    "NLSLanguage":"AMERICAN",
                  "Org_Id" : 81
                },
                "InputParameters" : {
                   "P_USER": this._username,
                   "P_PWD": this._password
                }
              }
              this.networkService.getAuthToken().then((tokenData) => {
                var _tokenData = JSON.parse(JSON.stringify(tokenData));
                this._authToken = "Basic "+_tokenData.networkCredentials.authorization;
            if(this._authToken.length > 0){
              this.networkService.authenticateUser(this.options, this._authToken).then((userInfo) => {
              var _data = JSON.parse(JSON.stringify(userInfo));
              if(_data.OutputParameters.VALIDATE_LOGIN === "Y"){
                this.utils.closeLoading();
                //UserdataModel.updateUserInfo(true, this._username, this._password, false, this._staffid, this._secretkey);
                //this.utils.saveUserInfoToLocalStorage();
                this.callHomePage(); 
              } else {
                this.disableBiometric();
                this.utils.closeLoading();
                this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.INVALID_CREDENTIALS_AFTR_BIOMETRIC, this.constants.COMMON_APP_MESSAGE.ALERT);
              }
            }, (err) => {
              console.log(err);
              this.utils.closeLoading();
              this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.SERVER_DATA_ERROR, this.constants.COMMON_APP_MESSAGE.ERROR);
            });
          } else {
            this._authToken= "";
            this.utils.closeLoading();
            this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.INVALID_AUTHORIZATION_TOKEN, this.constants.COMMON_APP_MESSAGE.ERROR);
          } }, (err) => {
            this._authToken= "";
            console.log(err);
            this.utils.closeLoading();
            this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.INVALID_AUTHORIZATION_TOKEN, this.constants.COMMON_APP_MESSAGE.ERROR);
          });
        }, 500);
            }
          }
          else {
              //Fingerprint/Face was not successfully verified
              //this.showAlert("Fingerprint/Face was not successfully verified", "ALERT");
              this.utils.showAlert("Verification failed",  this.constants.COMMON_APP_MESSAGE.ALERT);
              localStorage.removeItem('Authenticated');
          }
      })
      .catch((error: any) => {
        this.disableBiometric();
              //Fingerprint/Face was not successfully verified
              //this.utility.presentAlert(error);
              if(error === "Cancelled"){
                localStorage.setItem("BiometricCancelled","1");
                this.utils.showAlert("Cancelled by user",  this.constants.COMMON_APP_MESSAGE.ALERT);
              } else 
                this.utils.showAlert("Verification failed",  this.constants.COMMON_APP_MESSAGE.ALERT);
              localStorage.removeItem('Authenticated');
      });
      
      }
  }

  callHomePage(){
    this.getPersonIDFromServer();
    /*let currentIndex = this.navCtrl.getActive().index;
            this.navCtrl.push(HomePage, {AuthToken: this._authToken}).then(() => {
              this.navCtrl.remove(currentIndex);
            });*/
  }

  workinProgress(){
    this.utils.showAlert("Work in progress",  this.constants.COMMON_APP_MESSAGE.ALERT);
  }

  /*presentPrompt() {
    let alert = this.alertCtrl.create({
      title: 'Login',
      inputs: [
        {
          name: 'staffid',
          placeholder: 'Staff ID'
        },
        {
          name: 'secretkey',
          placeholder: 'Secret Key'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            this.key_status = false;
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Submit',
          handler: data => {
            if (this.utils.isEmpty(data.staffid)) {
              // Empty staffid!
              return false;
            } else if(this.utils.isEmpty(data.secretkey)){
              //Empty secret key
              return false;
            }else {
              // login
              this._staffid = data.staffid;
              this._secretkey = data.secretkey;
              this.key_status = true;
              this.callLoginService();
              return true;
            }
          }
        }
      ]
    });
    alert.present();
  }*/

}

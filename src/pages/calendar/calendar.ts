import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CalendarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-calendar',
  templateUrl: 'calendar.html',
})
export class CalendarPage {
  currentDate: string;
  callback : any = this.navParams.get("callback");
  fromDate: any;
  toDate: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.formatDate();
    this.fromDate = new Date();
    this.toDate = new Date(new Date().getTime()+(32460601000));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CalendarPage');
  }

  dateSelected(selectedDate){
    var date = ""+selectedDate;
    this.currentDate = date.slice(8,10)+" "+date.slice(0,3)+", "+date.slice(4,7)+"'"+date.slice(13,15);
  }

  formatDate(){
    var today = new Date();
    this.currentDate = today.getDate()+" "+(+1)+" "+today.getFullYear();
    var day = today.getDay();
    var daylist = ["Sun","Mon","Tues","Wed","Thur","Fri","Sat"];
    switch (today.getMonth()) {
      case 0:
          this.currentDate = today.getDate()+" "+daylist[day]+", Jan"+"'"+(""+today.getFullYear()).slice(2,4);
          break;
      case 1:
          this.currentDate = today.getDate()+" "+daylist[day]+", Feb"+"'"+(""+today.getFullYear()).slice(2,4);
          break;
      case 2:
          this.currentDate = today.getDate()+" "+daylist[day]+", Mar"+"'"+(""+today.getFullYear()).slice(2,4);
          break;
      case 3:
          this.currentDate = today.getDate()+" "+daylist[day]+", Apr"+"'"+(""+today.getFullYear()).slice(2,4);
          break;
      case 4:
          this.currentDate = today.getDate()+" "+daylist[day]+", May"+"'"+(""+today.getFullYear()).slice(2,4);
          break;
      case 5:
          this.currentDate = today.getDate()+" "+daylist[day]+", Jun"+"'"+(""+today.getFullYear()).slice(2,4);
          break;
      case 6:
          this.currentDate = today.getDate()+" "+daylist[day]+", Jul"+"'"+(""+today.getFullYear()).slice(2,4);
          break;
      case 7:
          this.currentDate = today.getDate()+" "+daylist[day]+", Aug"+"'"+(""+today.getFullYear()).slice(2,4);
          break;
      case 8:
          this.currentDate = today.getDate()+" "+daylist[day]+", Sep"+"'"+(""+today.getFullYear()).slice(2,4);
          break;
      case 9:
          this.currentDate = today.getDate()+" "+daylist[day]+", Oct"+"'"+(""+today.getFullYear()).slice(2,4);
          break;
      case 10:
          this.currentDate = today.getDate()+" "+daylist[day]+", Nov"+"'"+(""+today.getFullYear()).slice(2,4);
          break;
      case 11:
          this.currentDate = today.getDate()+" "+daylist[day]+", Dec"+"'"+(""+today.getFullYear()).slice(2,4);
          break;

  }

  }

  doneClick(){
   this.navCtrl.getPrevious().data.date = this.currentDate;
   this.navCtrl.pop();
  }
  public ionViewWillLeave(){
    
  }
  closeClick(){
    this.navCtrl.pop();
  }

}

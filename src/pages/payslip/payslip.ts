import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Constants } from '../../utilities/constants';
import { Utills } from '../../utilities/common';
import { PayslipListModels } from '../../models/PayslipListModel';
import { InAppBrowser , InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { NetworkProvider } from '../../providers/network/networkcalls';
import { Storage } from '@ionic/storage';
import{LeavesdetailsPage} from '../leavesdetails/leavesdetails';

@IonicPage()
@Component({
  selector: 'page-payslip',
  templateUrl: 'payslip.html',
})
export class PayslipPage {
  selectedIndex: any;
  actionContextId: string;
  downloadedBlob: string;
  private userName: any;
  private personId: any;
  private payslipDataList: PayslipListModels[] = [];
  authToken: any = this.navParams.get('AuthToken');
  options : InAppBrowserOptions = {
    location : 'yes',//Or 'no' 
    hidden : 'no', //Or  'yes'
    clearcache : 'yes',
    clearsessioncache : 'yes',
    zoom : 'yes',//Android only ,shows browser zoom controls 
    hardwareback : 'yes',
    mediaPlaybackRequiresUserAction : 'no',
    shouldPauseOnSuspend : 'no', //Android only 
    closebuttoncaption : 'Close', //iOS only
    disallowoverscroll : 'no', //iOS only 
    toolbar : 'yes', //iOS only 
    enableViewportScale : 'no', //iOS only 
    allowInlineMediaPlayback : 'no',//iOS only 
    presentationstyle : 'pagesheet',//iOS only 
    fullscreen : 'yes',//Windows only    
};

    monthSelected : string;
    yearSelected : string;
    months: any = [];
    years: any = [];
    constructor(private file: File, private fileOpener: FileOpener, private utils: Utills,public networkService: NetworkProvider,  private storage: Storage, public constants: Constants, public navCtrl: NavController, public navParams: NavParams, public alert: AlertController, private theInAppBrowser: InAppBrowser) {
          this.getPayslipLocalData();
    }


    leavedetail(){
      this.navCtrl.push(LeavesdetailsPage);
    }

goBack(){
    this.navCtrl.popToRoot();
}


getPayslipLocalData(){
    var payslipObjs = PayslipListModels.getPayslipDataList();
    for(var j=0; j< payslipObjs.length; j++){
      var _item = this.utils.formatPayslipTimestampForPayslipFilter(payslipObjs[j].EffectiveDate);
      var mitem = _item.split(",")[0];
      if(this.months.indexOf(mitem) === -1) {
        this.months.push(mitem);
      }
      /*var yitem = _item.split(",")[1];
      if(this.years.indexOf(yitem) === -1) {
        this.years.push(yitem);
      }*/
    }
}

monthPopup(){
  this.yearSelected = "";
  if(this.months.length <= 0){
    this.utils.showAlert("No data available", this.constants.COMMON_APP_MESSAGE.ALERT);
  } else {
    let alert = this.alert.create();
      alert.setTitle('Months');
      for(var i=0; i< this.months.length; i++){
      alert.addInput({
        type: 'radio',
        label: this.months[i],
        value: this.months[i]+","+i,
        checked: false
      });
    }
    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        this.payslipDataList.splice(0, this.payslipDataList.length);
        console.log("Clicked data: "+data);
        this.monthSelected = data.split(",")[0];
        this.selectedIndex = data.split(",")[1];

        var __payslipObjs = PayslipListModels.getPayslipDataList();
        this.years.splice(0, this.years.length);
        for(var j=0; j< __payslipObjs.length; j++){
          if(__payslipObjs[j].Month === this.monthSelected){
            this.payslipDataList.push(__payslipObjs[j]);
            if(this.years.indexOf(__payslipObjs[j].Year) === -1)
              this.years.push(__payslipObjs[j].Year);
          }
        }
      }
    });
    alert.present();
  }
}

yearPopup(){
  if(this.monthSelected === '' || this.monthSelected === null || this.monthSelected === undefined){
    this.utils.showAlert("Select month first", this.constants.COMMON_APP_MESSAGE.ALERT);
  }else if(this.years.length <= 0){
    this.utils.showAlert("No data available", this.constants.COMMON_APP_MESSAGE.ALERT);
  } else {
    let alert = this.alert.create();
      alert.setTitle('Year');
      for(var i=0; i< this.years.length; i++){
      alert.addInput({
        type: 'radio',
        label: this.years[i],
        value: this.years[i],
        checked: false
      });
    }
    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        console.log("Clicked data: "+data);
        this.yearSelected = data;
        for(var j=0; j< this.payslipDataList.length; j++){
          if(this.payslipDataList[j].Year === this.yearSelected)
            this.actionContextId = this.payslipDataList[j].ActionContextId;
        }
      }
    });
    alert.present();
  }
}

exportPayslip(){
  if(this.monthSelected === '' || this.monthSelected === null || this.monthSelected === undefined)
    this.utils.showAlert("Please select month", this.constants.COMMON_APP_MESSAGE.ALERT);
  else if(this.yearSelected === '' || this.yearSelected === null || this.yearSelected === undefined)
  this.utils.showAlert("Please select year", this.constants.COMMON_APP_MESSAGE.ALERT);
  else
  this.getPayslipBlobDataFromServer();
    //this.downloadPayslip();
  //this.utils.showAlert("Work in progress", this.constants.COMMON_APP_MESSAGE.ALERT);
}

downloadBlobToPDF(){
  var name = this.monthSelected+"_"+this.yearSelected+"_"+this.personId+".pdf";
    //let downloadPDF: any = "JVBERi0xLjQNCjUgMCBvYmoNCjw8DQovVHlwZSAvWE9iamVjdA0KL1N1YnR5cGUgL0ltYWdlDQovRmlsdGVyIC9GbGF0ZURlY29kZQ0KL0xlbmd0aCAyOTA3DQovV2lkdGggNDMyDQovSGVpZ2h0IDcxDQovQml0c1BlckNvbXBvbmVudCA4DQovQ29sb3JTcGFjZSAvRGV2aWNlUkdCDQovTWFzayBbMjU1IDI1NSAyNTUgMjU1IDI1NSAyNTVdDQo+Pg0Kc3RyZWFtDQp4nO1dS3IrKwz1nnojWUVWkUG24T15/LZwqzJKZqnKoB807U67P3AQAiRHp1SuXF/bzUccJCFgHA0Gg+FJ8fPZuwQGg8FgMBgMBoPBYDAYDAaDwWAwGAwGg8FgMHDg59PJNze0l19E1ci43cbrdZbbTUeZJ8wtPHXo/Mr6fviv42evP3xPC1HUdLnYN1GoLO/7pw3oVNRJj/Sbj48PPy7e3sfX13EYvFyG+Q+aXFa/EP8p90T33Ovt6+uLrS6b5xLqgpd//8X1Hy8vvoLXK1ftGOBa2xfv8iDuHacAXXCi8L7FrrcHnWws7ulrxIvRq/X2yCcQz0hhfuzX2mtW9EPYFWOtnLdbpPxsuN2OR0cvcexRTh2uUt0rciiui9t0awTxvn556W/thIHZXSddATbEEi+SG0cK4ceaBAZYtd5sz4Q318pQc8bxD3WP69sI0fYhs6KvWvfyR8Rxfi9WdBqFFK8XXLO4p3fvoCD7PnoyPnQVXNtgHeXROJx1YKHBYByGolYYOP7REuZfoJWIM4JY+3Atzd2r32k3Ke3pWhQTXk7ILV5CRXwohwn3+hYGb2jMwITBoXa2eo1GDr/fvQVwybempNuHizSmRHwIuE+2hLT5a2OuLHgOPkR8hJayUbZQvLDA55gq+PLBZwmBTSb435fsIMclizqkja9YvVpZYrlt0sxEFKiTZ3VXzoc+ACXKCL8cTT13PvQGYViUvFzm0BkjH6pwkFNNBw5S33rdS4tLg3FEmSDqr+jJdFUiM69qPvSGVu+2BXRsHryO+hwfBt/wcl+IvDB5LtIs5BIBBqkaf3mRmo7z7HcQSnXmNrJApk7G15L08qGoaGFS7YPLMOXg+X+G2HLQ4fI5+qrHeQQl2SaK/OUgw1AxO7HEIa1E1FI7yM+kEWjhw02akEwyPJ96fv169xryIbm0kaZ4wxBSiH1hqkoICBCKF6fEZK1r1yuI71O4dpVGUznzsHvNZJ0Mo6OeJGuqhQ/XoM2Gw32XRDVJTD3jY4iPI3E3I7/icmfs5tso5sT4t8yM0GjacNpfbrUDaNphgUZu0xqSCd9E5fFz1nRE3yBZHR1yroRsJ1THh7lTz8QA7HpYAs7dr/hY6JghvEbWok9E/VJq0Jjz0XU9ducUidEhDc5YMNx3m7bLiaDBBer4EDcOJeycqgp8apCz7zI3KeisB5N1b971kKnGaolBi0pLvmvqYzw2Ax7KfnuXxYQBuvhQJwPUAsgqMicFsCvPFt9TX+/iESDrvJyGK2SRTr2P0FRxngO6zD1I1clRGx+C7uEfIENU9yQ3BebrHVoRadNI7PZhrvRspPXWFIeMncJGA513sWQ4quJD0KKQ3NqMQHRPMhkGILGmow4Vy4fpRHGOToHW0YZHIxkZPgXpiOgELXx4KuJD3Dt4esz7XKJN0fEMExjQIDqsiDR/eb2c3WBMIfPInniRAAu5eMgELZwMRz18iMxuckpbG8nW2B/sJhZIz+7Hkbz1lF/Eaad8OxLSYmezYT0TLmmuND5BggYtfJic2iScctkMSfNAvqe8RrJzd9UR6y97VOVDxDWIVB/zmnMLBW0nl28cjjr4EPKqVLQ2F+KtwXdgThukR9Pe2pFsH8Znq8IxVe7zIr52biEBZ1nQZQoRaODDtPKrMMW5IJkKyEgN863xLy1+CBesyHQHF3CjQLe0ZJUz+YNaHBYVfBgfLFV3ygtEqjU0xg2AZdlHkhc7KSStL2rB0CPOEOZhT9VomXVZFSr4MDH7DHX3gOfK62td4yQ+4vSayvEx9TjMhcYPgXUuOjMUpGseADERMV2ihDvEQjwf0s926yW1LTTxXUZETthNIh8i2+KosxV/dh+yYwULRP/791+lWneA+MGl7CTkBiMx3mVaAjV7ZC3LCjvPAT2ngqwbNRZBEBMR+c2qIdPGEM+Hyk5CbtBi8XGncTElgJUPN+3wfYfPV/n5XP7+JmH54nzYF7iNlOw4VNpawhVFND5sCE182GYtw/gQ0Ip1CPfX2QyX1yyvZFm+nqUeJFqouw+OJSZpfNgQmviwDRcl/OU/wYfZ68vdLzYirykgCYfkAB2Y3R0nNOPDhlATP2yWBW3xw5GSf5h9ljiv0OZKyKUtyzfDvOZYygRyd4MWiOdDNfZhMyKquv2hI7JUkZZ/2OuSI7JutDlzG1msid8NFP+urS/zwccu8Ct7eknLLXJxO0pRrtcK6SgZV/5he8eZzFdIcI9j2/5EaHSveRqhYkZHIcTzoYf8/MOWqxipYaJmL8AamfZeUf5hS0qkkiHq3XMpHrSwcq5aT3O8gAo+TNjz0/1cHaVxXz9T+HpBatVgOxLL9i/zXEuXlBI/sUbCYRwlO1aSpRXCJEmo4MPcwfLUmO5zjOqeIvdkQjoksq9R8f5l/9B6t3gXnoqPpcE4TVjualy85vU7B5mT5+8XpSMmC6xFJ1XwoZ1vs4Gdf8i1Xy/r/lOEBl3XFDY+6M7fbrNPzShgHXdtCy16qnCZNfAhlJKqorW5QDtTWiYA0+LA/uc73wa9OnkRx3j3gzvmP6aYCY+TgiYE3muHWJLscpihnZyjVZy8pIEPPYBRI+rS+epIDmEVVwYgxH5o/POef+gaKusW9XpzDSH7up7Xn8UMz3GjhxI+hAxynakmRLRKxqiHKRBKXUKtcf4heJ81+feTwLKvN1TfLUF33wJtsiWrQgkfeiDz4J8JJKLbWiXPEcjwOYnD1zrvC3c/uS9VRzv0kE+6eM372RZckZFMiYr4EGxtUWWuCnAUyGwQ0Bg7KXzF8w/xPSy8jjPSIJHZrUH6EMJsYDFcZWUGcxTx4QgPor9hJWbs7Ha9LGd5JSMd+jwmXPX+lCzHmcXaKT96q9NWxG0795pNcnHGxnHNFMaHqE8xMcCfWF7JGrnuw31Z0T09K/gfOaun9v0pWR5o+TAp20FMKTOX7EuVVQxXcTkz9ajwsJSsCE93BqgMyj4L9/mwp6a+wzLn+rpn4YemrkfKOVrcF4CcqL9qVfLSVVHkcIMuO1vJXvPvUB3CUA0Z5vtzesM74y6ZvPD9cZXHDpX87T2e1p5Meie/f1CXLOXZjKx7hliD8j+8NuEc+ijgzeYlp/gS6KXN/XpZO53JDiAWOUw0yFJg3svREGbbmYho5kAvndzIuteQNDYhskxDJaGSxmVus7zb/bBTXhmAvV3N7l+uTYnsV3/yAswP3xdPi05uJpouy1IE2WxS6BIqIUizgINXv96VZepoiMoa3r/8nRuUyOp0hDT6hvGxTNeDL6qgxI2eaOHDfWhdPiU23qCkQv3i8vIC7nprfd9o1gaWC8xgvFcq1wM50Vq4Tu67SQUfnoXWvZEguLWb+zjZW3FFyesrPvD73L+ctZqfrI5wTzm/qIeG/XfVc4RK5DAsI5m97xKzGcROQB19HMJibl/x6ZGZgYVm8cN92+L6dp72ic5cchLeCveH8p4jVC5n24vkD5x4NObn06uWUxtRre1atXv6vetu+Z1LTj9rGD/cwOtb+eEPJYev9kLZhllPlXJYMXKdRPeyRQQOTU+tLcNWHCQdLCOWFcu2zCT35lTPxs9LLH9UYyyzUdopx+mDiIPEu9UNje7+S3yikendX6g5rt0ZQELAZ4OQliaBGJds8PIade2F7EXnezhxOsQV+LzArRBjxsHd6Z9asvQb2zCp9Sm5Ac8SlQ4M0N5clObjPMIratDD5UTT2sJ+YuoEzyrLiaz717f3BsaVHzj7py+13ryzRAbCpsVkIrRMOOsuWfLMGMj3soMpqCXSOGTByxYIpGphsoRpftwyQGWR5uMYDAaDwWAwGAwGg8FgMBgMBoPBYDAYDAaDwWAwGAwGg8FgMBgMBoPBYDAY/ib+B2eP9zBlbmRzdHJlYW0NCmVuZG9iag0KNiAwIG9iag0KPDwNCi9UeXBlIC9QYWdlDQovUGFyZW50IDMgMCBSDQovUmVzb3VyY2VzIDQgMCBSDQovQ29udGVudHMgNyAwIFINCi9NZWRpYUJveFsgMCAwIDYxMi4wIDc5Mi4wIF0NCi9Dcm9wQm94WyAwIDAgNjEyLjAgNzkyLjAgXQ0KL1JvdGF0ZSAwDQo+Pg0KZW5kb2JqDQo3IDAgb2JqDQo8PCAvTGVuZ3RoIDI5OTggL0ZpbHRlciAvRmxhdGVEZWNvZGUgPj4NCnN0cmVhbQ0KeJytW99z2zYSfveM/wc83bhzDkv8JvJmx3aaTJPmbPfmOtMXRqJtXiTSJaWmvr/+PpCgREqAIjJNJkqs5WKx2P12PwDMH0SJKCExftM4UpIIQbQQkZaKzJbkx3fLmFyV5F+nJxEjkSCRIrdvyebf1ePpyeX96QltR8AYCUbrBrhfnp78eENPTwhl5P7h9OTsU/pSlYvFD/f/PT2JGx2M1v7DDtUNwxIRGTsRFTHjhmF2mHgzTL3In5thOh2hecRgVAsTCUF7xnWn9JgNNGQcR2Lw/BltHsDPGt5xSiKdiMbh4Td2rtdwG35iyTSVkaISs8N4khj7UWXkwevicLXgX8IwQBLFtO+om/L18nlRvmTDaVMZR4lslJjUHj+fSmgVX7LqnHyoIvKhfEqXy2xOPqZfs81f4xzllMFYz1MWxWM85RKKNrW+4WmF2S2zSUHAWlI1IQiJimKloY/MdTM7ezfPilW+eiEf18vPWeVd/thgCTzLnxjKKJWMAwti2ipvXBm7ykpGWqmBL4NV/qV6TIv8f+kqL4sheFhsTQWd4rGOY8rJK/LT7bsP5O6lXmXLekqclFGRpmp8nLSOEgN1E3F1ACoHQmZ1VezxjlJqOJWTgrX1Z2ywtIyEVmGPUOTIZVrntS9SQV/ub94C8cXqafFC7tJFWr1MilKiI6YmRImii1CUNJWYyPhS8Odytp9+mwBB2Zd+1qn7p4zclrMv5HKdL+Z58TgtXBvHxoYrSSJ+wK+3VTrP/JEKONVoEDMpPBYMU0BkwyNR7VQCRV/SvS8/k/t8tfC2HKulfUl3UaQLlITXqA7TakMXnY1fo6MTR4bLsF+fyjoPVb2gW5QqkURbn4jzc1LIFPJnSn+ylEoh8TT1d86fcgxzla78EYOWlyQw+up9WrxiKOrTIrVxZ2ykLN1iB/z5pcofc6wz8TvWxexvd8yFSSbA94Qwaay3AbAUBcA8bl3Udf5YLMErDnUoaCd/e4fauDQaVDJisQz71Gfzu/EJeWJL+U1Zz57yIidvq3L93LWrSeGy7YaKLlxAaxKzIwOmIs6xOAwDHOITF/N5ldW1N1xQ5r4kvFtVWdYF+jURXmV4wOBCQzidQtnyX1I+kJu0Wr4m/y7Lqn4uwdpvy3ROvONwsEXN23HeYpjiKf0z8/dXBvaduK3O3frzuvpsTV7l9arKZyjfF4hDWWTn5L78WljRG5Dg1+RN+pz5bVN0w8SRZqvjf4g20GgeQh1eAdtvynn2GpROTWPI27A3KT0m7Bx7SpCnbwa+8ga+y+5Q4Lki77PiS17UZUHaoHr1+7H/lFblV3Kd1v5H++HV2LK2C2aQu5HBUkQJ2JZdsOE33YJJyzKJAq9PFOSSoBjImLq1clJXyyHF9izB4gykjjFBivRB+R5Iux1hZAuLdiJ4F7bZCgMmW2HAYiMMG9RMNORBcpQrK9+bjNiVRbpZL6SB3VvYVRx8YRdR2OnLqB2XYK78uDyT9mxBYnNpBA9WTvKueCir5ZYQRwZumwR/jItq7+dN6cOU2upFtR49JZ0g710+fcqqvJyT62K+bbRHTYGhg0+aA2N6dxL36V+kncjx9jkyZpJ9EYu9RUhfmrZ8U2V/rLNi9rJzSGSfVyaSzHOmxOmrD2ll2YYZaDk3e2ruaGlnHv0H3qSLrJinVdsP27XQSP7tR5Ogg2963VAjKxSsJkkDHOxKJetB9YCYMbojphqVpBNzkxwSO9MIRpx4sNfT9AsNhycBmT0GpBNBi8BJMw21trprT2uwmToZsYI2ZzNTICt4JIXZouVula7W9Ui0TrLv8nh3ArfZQ1YBKhn5WI4E7aRpOLT0p2FPR/Y2B+1ycWSNrzl/tIFb+HDa0ziLOY1NLKmRPsD2n7Tgv7jdgn8sWjkDjMQGjnQHrQFxh9atuIEj3UFrQOxMSwNS4WuVukUOWFJA6Mb1CxFcud+BjwQslU1NmgJYuCi53Afs3Xq5TKsXy6ddlR+H2Vg1hW5cslKUO4PyIbFx17HsyLGlvddpVeTF41joTpoGB3FMqPZN4yqbr2e2go2YSAPeSRORSF97MeOZyEfseRCY4a4BbEtYqhbHmx493OEn5wy5wtlAjSuwgcbKVu0MT9q9wbD1Sm72DJxRc24wgqRTkCyM3VeF2m5A6nC8lfqabkDq4IZVkDHzY9Fp+nEKpsx5QNEGWek94XEgFhqtUxCm7ZnnWBgLLSLKPGx5PGzsVBS6BSp8PLrfCiUjZlxiXGX1rMqfx/V7hig7+/gctRIs4RGndDiJi2W5Llb7rU5IZFTs4aVv0vqpf/y/GR3EV9Gh4hmj5zH26DuAag0APmiHDrNV+me2IHezp2w57LssYchENXxcnzP0zG7DahN/+9Hkz+CbHURhlx7rZgvIAXUhOlhQ9N+glEm6o8wspPkQGIgIU02Kqn3UONWAFNkN7wJCKSJsa0NSe0DN9oRHoIqz9l7eIUvC4TH5xLEE9ogkhK1JvcBNqc1wati0KX03zGxoNzAbg3LEhKpvY6ybaAhl4OEPD+RiNttHpwDBMjs4E0J2cNi10MfNp4vfrncG401rGzwlznmCviq8w6HDgRM5v+bLbH4xn//TP8H+o8wEJ8jj5myprQP2ijGb5zN08It8Tq4D0+3r0HPb0QKDMyQQ59vBb7NVXmXtpnxdhA309ei5bbeJ9Bqgdo/povDruxvifa9isyT9pynWXLNxJUxS1kAeNdVeKwHsKm4P5Qa0oBE3vV2ZSNNN52dN4T0sdTVMWo+31cQJuxrmlzJ73sZCUmc2IHX1zy/s6p9f6uqfX8jtAYEIzhj7kT3ZcXyEG4GdgZpISLgBp/SdEd9UoCMZucyK7CFf1SRFkgJizQ1j2p7qjeIq9qZFo39OICs8sWtnvpOsdBOYylYGswixFa6TzUXAkK2UxapKZyt7zYEtPXnOKvJTua52OEbStPr+KGcUABfGZwo50+01esPbUetm/O0h276Bni7FpmAaheEqiRh4dEdS+IDChKSOwvTEHgpjo4XOEqAanW5ACkSwJKirVXNOHiAxMdKD7usez2I2eJxIY4KI/O3+yjbi9XK9aM7mavIPcgn+W8yy8cSmQ8NUZvP9mLQR32JyArX5FiC7mYYgyZVlLelf6edFhno2K5cZub15591IDxDp30xszPWgJSiabL8bkwao+ef13osaXWseQnPbmnfNKICo29nvmqmOMNPXP2BGAg7d6b2I8U2fFR2w0tKXgfohfoSiFEn3HuwZT2g8ZF+3nSm/lb7yQSuoQ4brbtEA886KPfF9U+GHFfm1dm+O7i1ZX1vZq0u/EaZR7FwAue7lmNtcXiwW5VeLWr8zffXB/nLXDrXHJ7xbMti5qGt72LSuZk8pnCA3l34/+ooM7N1o//hxA7Mu9JR8yooake4I6+GY9LUPEVceb9+8sJHXO2a+Efq+Njvn9kbEu19gqHNGb0IfsybkKKneYYdP97chYxkyMyCFoj2kRmtJxM4ReF8ODaXpgBh+Q8wFAzcI8WDO0IFoiJHyGFstFiKzm/7rlXbTCuh23dkvRfuPk6BdziMTB0fuOrtfSrETDZllxmzaske1IwV+et6RgoBDJvJsCo5j70xjb8/FlCsBpm149T5RcEfO7qWXXnk+lqszBS6EIFDe5PDYeYFPiqR7GcbdPH/IVk9jbr+p1g08ps6EJg3NH8zlMi2+kMsKpfepeTtnBGmy75ROnwyn6BzDubgTlf6LcsdFR9j/AvIdEXLkaTiZwG6GSftCutinTld5lc1W5Cp7tu+c7txttAvfU0WTiZXcqf7tkvSf+k/7yzDBvRxs8PDuZcbR+xa3zcYS+G8celLfTUdIbPNVBoe2+cPCYmc5Yc0bX94rC1snWBISIhsEOrpf2BideNnBuH3FQE8qT0hNnuxXpw9ZXaeP2e9n9e8/eFKO8UgnZj/lPpakr0lukLLzKZvW9lJK7V0bdwEOiJ0ytxfHgTunVlUGhNzeR+td4f8BrDZa5g0KZW5kc3RyZWFtDQplbmRvYmoNCjEgMCBvYmoNCjw8DQovVHlwZSAvQ2F0YWxvZw0KL1BhZ2VzIDMgMCBSDQo+Pg0KZW5kb2JqDQoyIDAgb2JqDQo8PA0KL1R5cGUgL0luZm8NCi9Qcm9kdWNlciAoT3JhY2xlIEJJIFB1Ymxpc2hlciAxMC4xLjMuNC4yKQ0KPj4NCmVuZG9iag0KMyAwIG9iag0KPDwNCi9UeXBlIC9QYWdlcw0KL0tpZHMgWw0KNiAwIFINCl0NCi9Db3VudCAxDQo+Pg0KZW5kb2JqDQo0IDAgb2JqDQo8PA0KL1Byb2NTZXQgWyAvUERGIC9UZXh0IF0NCi9Gb250IDw8IA0KL0YxIDggMCBSDQovRjIgOSAwIFINCj4+DQovWE9iamVjdCA8PCANCi9JbTAgNSAwIFINCj4+DQo+Pg0KZW5kb2JqDQo4IDAgb2JqDQo8PA0KL1R5cGUgL0ZvbnQNCi9TdWJ0eXBlIC9UeXBlMQ0KL0Jhc2VGb250IC9IZWx2ZXRpY2ENCi9FbmNvZGluZyAvV2luQW5zaUVuY29kaW5nDQo+Pg0KZW5kb2JqDQo5IDAgb2JqDQo8PA0KL1R5cGUgL0ZvbnQNCi9TdWJ0eXBlIC9UeXBlMQ0KL0Jhc2VGb250IC9IZWx2ZXRpY2EtQm9sZA0KL0VuY29kaW5nIC9XaW5BbnNpRW5jb2RpbmcNCj4+DQplbmRvYmoNCjEwIDAgb2JqDQpbIDYgMCBSIC9YWVogMzYuMCAyMDMuMjYgbnVsbCBdDQplbmRvYmoNCjExIDAgb2JqDQpbIDYgMCBSIC9YWVogMzYuMCAyMDMuMjYgbnVsbCBdDQplbmRvYmoNCnhyZWYNCjAgMTINCjAwMDAwMDAwMDAgNjU1MzUgZg0KMDAwMDAwNjM2NyAwMDAwMCBuDQowMDAwMDA2NDIyIDAwMDAwIG4NCjAwMDAwMDY1MDQgMDAwMDAgbg0KMDAwMDAwNjU3MiAwMDAwMCBuDQowMDAwMDAwMDEwIDAwMDAwIG4NCjAwMDAwMDMxMzIgMDAwMDAgbg0KMDAwMDAwMzI5MCAwMDAwMCBuDQowMDAwMDA2Njg5IDAwMDAwIG4NCjAwMDAwMDY3OTQgMDAwMDAgbg0KMDAwMDAwNjkwNCAwMDAwMCBuDQowMDAwMDA2OTU1IDAwMDAwIG4NCnRyYWlsZXINCjw8DQovU2l6ZSAxMg0KL1Jvb3QgMSAwIFINCi9JbmZvIDIgMCBSDQovSUQgWzxhMjllYTY3NGFmMGI5N2FhNTBkOGFhMDg1YmFmNjVkYz48YTI5ZWE2NzRhZjBiOTdhYTUwZDhhYTA4NWJhZjY1ZGM+XQ0KPj4NCnN0YXJ0eHJlZg0KNzAwNg0KJSVFT0YNCg==";
            fetch('data:application/pdf;base64,' + this.downloadedBlob,
              {
                method: "GET"
              }).then(res => res.blob()).then(blob => {
                setTimeout(() => {
                this.file.writeFile(this.file.externalApplicationStorageDirectory, name, blob, { replace: true }).then(res => {
                  setTimeout(() => {
                  this.fileOpener.open(
                    res.toInternalURL(),
                    'application/pdf'
                  ).then((res) => {
    
                  }).catch(err => {
                    console.log("open error");
                    this.utils.showAlert("Error in opening payslip pdf", this.constants.COMMON_APP_MESSAGE.ERROR);
                  });
                }, 500);
                }).catch(err => {
                    this.utils.showAlert("Error in saving payslip pdf", this.constants.COMMON_APP_MESSAGE.ERROR);
                      console.log("save error");     
           });
          }, 500);
              }).catch(err => {
                this.utils.showAlert("Error occured", this.constants.COMMON_APP_MESSAGE.ERROR);
                     console.log("error");
              });
    }

downloadBlobToPDF1(){
  var name = this.monthSelected+"_"+this.yearSelected+"_"+this.personId+".pdf";
    //let downloadPDF: any = "JVBERi0xLjQNCjUgMCBvYmoNCjw8DQovVHlwZSAvWE9iamVjdA0KL1N1YnR5cGUgL0ltYWdlDQovRmlsdGVyIC9GbGF0ZURlY29kZQ0KL0xlbmd0aCAyOTA3DQovV2lkdGggNDMyDQovSGVpZ2h0IDcxDQovQml0c1BlckNvbXBvbmVudCA4DQovQ29sb3JTcGFjZSAvRGV2aWNlUkdCDQovTWFzayBbMjU1IDI1NSAyNTUgMjU1IDI1NSAyNTVdDQo+Pg0Kc3RyZWFtDQp4nO1dS3IrKwz1nnojWUVWkUG24T15/LZwqzJKZqnKoB807U67P3AQAiRHp1SuXF/bzUccJCFgHA0Gg+FJ8fPZuwQGg8FgMBgMBoPBYDAYDAaDwWAwGAwGg8FgMHDg59PJNze0l19E1ci43cbrdZbbTUeZJ8wtPHXo/Mr6fviv42evP3xPC1HUdLnYN1GoLO/7pw3oVNRJj/Sbj48PPy7e3sfX13EYvFyG+Q+aXFa/EP8p90T33Ovt6+uLrS6b5xLqgpd//8X1Hy8vvoLXK1ftGOBa2xfv8iDuHacAXXCi8L7FrrcHnWws7ulrxIvRq/X2yCcQz0hhfuzX2mtW9EPYFWOtnLdbpPxsuN2OR0cvcexRTh2uUt0rciiui9t0awTxvn556W/thIHZXSddATbEEi+SG0cK4ceaBAZYtd5sz4Q318pQc8bxD3WP69sI0fYhs6KvWvfyR8Rxfi9WdBqFFK8XXLO4p3fvoCD7PnoyPnQVXNtgHeXROJx1YKHBYByGolYYOP7REuZfoJWIM4JY+3Atzd2r32k3Ke3pWhQTXk7ILV5CRXwohwn3+hYGb2jMwITBoXa2eo1GDr/fvQVwybempNuHizSmRHwIuE+2hLT5a2OuLHgOPkR8hJayUbZQvLDA55gq+PLBZwmBTSb435fsIMclizqkja9YvVpZYrlt0sxEFKiTZ3VXzoc+ACXKCL8cTT13PvQGYViUvFzm0BkjH6pwkFNNBw5S33rdS4tLg3FEmSDqr+jJdFUiM69qPvSGVu+2BXRsHryO+hwfBt/wcl+IvDB5LtIs5BIBBqkaf3mRmo7z7HcQSnXmNrJApk7G15L08qGoaGFS7YPLMOXg+X+G2HLQ4fI5+qrHeQQl2SaK/OUgw1AxO7HEIa1E1FI7yM+kEWjhw02akEwyPJ96fv169xryIbm0kaZ4wxBSiH1hqkoICBCKF6fEZK1r1yuI71O4dpVGUznzsHvNZJ0Mo6OeJGuqhQ/XoM2Gw32XRDVJTD3jY4iPI3E3I7/icmfs5tso5sT4t8yM0GjacNpfbrUDaNphgUZu0xqSCd9E5fFz1nRE3yBZHR1yroRsJ1THh7lTz8QA7HpYAs7dr/hY6JghvEbWok9E/VJq0Jjz0XU9ducUidEhDc5YMNx3m7bLiaDBBer4EDcOJeycqgp8apCz7zI3KeisB5N1b971kKnGaolBi0pLvmvqYzw2Ax7KfnuXxYQBuvhQJwPUAsgqMicFsCvPFt9TX+/iESDrvJyGK2SRTr2P0FRxngO6zD1I1clRGx+C7uEfIENU9yQ3BebrHVoRadNI7PZhrvRspPXWFIeMncJGA513sWQ4quJD0KKQ3NqMQHRPMhkGILGmow4Vy4fpRHGOToHW0YZHIxkZPgXpiOgELXx4KuJD3Dt4esz7XKJN0fEMExjQIDqsiDR/eb2c3WBMIfPInniRAAu5eMgELZwMRz18iMxuckpbG8nW2B/sJhZIz+7Hkbz1lF/Eaad8OxLSYmezYT0TLmmuND5BggYtfJic2iScctkMSfNAvqe8RrJzd9UR6y97VOVDxDWIVB/zmnMLBW0nl28cjjr4EPKqVLQ2F+KtwXdgThukR9Pe2pFsH8Znq8IxVe7zIr52biEBZ1nQZQoRaODDtPKrMMW5IJkKyEgN863xLy1+CBesyHQHF3CjQLe0ZJUz+YNaHBYVfBgfLFV3ygtEqjU0xg2AZdlHkhc7KSStL2rB0CPOEOZhT9VomXVZFSr4MDH7DHX3gOfK62td4yQ+4vSayvEx9TjMhcYPgXUuOjMUpGseADERMV2ihDvEQjwf0s926yW1LTTxXUZETthNIh8i2+KosxV/dh+yYwULRP/791+lWneA+MGl7CTkBiMx3mVaAjV7ZC3LCjvPAT2ngqwbNRZBEBMR+c2qIdPGEM+Hyk5CbtBi8XGncTElgJUPN+3wfYfPV/n5XP7+JmH54nzYF7iNlOw4VNpawhVFND5sCE182GYtw/gQ0Ip1CPfX2QyX1yyvZFm+nqUeJFqouw+OJSZpfNgQmviwDRcl/OU/wYfZ68vdLzYirykgCYfkAB2Y3R0nNOPDhlATP2yWBW3xw5GSf5h9ljiv0OZKyKUtyzfDvOZYygRyd4MWiOdDNfZhMyKquv2hI7JUkZZ/2OuSI7JutDlzG1msid8NFP+urS/zwccu8Ct7eknLLXJxO0pRrtcK6SgZV/5he8eZzFdIcI9j2/5EaHSveRqhYkZHIcTzoYf8/MOWqxipYaJmL8AamfZeUf5hS0qkkiHq3XMpHrSwcq5aT3O8gAo+TNjz0/1cHaVxXz9T+HpBatVgOxLL9i/zXEuXlBI/sUbCYRwlO1aSpRXCJEmo4MPcwfLUmO5zjOqeIvdkQjoksq9R8f5l/9B6t3gXnoqPpcE4TVjualy85vU7B5mT5+8XpSMmC6xFJ1XwoZ1vs4Gdf8i1Xy/r/lOEBl3XFDY+6M7fbrNPzShgHXdtCy16qnCZNfAhlJKqorW5QDtTWiYA0+LA/uc73wa9OnkRx3j3gzvmP6aYCY+TgiYE3muHWJLscpihnZyjVZy8pIEPPYBRI+rS+epIDmEVVwYgxH5o/POef+gaKusW9XpzDSH7up7Xn8UMz3GjhxI+hAxynakmRLRKxqiHKRBKXUKtcf4heJ81+feTwLKvN1TfLUF33wJtsiWrQgkfeiDz4J8JJKLbWiXPEcjwOYnD1zrvC3c/uS9VRzv0kE+6eM372RZckZFMiYr4EGxtUWWuCnAUyGwQ0Bg7KXzF8w/xPSy8jjPSIJHZrUH6EMJsYDFcZWUGcxTx4QgPor9hJWbs7Ha9LGd5JSMd+jwmXPX+lCzHmcXaKT96q9NWxG0795pNcnHGxnHNFMaHqE8xMcCfWF7JGrnuw31Z0T09K/gfOaun9v0pWR5o+TAp20FMKTOX7EuVVQxXcTkz9ajwsJSsCE93BqgMyj4L9/mwp6a+wzLn+rpn4YemrkfKOVrcF4CcqL9qVfLSVVHkcIMuO1vJXvPvUB3CUA0Z5vtzesM74y6ZvPD9cZXHDpX87T2e1p5Meie/f1CXLOXZjKx7hliD8j+8NuEc+ijgzeYlp/gS6KXN/XpZO53JDiAWOUw0yFJg3svREGbbmYho5kAvndzIuteQNDYhskxDJaGSxmVus7zb/bBTXhmAvV3N7l+uTYnsV3/yAswP3xdPi05uJpouy1IE2WxS6BIqIUizgINXv96VZepoiMoa3r/8nRuUyOp0hDT6hvGxTNeDL6qgxI2eaOHDfWhdPiU23qCkQv3i8vIC7nprfd9o1gaWC8xgvFcq1wM50Vq4Tu67SQUfnoXWvZEguLWb+zjZW3FFyesrPvD73L+ctZqfrI5wTzm/qIeG/XfVc4RK5DAsI5m97xKzGcROQB19HMJibl/x6ZGZgYVm8cN92+L6dp72ic5cchLeCveH8p4jVC5n24vkD5x4NObn06uWUxtRre1atXv6vetu+Z1LTj9rGD/cwOtb+eEPJYev9kLZhllPlXJYMXKdRPeyRQQOTU+tLcNWHCQdLCOWFcu2zCT35lTPxs9LLH9UYyyzUdopx+mDiIPEu9UNje7+S3yikendX6g5rt0ZQELAZ4OQliaBGJds8PIade2F7EXnezhxOsQV+LzArRBjxsHd6Z9asvQb2zCp9Sm5Ac8SlQ4M0N5clObjPMIratDD5UTT2sJ+YuoEzyrLiaz717f3BsaVHzj7py+13ryzRAbCpsVkIrRMOOsuWfLMGMj3soMpqCXSOGTByxYIpGphsoRpftwyQGWR5uMYDAaDwWAwGAwGg8FgMBgMBoPBYDAYDAaDwWAwGAwGg8FgMBgMBoPBYDAY/ib+B2eP9zBlbmRzdHJlYW0NCmVuZG9iag0KNiAwIG9iag0KPDwNCi9UeXBlIC9QYWdlDQovUGFyZW50IDMgMCBSDQovUmVzb3VyY2VzIDQgMCBSDQovQ29udGVudHMgNyAwIFINCi9NZWRpYUJveFsgMCAwIDYxMi4wIDc5Mi4wIF0NCi9Dcm9wQm94WyAwIDAgNjEyLjAgNzkyLjAgXQ0KL1JvdGF0ZSAwDQo+Pg0KZW5kb2JqDQo3IDAgb2JqDQo8PCAvTGVuZ3RoIDI5OTggL0ZpbHRlciAvRmxhdGVEZWNvZGUgPj4NCnN0cmVhbQ0KeJytW99z2zYSfveM/wc83bhzDkv8JvJmx3aaTJPmbPfmOtMXRqJtXiTSJaWmvr/+PpCgREqAIjJNJkqs5WKx2P12PwDMH0SJKCExftM4UpIIQbQQkZaKzJbkx3fLmFyV5F+nJxEjkSCRIrdvyebf1ePpyeX96QltR8AYCUbrBrhfnp78eENPTwhl5P7h9OTsU/pSlYvFD/f/PT2JGx2M1v7DDtUNwxIRGTsRFTHjhmF2mHgzTL3In5thOh2hecRgVAsTCUF7xnWn9JgNNGQcR2Lw/BltHsDPGt5xSiKdiMbh4Td2rtdwG35iyTSVkaISs8N4khj7UWXkwevicLXgX8IwQBLFtO+om/L18nlRvmTDaVMZR4lslJjUHj+fSmgVX7LqnHyoIvKhfEqXy2xOPqZfs81f4xzllMFYz1MWxWM85RKKNrW+4WmF2S2zSUHAWlI1IQiJimKloY/MdTM7ezfPilW+eiEf18vPWeVd/thgCTzLnxjKKJWMAwti2ipvXBm7ykpGWqmBL4NV/qV6TIv8f+kqL4sheFhsTQWd4rGOY8rJK/LT7bsP5O6lXmXLekqclFGRpmp8nLSOEgN1E3F1ACoHQmZ1VezxjlJqOJWTgrX1Z2ywtIyEVmGPUOTIZVrntS9SQV/ub94C8cXqafFC7tJFWr1MilKiI6YmRImii1CUNJWYyPhS8Odytp9+mwBB2Zd+1qn7p4zclrMv5HKdL+Z58TgtXBvHxoYrSSJ+wK+3VTrP/JEKONVoEDMpPBYMU0BkwyNR7VQCRV/SvS8/k/t8tfC2HKulfUl3UaQLlITXqA7TakMXnY1fo6MTR4bLsF+fyjoPVb2gW5QqkURbn4jzc1LIFPJnSn+ylEoh8TT1d86fcgxzla78EYOWlyQw+up9WrxiKOrTIrVxZ2ykLN1iB/z5pcofc6wz8TvWxexvd8yFSSbA94Qwaay3AbAUBcA8bl3Udf5YLMErDnUoaCd/e4fauDQaVDJisQz71Gfzu/EJeWJL+U1Zz57yIidvq3L93LWrSeGy7YaKLlxAaxKzIwOmIs6xOAwDHOITF/N5ldW1N1xQ5r4kvFtVWdYF+jURXmV4wOBCQzidQtnyX1I+kJu0Wr4m/y7Lqn4uwdpvy3ROvONwsEXN23HeYpjiKf0z8/dXBvaduK3O3frzuvpsTV7l9arKZyjfF4hDWWTn5L78WljRG5Dg1+RN+pz5bVN0w8SRZqvjf4g20GgeQh1eAdtvynn2GpROTWPI27A3KT0m7Bx7SpCnbwa+8ga+y+5Q4Lki77PiS17UZUHaoHr1+7H/lFblV3Kd1v5H++HV2LK2C2aQu5HBUkQJ2JZdsOE33YJJyzKJAq9PFOSSoBjImLq1clJXyyHF9izB4gykjjFBivRB+R5Iux1hZAuLdiJ4F7bZCgMmW2HAYiMMG9RMNORBcpQrK9+bjNiVRbpZL6SB3VvYVRx8YRdR2OnLqB2XYK78uDyT9mxBYnNpBA9WTvKueCir5ZYQRwZumwR/jItq7+dN6cOU2upFtR49JZ0g710+fcqqvJyT62K+bbRHTYGhg0+aA2N6dxL36V+kncjx9jkyZpJ9EYu9RUhfmrZ8U2V/rLNi9rJzSGSfVyaSzHOmxOmrD2ll2YYZaDk3e2ruaGlnHv0H3qSLrJinVdsP27XQSP7tR5Ogg2963VAjKxSsJkkDHOxKJetB9YCYMbojphqVpBNzkxwSO9MIRpx4sNfT9AsNhycBmT0GpBNBi8BJMw21trprT2uwmToZsYI2ZzNTICt4JIXZouVula7W9Ui0TrLv8nh3ArfZQ1YBKhn5WI4E7aRpOLT0p2FPR/Y2B+1ycWSNrzl/tIFb+HDa0ziLOY1NLKmRPsD2n7Tgv7jdgn8sWjkDjMQGjnQHrQFxh9atuIEj3UFrQOxMSwNS4WuVukUOWFJA6Mb1CxFcud+BjwQslU1NmgJYuCi53Afs3Xq5TKsXy6ddlR+H2Vg1hW5cslKUO4PyIbFx17HsyLGlvddpVeTF41joTpoGB3FMqPZN4yqbr2e2go2YSAPeSRORSF97MeOZyEfseRCY4a4BbEtYqhbHmx493OEn5wy5wtlAjSuwgcbKVu0MT9q9wbD1Sm72DJxRc24wgqRTkCyM3VeF2m5A6nC8lfqabkDq4IZVkDHzY9Fp+nEKpsx5QNEGWek94XEgFhqtUxCm7ZnnWBgLLSLKPGx5PGzsVBS6BSp8PLrfCiUjZlxiXGX1rMqfx/V7hig7+/gctRIs4RGndDiJi2W5Llb7rU5IZFTs4aVv0vqpf/y/GR3EV9Gh4hmj5zH26DuAag0APmiHDrNV+me2IHezp2w57LssYchENXxcnzP0zG7DahN/+9Hkz+CbHURhlx7rZgvIAXUhOlhQ9N+glEm6o8wspPkQGIgIU02Kqn3UONWAFNkN7wJCKSJsa0NSe0DN9oRHoIqz9l7eIUvC4TH5xLEE9ogkhK1JvcBNqc1wati0KX03zGxoNzAbg3LEhKpvY6ybaAhl4OEPD+RiNttHpwDBMjs4E0J2cNi10MfNp4vfrncG401rGzwlznmCviq8w6HDgRM5v+bLbH4xn//TP8H+o8wEJ8jj5myprQP2ijGb5zN08It8Tq4D0+3r0HPb0QKDMyQQ59vBb7NVXmXtpnxdhA309ei5bbeJ9Bqgdo/povDruxvifa9isyT9pynWXLNxJUxS1kAeNdVeKwHsKm4P5Qa0oBE3vV2ZSNNN52dN4T0sdTVMWo+31cQJuxrmlzJ73sZCUmc2IHX1zy/s6p9f6uqfX8jtAYEIzhj7kT3ZcXyEG4GdgZpISLgBp/SdEd9UoCMZucyK7CFf1SRFkgJizQ1j2p7qjeIq9qZFo39OICs8sWtnvpOsdBOYylYGswixFa6TzUXAkK2UxapKZyt7zYEtPXnOKvJTua52OEbStPr+KGcUABfGZwo50+01esPbUetm/O0h276Bni7FpmAaheEqiRh4dEdS+IDChKSOwvTEHgpjo4XOEqAanW5ACkSwJKirVXNOHiAxMdKD7usez2I2eJxIY4KI/O3+yjbi9XK9aM7mavIPcgn+W8yy8cSmQ8NUZvP9mLQR32JyArX5FiC7mYYgyZVlLelf6edFhno2K5cZub15591IDxDp30xszPWgJSiabL8bkwao+ef13osaXWseQnPbmnfNKICo29nvmqmOMNPXP2BGAg7d6b2I8U2fFR2w0tKXgfohfoSiFEn3HuwZT2g8ZF+3nSm/lb7yQSuoQ4brbtEA886KPfF9U+GHFfm1dm+O7i1ZX1vZq0u/EaZR7FwAue7lmNtcXiwW5VeLWr8zffXB/nLXDrXHJ7xbMti5qGt72LSuZk8pnCA3l34/+ooM7N1o//hxA7Mu9JR8yooake4I6+GY9LUPEVceb9+8sJHXO2a+Efq+Njvn9kbEu19gqHNGb0IfsybkKKneYYdP97chYxkyMyCFoj2kRmtJxM4ReF8ODaXpgBh+Q8wFAzcI8WDO0IFoiJHyGFstFiKzm/7rlXbTCuh23dkvRfuPk6BdziMTB0fuOrtfSrETDZllxmzaske1IwV+et6RgoBDJvJsCo5j70xjb8/FlCsBpm149T5RcEfO7qWXXnk+lqszBS6EIFDe5PDYeYFPiqR7GcbdPH/IVk9jbr+p1g08ps6EJg3NH8zlMi2+kMsKpfepeTtnBGmy75ROnwyn6BzDubgTlf6LcsdFR9j/AvIdEXLkaTiZwG6GSftCutinTld5lc1W5Cp7tu+c7txttAvfU0WTiZXcqf7tkvSf+k/7yzDBvRxs8PDuZcbR+xa3zcYS+G8celLfTUdIbPNVBoe2+cPCYmc5Yc0bX94rC1snWBISIhsEOrpf2BideNnBuH3FQE8qT0hNnuxXpw9ZXaeP2e9n9e8/eFKO8UgnZj/lPpakr0lukLLzKZvW9lJK7V0bdwEOiJ0ytxfHgTunVlUGhNzeR+td4f8BrDZa5g0KZW5kc3RyZWFtDQplbmRvYmoNCjEgMCBvYmoNCjw8DQovVHlwZSAvQ2F0YWxvZw0KL1BhZ2VzIDMgMCBSDQo+Pg0KZW5kb2JqDQoyIDAgb2JqDQo8PA0KL1R5cGUgL0luZm8NCi9Qcm9kdWNlciAoT3JhY2xlIEJJIFB1Ymxpc2hlciAxMC4xLjMuNC4yKQ0KPj4NCmVuZG9iag0KMyAwIG9iag0KPDwNCi9UeXBlIC9QYWdlcw0KL0tpZHMgWw0KNiAwIFINCl0NCi9Db3VudCAxDQo+Pg0KZW5kb2JqDQo0IDAgb2JqDQo8PA0KL1Byb2NTZXQgWyAvUERGIC9UZXh0IF0NCi9Gb250IDw8IA0KL0YxIDggMCBSDQovRjIgOSAwIFINCj4+DQovWE9iamVjdCA8PCANCi9JbTAgNSAwIFINCj4+DQo+Pg0KZW5kb2JqDQo4IDAgb2JqDQo8PA0KL1R5cGUgL0ZvbnQNCi9TdWJ0eXBlIC9UeXBlMQ0KL0Jhc2VGb250IC9IZWx2ZXRpY2ENCi9FbmNvZGluZyAvV2luQW5zaUVuY29kaW5nDQo+Pg0KZW5kb2JqDQo5IDAgb2JqDQo8PA0KL1R5cGUgL0ZvbnQNCi9TdWJ0eXBlIC9UeXBlMQ0KL0Jhc2VGb250IC9IZWx2ZXRpY2EtQm9sZA0KL0VuY29kaW5nIC9XaW5BbnNpRW5jb2RpbmcNCj4+DQplbmRvYmoNCjEwIDAgb2JqDQpbIDYgMCBSIC9YWVogMzYuMCAyMDMuMjYgbnVsbCBdDQplbmRvYmoNCjExIDAgb2JqDQpbIDYgMCBSIC9YWVogMzYuMCAyMDMuMjYgbnVsbCBdDQplbmRvYmoNCnhyZWYNCjAgMTINCjAwMDAwMDAwMDAgNjU1MzUgZg0KMDAwMDAwNjM2NyAwMDAwMCBuDQowMDAwMDA2NDIyIDAwMDAwIG4NCjAwMDAwMDY1MDQgMDAwMDAgbg0KMDAwMDAwNjU3MiAwMDAwMCBuDQowMDAwMDAwMDEwIDAwMDAwIG4NCjAwMDAwMDMxMzIgMDAwMDAgbg0KMDAwMDAwMzI5MCAwMDAwMCBuDQowMDAwMDA2Njg5IDAwMDAwIG4NCjAwMDAwMDY3OTQgMDAwMDAgbg0KMDAwMDAwNjkwNCAwMDAwMCBuDQowMDAwMDA2OTU1IDAwMDAwIG4NCnRyYWlsZXINCjw8DQovU2l6ZSAxMg0KL1Jvb3QgMSAwIFINCi9JbmZvIDIgMCBSDQovSUQgWzxhMjllYTY3NGFmMGI5N2FhNTBkOGFhMDg1YmFmNjVkYz48YTI5ZWE2NzRhZjBiOTdhYTUwZDhhYTA4NWJhZjY1ZGM+XQ0KPj4NCnN0YXJ0eHJlZg0KNzAwNg0KJSVFT0YNCg==";
            fetch('data:application/pdf;base64,' + this.downloadedBlob,
              {
                method: "GET"
              }).then(res => res.blob()).then(blob => {
                setTimeout(() => {
                  this.file.checkFile(this.file.externalApplicationStorageDirectory, name).then(
                    (files) => {
                      /*this.file.removeFile( this.file.externalApplicationStorageDirectory,name).then((res) => {
                        this.saveFile(blob);
                      }).catch(err => {
                        this.utils.closeLoading();
                        this.utils.showAlert("Error in opening payslip pdf", this.constants.COMMON_APP_MESSAGE.ERROR);
                      });*/
                      setTimeout(() => {
                        this.fileOpener.open(
                          this.file.externalApplicationStorageDirectory+name,
                          'application/pdf'
                        ).then((res) => {
          
                        }).catch(err => {
                          console.log("open error");
                          this.utils.showAlert("Error in opening payslip pdf", this.constants.COMMON_APP_MESSAGE.ERROR);
                        });
                       }, 500);
                    }
                  ).catch(
                    (err) => {
                          //this.saveFile(blob);  
                          this.file.writeFile(this.file.externalApplicationStorageDirectory, name, blob, { replace: true }).then(res => {
                            this.utils.closeLoading();
                            setTimeout(() => {
                            this.fileOpener.open(
                              res.toInternalURL(),
                              'application/pdf'
                            ).then((res) => {
                    
                            }).catch(err => {
                              console.log("open error");
                              this.utils.showAlert("Error in opening payslip pdf", this.constants.COMMON_APP_MESSAGE.ERROR);
                            });
                          }, 500);
                          }).catch(err => {
                              this.utils.showAlert("Error in saving payslip pdf", this.constants.COMMON_APP_MESSAGE.ERROR);
                                console.log("save error "+err);     
                        });
                    }
                );
          }, 500);
              }).catch(err => {
                this.utils.closeLoading();
                this.utils.showAlert("Error occured", this.constants.COMMON_APP_MESSAGE.ERROR);
                     console.log("error");
              });
    }

getPayslipBlobDataFromServer(){
  var payslipObjs = PayslipListModels.getPayslipDataList();
      this.storage.get("UserInfoObject").then((_userData)=>{
        if(_userData != null){
          //object = UserdataModel.fromJSON(_userData);
          this.userName = _userData.UserName;
          this.personId = _userData.Staffid;
        }
      });
      setTimeout(() => {
        this.options = {
          "RESTHeader" : {
            "xmlns" : "http://xmlns.oracle.com/apps/fnd/rest/header",
            "Responsibility":"GLOBAL_HRMS_MANAGER",
              "RespApplication":"PER",
              "SecurityGroup":"STANDARD",
              "NLSLanguage":"AMERICAN",
            "Org_Id" : 81
          },
          "InputParameters" : {
            //"personId": "546749",  
            "personId": ""+this.personId,
            "legCode": "ZA",  
            "bgID": "107",
            "actContextId": this.actionContextId
            //"actContextId": payslipObjs[this.selectedIndex].ActionContextId
            //"actContextId": "754197223"
        }
      }
      this.networkService.isNetworkConnectionAvailable()
      .then((isOnline: boolean) => {
      if (isOnline) {
        this.utils.presentLoading();
        this.networkService.getPayslipBlobData(this.options, this.authToken).then((data) => {
          var _data = JSON.parse(JSON.stringify(data));
          this.utils.closeLoading();
          this.downloadedBlob = _data.getPayslipPDFBlob_Output.OutputParameters.Output.PayslipPDFBlobBean[0].PayslipPDFBlob;
          setTimeout(() => {
            this.downloadBlobToPDF();
           }, 500
          );
        }, (err) => {
          console.log(err);
          this.utils.closeLoading();
          this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.SERVER_DATA_ERROR, this.constants.COMMON_APP_MESSAGE.ERROR);
        });
  
      } else {
        this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.NO_NETWORK_TITLE,  this.constants.COMMON_APP_MESSAGE.NO_NETWORK_MESSAGE);
        }
      });
    }, 500);
  
    }

    saveFile(blob){
      this.file.writeFile(this.file.externalApplicationStorageDirectory, name, blob, { replace: true }).then(res => {
        this.utils.closeLoading();
        setTimeout(() => {
        this.fileOpener.open(
          res.toInternalURL(),
          'application/pdf'
        ).then((res) => {

        }).catch(err => {
          console.log("open error");
          this.utils.showAlert("Error in opening payslip pdf", this.constants.COMMON_APP_MESSAGE.ERROR);
        });
      }, 500);
      }).catch(err => {
          this.utils.showAlert("Error in saving payslip pdf", this.constants.COMMON_APP_MESSAGE.ERROR);
            console.log("save error "+err);     
    });
    }

}
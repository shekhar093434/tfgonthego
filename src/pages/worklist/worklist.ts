import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {LeaveRequestPage} from '../leave-request/leave-request';
import {SummarylistPage} from '../summarylist/summarylist';
/**
 * Generated class for the WorklistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-worklist',
  templateUrl: 'worklist.html',
})
export class WorklistPage {

  hideMe:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }



    

  ionViewDidLoad() {
    console.log('ionViewDidLoad WorklistPage');
  }

  goBack(){
    this.navCtrl.popToRoot();
  }
  leaverequest(){
    this.navCtrl.push(LeaveRequestPage);
  }
  hide() {
    this.hideMe = true;
  }
  summarylist(){
    this.navCtrl.push(SummarylistPage);
  }
  

}

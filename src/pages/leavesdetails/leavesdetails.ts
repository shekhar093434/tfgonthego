import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CalendarPage } from '../calendar/calendar';
import { DurationCalPage } from '../duration-cal/duration-cal';
import { ApplyLeavePage } from '../apply-leave/apply-leave';
import { PayslipPage } from '../payslip/payslip';
import { LeaveDetailsModel } from '../../models/LeaveDetailsModel';
import { NetworkProvider } from '../../providers/network/networkcalls';
import { Utills } from '../../utilities/common';
import { Constants } from '../../utilities/constants';

/**
 * Generated class for the LeavesdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-leavesdetails',
    templateUrl: 'leavesdetails.html',
})
export class LeavesdetailsPage {
    monthSelected: string;
    currentDate: string;
    date: string;
    leaveDetailsObjs: any;
    annualLeave: any;
    sickLeave: any;
    parentalLeave: any;
    longLeave: any;
    familyResponsibilityLeave: any;
    constructor(public navCtrl: NavController, public navParams: NavParams, public networkService: NetworkProvider, private utils: Utills, public constants: Constants) {
    }

    formatDate() {
        var today = new Date();
        this.currentDate = today.getDate() + " " + (+1) + " " + today.getFullYear();
        var day = today.getDay();
        var daylist = ["Sun", "Mon", "Tues", "Wed", "Thur", "Fri", "Sat"];
        switch (today.getMonth()) {
            case 0:
                this.currentDate = today.getDate() + " " + daylist[day] + ", Jan" + "'" + ("" + today.getFullYear()).slice(2, 4);
                break;
            case 1:
                this.currentDate = today.getDate() + " " + daylist[day] + ", Feb" + "'" + ("" + today.getFullYear()).slice(2, 4);
                break;
            case 2:
                this.currentDate = today.getDate() + " " + daylist[day] + ", Mar" + "'" + ("" + today.getFullYear()).slice(2, 4);
                break;
            case 3:
                this.currentDate = today.getDate() + " " + daylist[day] + ", Apr" + "'" + ("" + today.getFullYear()).slice(2, 4);
                break;
            case 4:
                this.currentDate = today.getDate() + " " + daylist[day] + ", May" + "'" + ("" + today.getFullYear()).slice(2, 4);
                break;
            case 5:
                this.currentDate = today.getDate() + " " + daylist[day] + ", Jun" + "'" + ("" + today.getFullYear()).slice(2, 4);
                break;
            case 6:
                this.currentDate = today.getDate() + " " + daylist[day] + ", Jul" + "'" + ("" + today.getFullYear()).slice(2, 4);
                break;
            case 7:
                this.currentDate = today.getDate() + " " + daylist[day] + ", Aug" + "'" + ("" + today.getFullYear()).slice(2, 4);
                break;
            case 8:
                this.currentDate = today.getDate() + " " + daylist[day] + ", Sep" + "'" + ("" + today.getFullYear()).slice(2, 4);
                break;
            case 9:
                this.currentDate = today.getDate() + " " + daylist[day] + ", Oct" + "'" + ("" + today.getFullYear()).slice(2, 4);
                break;
            case 10:
                this.currentDate = today.getDate() + " " + daylist[day] + ", Nov" + "'" + ("" + today.getFullYear()).slice(2, 4);
                break;
            case 11:
                this.currentDate = today.getDate() + " " + daylist[day] + ", Dec" + "'" + ("" + today.getFullYear()).slice(2, 4);
                break;

        }
        console.log(this.currentDate);
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad LeavesdetailsPage');
    }

    payslip() {
        this.navCtrl.push(PayslipPage);
    }

    goBack() {
        this.navCtrl.popToRoot();
    }
    monthPopup() {
    }

    // public ionViewWillEnter() {
    //     this.date = this.navParams.get("date");
    //     console.log(this.date); // ==> "yourValue"
    //     if (this.date != null && this.date != undefined && this.date.length > 0)
    //         this.currentDate = this.date;
    //     else
    //         this.formatDate();
    // }

    public ionViewWillEnter() {
        this.date = this.navParams.get("date");
        console.log('ionViewWillEnter  LeavesdetailsPage');
        console.log(this.date); // ==> "yourValue"

        if (this.date != null && this.date != undefined && this.date.length > 0) {
            this.currentDate = this.date;
        } else {
            this.formatDate();
            console.log(this.date);
        }
        this.getLeaveBalances();
        setTimeout(() => {
        this.leaveDetailsObjs = LeaveDetailsModel.getLeaveDetails();
        if (this.leaveDetailsObjs != null && this.leaveDetailsObjs != undefined) {
            this.annualLeave = this.leaveDetailsObjs.annualLeave;
            this.sickLeave = this.leaveDetailsObjs.sickLeave;
            this.parentalLeave = this.leaveDetailsObjs.parentalLeave;
            this.longLeave = this.leaveDetailsObjs.longLeave;
            this.familyResponsibilityLeave = this.leaveDetailsObjs.familyResponsibilityLeave;
        } else {
            console.log('Service Call Data is not loading');
        }
    }, 800);
    }


    selectDate() {
        this.navCtrl.push(CalendarPage);
    }

    applyLeave() {
        this.navCtrl.push(ApplyLeavePage);
    }

    getLeaveBalances() {
        this.networkService.isNetworkConnectionAvailable()
        .then((isOnline: boolean) => {
            if (isOnline) {
                this.networkService.getLeaveBalances().then((leaveBalances) => {
                    var _leaveBalances = JSON.parse(JSON.stringify(leaveBalances));
                    LeaveDetailsModel.fromJSON(_leaveBalances);
                })
            } else {
                this.utils.showAlert(this.constants.COMMON_APP_MESSAGE.NO_NETWORK_TITLE, this.constants.COMMON_APP_MESSAGE.NO_NETWORK_MESSAGE);
            }
        });
    }

}

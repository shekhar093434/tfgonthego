import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LeavesdetailsPage } from './leavesdetails';

@NgModule({
  declarations: [
    LeavesdetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(LeavesdetailsPage),
  ],
})
export class LeavesdetailsPageModule {}

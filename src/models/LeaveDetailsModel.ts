export class LeaveDetailsModel {
    annualLeave: string;
    sickLeave: string;
    parentalLeave: string;
    longLeave: string;
    familyResponsibilityLeave: string;

    private static leaveDetailsModelObj: LeaveDetailsModel;

    public static getLeaveDetails(){
        return this.leaveDetailsModelObj;
    }

    public static fromJSON(json: any): LeaveDetailsModel {
        let object = Object.create(LeaveDetailsModel.prototype);
        Object.assign(object, json);
        this.leaveDetailsModelObj = object;
        return object;
    }
}
export class UserProfileData {
    PersonId: any;
    Language: string;
    LastName: string;
    FirstName: string;
    PersonImage: any ="";
    WorkPhone: string;
    EmailAddress: string;
    Location: string;
    Organization: string;
    Directs: string;
    Total: string;
    SupervisorId: string;
    SupervisorLastName: string;
    SupervisorFirstName: string;
    Tags: string;
    Address: string;
    Notes: string;
    BusinessGroupId: string;
    PartyId: string;
    LocalTime: string;
    Sm1id: string;
    Sm1url: string;
    Sm2id: string;
    Sm2url: string;
    Sm3id: string;
    Sm3url: string;
    LocationAddress: string;
    constructor(){
    }

    private static userProfileData: UserProfileData[] = [];

    public static fromJSON(json: any): UserProfileData[] {
        if(json != null && json != undefined ){
            if(json.PersonDataBean != null && json.PersonDataBean != undefined){
                this.userProfileData.splice(0, this.userProfileData.length);
                for(var i=0; i< json.PersonDataBean.length; i++){
                    let object = Object.create(UserProfileData.prototype);
                    Object.assign(object, json.PersonDataBean[i]);
                    this.userProfileData.push(object);
                }
            }
        }
        return this.userProfileData;
    }
    
    public static getProfileData(){
        return this.userProfileData;
    }

    public static resetProfileData(){
        this.userProfileData.splice(0, this.userProfileData.length);
    }

}
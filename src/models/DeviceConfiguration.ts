
export class DeviceConfiguration{
    version: string;
    platform: string;
    screenorientation: string;
    devicetype: string;
    deviceWidth: any;
    deviceHeight: any;
    isTouchIDEnabled: boolean = false;
    isFaceIDAvailable: boolean = false;
    private static deviceConfiguration: DeviceConfiguration = new DeviceConfiguration();
    constructor() { 
    }

    public static updateDeviceConfiguration(_version: any, _platform:any, _screenorientation: any, _devicetype: any, _deviceWidth: any, _deviceHeight: any): DeviceConfiguration {
        this.deviceConfiguration.version = _version;
        this.deviceConfiguration.platform = _platform;
        this.deviceConfiguration.screenorientation = _screenorientation;
        this.deviceConfiguration.devicetype = _devicetype;
        this.deviceConfiguration.deviceWidth = _deviceWidth;
        this.deviceConfiguration.deviceHeight = _deviceHeight;
        return  this.deviceConfiguration;
    }

    public static setTouchIDAvailabilityStatus(status: boolean){
        this.deviceConfiguration.isTouchIDEnabled = status;
        if(status)
            localStorage.setItem("TouchID", "true");
        else
            localStorage.setItem("TouchID", "false");
    }

    public static setFaceIDAvailabilityStatus(status: boolean){
        this.deviceConfiguration.isFaceIDAvailable = status;
        if(status)
            localStorage.setItem("FaceID", "true");
        else
            localStorage.setItem("FaceID", "false");
    }

    public static getTouchIDAvailabilityStatus(){
        //return this.deviceConfiguration.isTouchIDEnabled;
        return localStorage.getItem("TouchID");
    }

    public static getFaceIDAvailabilityStatus(){
        //return this.deviceConfiguration.isFaceIDAvailable;
        return localStorage.getItem("FaceID");
    }

    public static getDeviceConfiguration(): DeviceConfiguration{
        return this.deviceConfiguration;
    }

    public static fromJSON(json: any): DeviceConfiguration {
        let object = Object.create(DeviceConfiguration.prototype);
        Object.assign(object, json);
        return object;
    }
}
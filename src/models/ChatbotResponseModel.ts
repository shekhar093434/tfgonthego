export class ChatbotResponseModel {
    queryText: string;
    action: string;
    allRequiredParamsPresent: string;
    fulfillmentText: string;
    private static chatbotResponseObj: ChatbotResponseModel;

    public static fromJSON(json: any): ChatbotResponseModel {
        //this.objArr.splice(0,this.objArr.length);
        let object = Object.create(ChatbotResponseModel.prototype);
        Object.assign(object, json);
        this.chatbotResponseObj = object;
        return object;
    }
}
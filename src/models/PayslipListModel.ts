export class PayslipListModels {
    EffectiveDate: string;
    ActionContextId: string;
    PersonId: string;
    Month: string;
    Year: string;
    private static payslipDataList: PayslipListModels[] = [];
    constructor(){
    }
    public static fromJSON(json: any): PayslipListModels[] {
        if(json != null && json != undefined ){
            if(json.PayslipListBean != null && json.PayslipListBean != undefined){
                this.payslipDataList.splice(0, this.payslipDataList.length);
                for(var i=0; i< json.PayslipListBean.length; i++){
                    let object = Object.create(PayslipListModels.prototype);
                    Object.assign(object, json.PayslipListBean[i]);

                    let str: string;
                    var d = new Date(json.PayslipListBean[i].EffectiveDate);
                    var n = d.getMonth();
                    var y = (""+json.PayslipListBean[i].EffectiveDate).slice(0,4);
                    switch (n) {
                        case 0:
                            str = "January";
                            break;
                        case 1:
                            str = "February";
                            break;
                        case 2:
                            str = "March";
                            break;
                        case 3:
                            str = "April";
                            break;
                        case 4:
                            str = "May";
                            break;
                        case 5:
                            str = "June";
                            break;
                        case 6:
                            str = "July";
                            break;
                        case 7:
                            str = "August";
                            break;
                        case 8:
                            str = "September";
                            break;
                        case 9:
                            str = "October";
                            break;
                        case 10:
                            str = "November";
                            break;
                        case 11:
                            str = "December";
                            break;
            
                    }
                    object.Month = str;
                    object.Year = y;
                    this.payslipDataList.push(object);
                }
            }
        }
        return this.payslipDataList;
    }
    public static getPayslipDataList(){
        return this.payslipDataList;
    }

    public static resetPayslipDataList(){
        this.payslipDataList.splice(0, this.payslipDataList.length);
    }

}
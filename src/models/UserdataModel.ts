export class UserdataModel {
    
    Authenticated:boolean;
    UserName: string;
    Password: string;
    Staffid: string;
    SecretKey: string;
    isOracleUser: boolean;
    isAutoLogin: boolean;
    private static userDataObj: UserdataModel = new UserdataModel();
    constructor() { 
    }  

    public static updateUserInfo(_authenticated, _username, _password, _isOracleUser, _staffid, _secretkey){
        this.userDataObj.Authenticated = _authenticated;
        this.userDataObj.UserName = _username;
        this.userDataObj.Password = _password;
        this.userDataObj.isOracleUser = _isOracleUser;
        this.userDataObj.Staffid = _staffid;
        this.userDataObj.SecretKey = _secretkey;
    }

    public static getUserInfo(): UserdataModel {
        return this.userDataObj;
    }

    public static setAutoLogin(status){
        this.userDataObj.isAutoLogin = status;
    }

    public static fromJSON(json: any): UserdataModel {
        let object = Object.create(UserdataModel.prototype);
        Object.assign(object, json);
        return object;
    }

}
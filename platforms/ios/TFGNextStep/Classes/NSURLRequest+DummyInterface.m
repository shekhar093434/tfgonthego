//
//  NSURLRequest+DummyInterface.m
//  TFGNextStep
//
//  Created by Rahul Mahajan on 12/04/19.
//

#import "NSURLRequest+DummyInterface.h"

@implementation NSURLRequest (DummyInterface)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host{
    return YES;
}
@end

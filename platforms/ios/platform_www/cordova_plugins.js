cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
  {
    "id": "cordova-fabric-plugin.FabricPlugin",
    "file": "plugins/cordova-fabric-plugin/www/FabricPlugin.js",
    "pluginId": "cordova-fabric-plugin",
    "clobbers": [
      "window.fabric.core"
    ]
  },
  {
    "id": "cordova-fabric-plugin.FabricAnswersPlugin",
    "file": "plugins/cordova-fabric-plugin/www/FabricPlugin.Answers.js",
    "pluginId": "cordova-fabric-plugin",
    "clobbers": [
      "window.fabric.Answers"
    ]
  },
  {
    "id": "cordova-fabric-plugin.FabricCrashlyticsPlugin",
    "file": "plugins/cordova-fabric-plugin/www/FabricPlugin.Crashlytics.js",
    "pluginId": "cordova-fabric-plugin",
    "clobbers": [
      "window.fabric.Crashlytics"
    ]
  },
  {
    "id": "cordova-plugin-android-permissions.Permissions",
    "file": "plugins/cordova-plugin-android-permissions/www/permissions-dummy.js",
    "pluginId": "cordova-plugin-android-permissions",
    "clobbers": [
      "cordova.plugins.permissions"
    ]
  },
  {
    "id": "cordova-plugin-device.device",
    "file": "plugins/cordova-plugin-device/www/device.js",
    "pluginId": "cordova-plugin-device",
    "clobbers": [
      "device"
    ]
  },
  {
    "id": "cordova-plugin-fingerprint-aio.Fingerprint",
    "file": "plugins/cordova-plugin-fingerprint-aio/www/Fingerprint.js",
    "pluginId": "cordova-plugin-fingerprint-aio",
    "clobbers": [
      "Fingerprint"
    ]
  },
  {
    "id": "cordova-plugin-ionic-webview.ios-wkwebview-exec",
    "file": "plugins/cordova-plugin-ionic-webview/src/www/ios/ios-wkwebview-exec.js",
    "pluginId": "cordova-plugin-ionic-webview",
    "clobbers": [
      "cordova.exec"
    ]
  },
  {
    "id": "cordova-plugin-network-information.network",
    "file": "plugins/cordova-plugin-network-information/www/network.js",
    "pluginId": "cordova-plugin-network-information",
    "clobbers": [
      "navigator.connection",
      "navigator.network.connection"
    ]
  },
  {
    "id": "cordova-plugin-network-information.Connection",
    "file": "plugins/cordova-plugin-network-information/www/Connection.js",
    "pluginId": "cordova-plugin-network-information",
    "clobbers": [
      "Connection"
    ]
  },
  {
    "id": "cordova-plugin-screen-orientation.screenorientation",
    "file": "plugins/cordova-plugin-screen-orientation/www/screenorientation.js",
    "pluginId": "cordova-plugin-screen-orientation",
    "clobbers": [
      "cordova.plugins.screenorientation"
    ]
  },
  {
    "id": "cordova-plugin-speechrecognition.SpeechRecognition",
    "file": "plugins/cordova-plugin-speechrecognition/www/speechRecognition.js",
    "pluginId": "cordova-plugin-speechrecognition",
    "merges": [
      "window.plugins.speechRecognition"
    ]
  },
  {
    "id": "cordova-plugin-splashscreen.SplashScreen",
    "file": "plugins/cordova-plugin-splashscreen/www/splashscreen.js",
    "pluginId": "cordova-plugin-splashscreen",
    "clobbers": [
      "navigator.splashscreen"
    ]
  },
  {
    "id": "cordova-plugin-touch-id.TouchID",
    "file": "plugins/cordova-plugin-touch-id/www/TouchID.js",
    "pluginId": "cordova-plugin-touch-id",
    "clobbers": [
      "window.plugins.touchid"
    ]
  },
  {
    "id": "cordova-plugin-tts.tts",
    "file": "plugins/cordova-plugin-tts/www/tts.js",
    "pluginId": "cordova-plugin-tts",
    "clobbers": [
      "TTS"
    ]
  },
  {
    "id": "cordova-sqlite-storage.SQLitePlugin",
    "file": "plugins/cordova-sqlite-storage/www/SQLitePlugin.js",
    "pluginId": "cordova-sqlite-storage",
    "clobbers": [
      "SQLitePlugin"
    ]
  },
  {
    "id": "es6-promise-plugin.Promise",
    "file": "plugins/es6-promise-plugin/www/promise.js",
    "pluginId": "es6-promise-plugin",
    "runs": true
  },
  {
    "id": "ionic-plugin-keyboard.keyboard",
    "file": "plugins/ionic-plugin-keyboard/www/ios/keyboard.js",
    "pluginId": "ionic-plugin-keyboard",
    "clobbers": [
      "cordova.plugins.Keyboard"
    ],
    "runs": true
  }
];
module.exports.metadata = 
// TOP OF METADATA
{
  "cordova-fabric-plugin": "1.1.14-dev",
  "cordova-plugin-android-permissions": "1.0.0",
  "cordova-plugin-device": "1.1.7",
  "cordova-plugin-add-swift-support": "1.7.2",
  "cordova-plugin-fingerprint-aio": "1.6.0",
  "cordova-plugin-ionic-webview": "1.2.1",
  "cordova-plugin-network-information": "2.0.1",
  "cordova-plugin-screen-orientation": "3.0.1",
  "cordova-plugin-speechrecognition": "1.1.2",
  "cordova-plugin-splashscreen": "4.1.0",
  "cordova-plugin-touch-id": "3.3.1",
  "cordova-plugin-tts": "0.2.3",
  "cordova-plugin-whitelist": "1.3.3",
  "cordova-sqlite-storage": "3.2.0",
  "es6-promise-plugin": "4.2.2",
  "ionic-plugin-keyboard": "2.2.1"
};
// BOTTOM OF METADATA
});
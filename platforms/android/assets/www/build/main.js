webpackJsonp([4],{

/***/ 123:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserdataModel; });
var UserdataModel = (function () {
    function UserdataModel() {
    }
    UserdataModel.updateUserInfo = function (_authenticated, _username, _password, _isOracleUser) {
        this.userDataObj.Authenticated = _authenticated;
        this.userDataObj.UserName = _username;
        this.userDataObj.Password = _password;
        this.userDataObj.isOracleUser = _isOracleUser;
    };
    UserdataModel.getUserInfo = function () {
        return this.userDataObj;
    };
    UserdataModel.setAutoLogin = function (status) {
        this.userDataObj.isAutoLogin = status;
    };
    UserdataModel.fromJSON = function (json) {
        var object = Object.create(UserdataModel.prototype);
        Object.assign(object, json);
        return object;
    };
    UserdataModel.userDataObj = new UserdataModel();
    return UserdataModel;
}());

//# sourceMappingURL=UserdataModel.js.map

/***/ }),

/***/ 162:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatbotPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_speech_recognition__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__utilities_common__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_network_networkcalls__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__utilities_constants__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__models_ChatbotResponseModel__ = __webpack_require__(399);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_text_to_speech__ = __webpack_require__(228);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__models_DeviceConfiguration__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_rxjs_add_observable_interval__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_rxjs_add_observable_interval___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_rxjs_add_observable_interval__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












/**
 * Generated class for the ChatbotPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ChatbotPage = (function () {
    function ChatbotPage(keyboard, tts, toastCtrl, constants, storage, navCtrl, ngzone, viewCtrl, events, cd, navParams, alertCtrl, loadingController, speechRecognition, app, modalCtrl, platform, utils, networkService) {
        var _this = this;
        this.keyboard = keyboard;
        this.tts = tts;
        this.toastCtrl = toastCtrl;
        this.constants = constants;
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.ngzone = ngzone;
        this.viewCtrl = viewCtrl;
        this.events = events;
        this.cd = cd;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.loadingController = loadingController;
        this.speechRecognition = speechRecognition;
        this.app = app;
        this.modalCtrl = modalCtrl;
        this.platform = platform;
        this.utils = utils;
        this.networkService = networkService;
        this.keyBoardOpen = false;
        this.msgReceived = null;
        this.sendBtnVisibility = false;
        this.animationstart = false;
        this.gifloading = false;
        this.speaking = false;
        this.mute = true;
        this.chatMsg = "";
        this.rawList = [];
        this.chatMessages = [];
        this.isRecording = false;
        this.deviceDataObj = __WEBPACK_IMPORTED_MODULE_9__models_DeviceConfiguration__["a" /* DeviceConfiguration */].getDeviceConfiguration();
        console.log("Device Platform - " + this.deviceDataObj.platform);
        //this.locale = 'e-nUS';
        this.locale = 'en-ZA';
        if (this.deviceDataObj.platform === 'iOS')
            this.rate = 12;
        else
            this.rate = 6;
        //this.pushNotify = localStorage.getItem("notify");//navParams.get("secondPassed");
        this.isEnabled = true;
        //this.notifyFlag=false;
        //this.userId = localStorage.getItem("userId");
        this.myLikeImage = "assets/imgs/like.svg";
        this.myDisLikeImage = "assets/imgs/dislike.svg";
        this.myDate = new Date().toISOString();
        this.disabledBtn = false;
        //this.pushMsg();
        this.loader = this.loadingController.create({
            content: "Logging Out",
            spinner: 'bubbles'
        });
        this.loaderForm = this.loadingController.create({
            content: "Loading.."
        });
        if (this.deviceDataObj.platform === 'iOS') {
            // This will only print when on iOS
            console.log('I am an iOS device!');
            this.isEnabled = false;
        }
        else if (this.deviceDataObj.platform === 'android') {
            // This will only print when on Android
            console.log('I am an android device!');
            this.isEnabled = false;
        }
        else if (this.deviceDataObj.platform === 'Android') {
            // This will only print when on Android
            console.log('I am an android device!');
            this.isEnabled = false;
        }
        else {
            console.log('I am on Web!');
            this.isEnabled = true;
        }
        this.platform.pause.subscribe(function () {
            if (_this.speaking) {
                _this.tts.speak({ text: '' }); // <<< speak an empty string to interrupt.
                _this.speaking = false;
                return;
            }
        });
        this.sub = __WEBPACK_IMPORTED_MODULE_10_rxjs__["Observable"].interval(100)
            .subscribe(function (val) {
            console.log("Keyboard status" + _this.keyboard.isOpen());
            if (!_this.keyboard.isOpen())
                _this.keyBoardOpen = false;
            else if (_this.keyboard.isOpen())
                _this.keyBoardOpen = true;
        });
    }
    ChatbotPage.prototype.openhideKeyboard = function () {
        if (this.keyboard.isOpen())
            this.keyBoardOpen = true;
        else
            this.keyBoardOpen = true;
    };
    ChatbotPage.prototype.toggleMute = function () {
        if (this.mute) {
            this.mute = false;
        }
        else {
            this.mute = true;
            if (this.speaking) {
                this.tts.speak({ text: '' }); // <<< speak an empty string to interrupt.
                this.speaking = false;
                return;
            }
        }
    };
    ChatbotPage.prototype.startAnimationAudio = function () {
        this.animationstart = true;
        this.sendBtnVisibility = true;
        this.startRecording();
    };
    ChatbotPage.prototype.startAnimationText = function () {
        var _this = this;
        this.animationstart = true;
        setTimeout(function () {
            _this.sendBtnVisibility = true;
        }, 10);
    };
    ChatbotPage.prototype.presentToast = function () {
        var toast = this.toastCtrl.create({
            message: 'Please Say Something',
            duration: 3000,
            cssClass: "toastMessageClass",
        });
        toast.present();
    };
    ChatbotPage.prototype.closeListening = function () {
        this.isRecording = false;
        this.gifloading = false;
        this.sendBtnVisibility = false;
        this.stopListening();
    };
    ChatbotPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.storage.get("UserInfoObject").then(function (_userData) {
            if (_userData != null) {
                //object = UserdataModel.fromJSON(_userData);
                _this.userName = _userData.UserName;
                _this.welcomeMsg = "Hello " + _this.userName + ", welcome to Employee Engagement Application. This is Kiara here. I am your Virtual Assistant. You are free to chat below with me anytime for your queries :).";
                localStorage.setItem("welcomeMsg", _this.welcomeMsg);
                _this.userWelcomeMsg = localStorage.getItem("welcomeMsg");
                console.log("UserName >>" + _this.userName + "  Message >>>" + _this.welcomeMsg);
            }
        });
    };
    /*pushMsg(){
      if(this.pushNotify.length>0){
        this.notifyFlag=true;
        }
          localStorage.setItem("notify","");
    }*/
    ChatbotPage.prototype.scrollBottom = function () {
        var _this = this;
        setTimeout(function () {
            _this.content.scrollToBottom(100);
        });
    };
    ChatbotPage.prototype.startRecording = function () {
        if (!this.isRecording) {
            //this.presentToast();
            this.gifloading = true;
            this.animationstart = false;
            if (this.deviceDataObj.platform === 'iOS') {
                this.getPermission();
            }
            if (this.isEnabled == false) {
                this.startListening();
            }
            else {
                console.log("Mic ios");
                this.getPermission();
            }
        }
    };
    ChatbotPage.prototype.stopListening = function () {
        var _this = this;
        this.speechRecognition.stopListening().then(function () {
            _this.isRecording = false;
        });
    };
    ChatbotPage.prototype.startListening = function () {
        var _this = this;
        if (this.platform.is('android' || 'Android')) {
            this.getPermission();
        }
        if (this.deviceDataObj.platform === 'iOS') {
            console.log("Mic3 ios");
        }
        console.log("Mic active");
        // let options = {
        //   language: 'en-US'
        // }
        this.timeout = setTimeout(function () {
            _this.closeListening();
        }, 10000);
        this.speechRecognition.startListening({ language: this.locale, showPopup: false }).subscribe(function (matches) {
            _this.ngzone.run(function () {
                _this.matches = matches;
                _this.cd.detectChanges();
                _this.chatMsg = matches[0];
                if (_this.chatMsg.length > 0) {
                    clearTimeout(_this.timeout);
                    _this.closeListening();
                    _this.sendMessages();
                }
            });
        }, function (onerror) {
            console.log('Speech Error:', onerror);
            _this.msgReceived = true;
            clearTimeout(_this.timeout);
            _this.closeListening();
        });
        this.isRecording = true;
    };
    ChatbotPage.prototype.buttonRequest = function (formTitle, indexVal, selectedBtn) {
        localStorage.setItem("formindex", selectedBtn);
        this.chatMsg = formTitle;
        this.sendMessages();
    };
    ChatbotPage.prototype.buttonFormRequest = function (formTitle, indexVal, selectedBtn) {
        localStorage.setItem("formindex", selectedBtn);
        console.log("Temp Values" + JSON.stringify(this.tempValue));
        this.loaderForm.present();
        //this.formRequest(this.userId);
    };
    /*formRequest(dataId) {
      this.dataService.isFormPresent("fbId=", dataId).then((formData) => {
        this.responseForm = formData;
        console.log("Response form " + JSON.stringify(this.responseForm));
        this.responseForm = JSON.stringify(formData);
        localStorage.setItem('policyDetails', this.responseForm);
  
        if (this.responseForm) {
          this.loaderForm.dismiss();
          var modalPage = this.modalCtrl.create('ModalPage', "");
          modalPage.onDidDismiss(data => {
            console.log(data);
          });
          modalPage.present();
        }
      }, (err) => {
        console.log(err);
        this.loaderForm.dismiss();
      });
    }*/
    ChatbotPage.prototype.goBack = function () {
        if (this.speaking) {
            this.tts.speak({ text: '' }); // <<< speak an empty string to interrupt.
            this.speaking = false;
            //return;
        }
        this.navCtrl.pop();
    };
    ChatbotPage.prototype.sendMessages = function () {
        var _this = this;
        this.keyBoardOpen = false;
        this.keyboard.close();
        if ((this.chatMsg != null || this.chatMsg != '') && this.chatMsg.length > 0) {
            this.msgReceived = false;
            localStorage.setItem('chatMsg', this.chatMsg);
            this.chatMessages.push({
                "request": localStorage.getItem('chatMsg'),
                "old_message": false
            });
            this.scrollBottom();
            this.options = {
                "queryInput": {
                    "text": this.chatMsg,
                    "language": "en"
                },
                "userAttributes": {
                    "username": this.userName,
                    "session": this.userName,
                    "channel": "mobile"
                }
            };
            this.chatMsg = "";
            this.networkService.isNetworkConnectionAvailable()
                .then(function (isOnline) {
                if (isOnline) {
                    _this.networkService.chatQuery("query", _this.options).then(function (userInfo) {
                        if (_this.gifloading) {
                            _this.gifloading = false;
                            _this.sendBtnVisibility = false;
                        }
                        _this.msgReceived = true;
                        var _data = JSON.parse(JSON.stringify(userInfo));
                        _this.responseData = __WEBPACK_IMPORTED_MODULE_7__models_ChatbotResponseModel__["a" /* ChatbotResponseModel */].fromJSON(_data.queryResult);
                        console.log("FulfillmentText - " + _this.responseData.fulfillmentText);
                        var element = null;
                        element = {
                            "request": _this.chatMessages[_this.chatMessages.length - 1].request,
                            "response": (_this.responseData.fulfillmentText).replace(new RegExp('\n', 'g'), "<br />"),
                            "old_message": true,
                            "flagStatus": "1"
                        };
                        _this.chatMessages[_this.chatMessages.length - 1] = element;
                        if (!_this.mute)
                            _this.playText(_this.responseData.fulfillmentText);
                        _this.scrollBottom();
                        _this.chatMsg = "";
                    }, function (err) {
                        _this.msgReceived = true;
                        if (_this.gifloading) {
                            _this.gifloading = false;
                            _this.sendBtnVisibility = false;
                        }
                        console.log(err);
                        _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.SERVER_DATA_ERROR, _this.constants.COMMON_APP_MESSAGE.ERROR);
                        // this.utilPro.presentToast(JSON.stringify(err.name));
                        _this.loader.dismiss();
                    });
                }
                else {
                    _this.msgReceived = true;
                    _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.NO_NETWORK_TITLE, _this.constants.COMMON_APP_MESSAGE.NO_NETWORK_MESSAGE);
                    _this.loader.dismiss();
                }
            });
        }
    };
    ChatbotPage.prototype.playText = function (test) {
        var _this = this;
        if (this.speaking) {
            this.tts.speak({ text: '' }); // <<< speak an empty string to interrupt.
            this.speaking = false;
            return;
        }
        this.speaking = true;
        this.tts.speak({
            text: test,
            rate: this.rate / 6,
            locale: this.locale
        })
            .then(function (val) { console.log('Success'); _this.speaking = false; })
            .catch(function (reason) { console.log(reason), _this.speaking = false; });
    };
    ChatbotPage.prototype.ionViewWillLeave = function () {
        this.sub.unsubscribe();
        if (this.speaking) {
            this.tts.speak({ text: '' }); // <<< speak an empty string to interrupt.
            this.speaking = false;
            return;
        }
    };
    ChatbotPage.prototype.moreDetails = function (payValue) {
        var data = {
            enableBackdropDismiss: false
        };
        if (this.isForm == "true") {
            var claimPage = this.modalCtrl.create('ClaimPage', data);
            claimPage.present();
            this.claimStatus = localStorage.getItem("claimStatus");
        }
        else {
            this.chatMsg = payValue;
            this.sendMessages();
        }
    };
    ChatbotPage.prototype.likedislike = function (likeResponse, value, position) {
        if (value == "like") {
            /*this.likeData = {
              "userId": this.userId,
              "botResponse": likeResponse,
              "channelId": 2,
              "like": 1
            }*/
        }
        else {
            /*this.likeData = {
              "userId": this.userId,
              "botResponse": likeResponse,
              "channelId": 2,
              "dislike": 1
            }*/
        }
        /*this.dataService.isNetworkConnectionAvailable()
          .then((isOnline: boolean) => {
            if (isOnline) {
              this.dataService.likeService("likedislike", this.likeData).then((userInfo) => {
                this.responseData = userInfo;
                console.log("Response Like >>" + JSON.stringify(this.responseData));
                console.log("Response Like Status >>" + this.responseData.status);
              }, (err) => {
                console.log(err);
                this.loader.dismiss();
              });
            }
            else {
              this.utilPro.presentToast('No Internet');
              this.loader.dismiss();
            }
          });*/
    };
    ChatbotPage.prototype.getPermission = function () {
        var _this = this;
        this.speechRecognition.hasPermission()
            .then(function (hasPermission) {
            if (!hasPermission) {
                _this.speechRecognition.requestPermission()
                    .then(function () { return _this.startListening(); }, function () { return _this.utils.showAlert("Permission Denied by user", _this.constants.COMMON_APP_MESSAGE.ERROR); });
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Navbar */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Navbar */])
    ], ChatbotPage.prototype, "navBar", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], ChatbotPage.prototype, "content", void 0);
    ChatbotPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-chatbot',template:/*ion-inline-start:"D:\Projects\TFG_Next_Step\workspace\TFGNextStep\src\pages\chatbot\chatbot.html"*/`<!-- <ion-navbar>\n\n		<ion-buttons left class="backtstart">\n\n			<button ion-button></button>\n\n		</ion-buttons>\n\n		<ion-title>Ask to Kiara</ion-title>\n\n		<ion-buttons end class="userend">\n\n			<button ion-button (click)="presentConfirm()">\n\n			</button>\n\n		</ion-buttons>\n\n	</ion-navbar> -->\n\n<div class="bg-img">\n\n	<div class="back-btn" (click)="goBack()">\n\n		<img src="assets/imgs/back-btn-chtbot.jpg" alt="Chatbot-Icon">\n\n	</div>\n\n	<div class="audio-icon" (click)="toggleMute()">\n\n		<img *ngIf="mute" src="assets/imgs/audio_off.svg" width="30" class="mute_audio">\n\n		<img *ngIf="!mute" src="assets/imgs/audio_on.svg" width="30" class="mute_audio">\n\n		<!--img *ngIf="mute" src="assets/imgs/mute.svg" class="mute_audio">\n\n		<img *ngIf="!mute" src="assets/imgs/unmute.svg" class="mute_audio"-->\n\n	</div>\n\n	<div class="rectangle-1">\n\n		<div class="tfg-logo">\n\n		</div>\n\n		<div class="toggle-btn">\n\n		</div>\n\n	</div>\n\n	<div class="rectangle-2">\n\n	</div>\n\n	<ion-card>\n\n		<!-- <ion-scroll scrollY="true" item-height="1" >  -->\n\n		<ion-content scroll=true class="content-stable " on-swipe-left="hideTime = false" on-swipe-right="hideTime = true" no-bounce>\n\n\n\n			<ion-row>\n\n				<ion-col col-12 class="kiaraAssets message-box-holder">\n\n					<span class="input message-box message-partner">\n\n						<span class="welcomeText">{{userWelcomeMsg}}</span>\n\n						<!-- <span class="dateSpan">{{myDate|date:\'dd MMM yyyy hh:mm a\'}}</span> -->\n\n\n\n					</span>\n\n					<img src="assets/imgs/txt-msg-chatbot.svg" width="30" class="receiver-avtar-img">\n\n				</ion-col>\n\n			</ion-row>\n\n\n\n			<ion-row *ngIf="notifyFlag">\n\n				<ion-col col-12 class="kiaraAssets message-box-holder">\n\n					<span class="input message-box  message-partner">\n\n						<span class="welcomeText">{{pushNotify}}</span>\n\n						<!-- <span class="dateSpan">{{myDate|date:\'dd MMM yyyy hh:mm a\'}}</span> -->\n\n						<!-- <span class="imgSpan">\n\n						 <img src={{myLikeImage}}   (click)="likedislike(chatt.response,\'like\')" width="20" class="likeImg"> \n\n						 <img src={{myDisLikeImage}}  (click)="likedislike(chatt.response,\'dislike\')" width="20" class="likeImg"> \n\n					</span> -->\n\n					</span>\n\n					<img src="assets/imgs/txt-msg-chatbot.svg" width="30" class="receiver-avtar-img">\n\n				</ion-col>\n\n			</ion-row>\n\n\n\n\n\n			<ion-list *ngFor="let chatt of chatMessages; let i=index">\n\n				<ion-row>\n\n					<ion-col col-12 class="kiaraAssets message-box-holder">\n\n\n\n						<span class="messageRespond message-box message-sender-margin">\n\n							<span class="welcomeText">{{chatt.request}}</span>\n\n							<!-- <span class="dateSpan">{{myDate|date:\'dd MMM yyyy hh:mm a\'}}</span> -->\n\n						</span>\n\n						<img src="assets/imgs/userReq-icon.svg" width="30" class="Sender-avtar-img">\n\n\n\n					</ion-col>\n\n				</ion-row>\n\n\n\n				<ion-row *ngIf="!chatt.old_message" [ngClass]="{\'hide\': msgReceived, \'unhide\': !msgReceived}">\n\n					<ion-col style="margin-left: 30px;">\n\n						<img src="assets/imgs/chat_loading.gif" width="50">\n\n					</ion-col>\n\n				</ion-row>\n\n\n\n				<ion-row *ngIf="chatt.response">\n\n					<ion-col *ngIf="chatt.flagStatus ==1" col-12 class="kiaraAssets message-box-holder">\n\n						<span class="input message-box  message-partner">\n\n							<span class="welcomeText" [innerHTML]="chatt.response"></span>\n\n							<!-- <span class="dateSpan">{{myDate|date:\'dd MMM yyyy hh:mm a\'}}</span> -->\n\n							<!-- <span class="imgSpan">\n\n						 <img src={{myLikeImage}}   (click)="likedislike(chatt.response,\'like\')" width="20" class="likeImg"> \n\n						 <img src={{myDisLikeImage}}  (click)="likedislike(chatt.response,\'dislike\')" width="20" class="likeImg"> \n\n					</span> -->\n\n						</span>\n\n						<img src="assets/imgs/txt-msg-chatbot.svg" width="30" class="receiver-avtar-img">\n\n					</ion-col>\n\n				</ion-row>\n\n\n\n\n\n\n\n				<ion-row *ngIf="chatt.flagStatus == 2" class="btnRow">\n\n					<ion-col col-12 class="kiaraAssets message-box-holder">\n\n						<ion-card class="myCard">\n\n							<ion-card-header>\n\n								{{chatt.response}}\n\n							</ion-card-header>\n\n							<ion-card-content>\n\n								<span class="input message-box message-partner-card">\n\n									<div class="btn" *ngFor="let button of chatt.buttons; let j=index">\n\n										<button *ngIf="!chatt.isForm" ion-button round (click)="buttonRequest(button.payLoad,i,j)" [disabled]="!button.active">{{button.title}}</button>\n\n										<button *ngIf="chatt.isForm" ion-button round (click)="buttonFormRequest(button.payLoad,i,j)" [disabled]="!button.active">{{button.title}}</button>\n\n									</div>\n\n									<!-- <span class="btndateSpan">{{myDate|date:\'dd MMM yyyy hh:mm a\'}}</span> -->\n\n									<!-- <span class="btnimgSpan">\n\n							 <img src={{myLikeImage}} (click)="likedislike(chatt.response,\'like\',i)" width="20" height="20" class="likeImg">\n\n							<img src={{myDisLikeImage}} (click)="likedislike(chatt.response,\'dislike\',i)" width="20"> \n\n						</span> -->\n\n								</span>\n\n							</ion-card-content>\n\n						</ion-card>\n\n						<img src="assets/imgs/txt-msg-chatbot.svg" width="30" class="receiver-avtar-img-btn">\n\n					</ion-col>\n\n				</ion-row>\n\n\n\n				<ion-row *ngIf="chatt.flagStatus == 3">\n\n					<ion-col col-12 class="kiaraAssets message-box-holder">\n\n						<span class="input message-box  message-partner">\n\n							<span class="welcomeText">{{chatt.response}}</span>\n\n							<!-- <span class="dateSpan">{{myDate|date:\'dd MMM yyyy hh:mm a\'}}</span> -->\n\n							<!-- <span class="imgSpan">\n\n						 <img src={{myLikeImage}}   (click)="likedislike(chatt.response,\'like\')" width="20" class="likeImg"> \n\n						 <img src={{myDisLikeImage}}  (click)="likedislike(chatt.response,\'dislike\')" width="20" class="likeImg"> \n\n					</span> -->\n\n						</span>\n\n						<ion-slides slidesPerView="2" spaceBetween="280" loop=true pager="2" class="input message-box ionSlideMsg">\n\n							<ion-slide *ngFor="let data of chatt.imgsrc;  let i=index">\n\n								<ion-item class="card">\n\n									<img src=\'{{data.image}}\'>\n\n									<!-- <span class="cardateSpan">{{myDate|date:\'dd MMM yyyy  HH:mm\'}}</span> -->\n\n									<div class="borderTitle">\n\n										<p>{{data.title}}</p>\n\n										<p *ngFor="let details of data.buttonList" class="carMoreDetails">\n\n											<button (click)="moreDetails(details.payLoad)"> > {{details.title}}</button>\n\n										</p>\n\n									</div>\n\n								</ion-item>\n\n							</ion-slide>\n\n						</ion-slides>\n\n						<img src="assets/imgs/txt-msg-chatbot.svg" width="30" class="receiver-avtar-img-btn-car">\n\n					</ion-col>\n\n				</ion-row>\n\n\n\n				<!-- Bharti 27 July and 1 august-->\n\n				<ion-row class="btnRow" *ngIf="chatt.flagStatus == 4">\n\n					<ion-col col-12 class="kiaraAssets message-box-holder">\n\n						<ion-card class="myCard">\n\n							<ion-card-header>\n\n								{{chatt.response}}\n\n							</ion-card-header>\n\n							<ion-card-content>\n\n								<span class="input message-box message-partner-card">\n\n									<div class="btn" *ngFor="let button of chatt.buttons; let j=index">\n\n										<button ion-button round (click)="buttonRequest(button.payLoad,i,j)" [disabled]=!button.active>{{button.title}}</button>\n\n									</div>\n\n									<!-- <span class="btndateSpan">{{myDate|date:\'dd MMM yyyy hh:mm a\'}}</span> -->\n\n									<span class="btnimgSpan">\n\n										<img src={{myLikeImage}} (click)="likedislike(chatt.response,\'like\',i)" width="20" height="20" class="likeImg">\n\n										<img src={{myDisLikeImage}} (click)="likedislike(chatt.response,\'dislike\',i)" width="20">\n\n									</span>\n\n								</span>\n\n							</ion-card-content>\n\n						</ion-card>\n\n						<img src="assets/imgs/txt-msg-chatbot.svg" width="30" class="receiver-avtar-img-btn-car">\n\n					</ion-col>\n\n				</ion-row>\n\n\n\n				<ion-row class="buttonCrousel_row" *ngIf="chatt.flagStatus == 4">\n\n					<ion-slides slidesPerView="2" spaceBetween="2" loop=true pager="1">\n\n						<ion-slide *ngFor="let carousel of chatt.btnCarsl; let j=index">\n\n							<div class="btn">\n\n								<button [disabled]=!carousel.active (click)="buttonRequest(carousel.payLoad,i,j)" ion-button round>{{carousel.title}}</button>\n\n							</div>\n\n						</ion-slide>\n\n					</ion-slides>\n\n				</ion-row>\n\n				<!-- End -->\n\n			</ion-list>\n\n\n\n\n\n\n\n		</ion-content>\n\n		<!-- </ion-scroll> -->\n\n\n\n			\n\n			<!-- <button icon-only ion-button clear type="button" item-right (click)="startRecording()">\n\n						<img src="assets/imgs/audio-btn-chatbot.svg" width="17">\n\n			</button>\n\n			<ion-input placeholder="Please enter here.." [(ngModel)]="chatMsg" #input></ion-input>\n\n			\n\n			<button icon-only ion-button clear type="button" [disabled]="!chatMsg" item-right (click)="sendMessages()" class="sendIcon">\n\n				<img src="assets/imgs/send_icon.svg" width="25">\n\n			</button> -->\n\n	\n\n	\n\n			<div [ngClass]="{\'search\': !keyBoardOpen,\'search-open\': keyBoardOpen}" sticky>\n\n					<div  [ngClass]="{\'audio-btn\':animationstart, \'hide \': animationstart }">\n\n						<img  *ngIf="!gifloading" src="assets/imgs/audio-inactive-chatbot.svg" (click)="startAnimationAudio()">\n\n						<img  *ngIf="gifloading" src="assets/imgs/listeningMic.gif">\n\n					</div>\n\n					<div class="audio-active" [ngClass]="{ \'unhide \': animationstart ,\'hide \': !animationstart }">\n\n						<img src="assets/imgs/audio-active-chatbot.svg" (click)="startRecording()">\n\n					</div>\n\n					<div class="space-between"></div>\n\n					<div  [ngClass]="{\'hide\': !gifloading, \'unhide\': gifloading }">\n\n						<ion-label style="margin-top: 20px; color: #4A134A;">Please Say Something</ion-label>\n\n					</div>\n\n					<div [ngClass]="{\'chat-area_nowidth\': !sendBtnVisibility, \'chat-area\': sendBtnVisibility && !gifloading}">\n\n						<ion-input [ngClass]="{ \'unhide \': animationstart ,\'hide \': gifloading || !animationstart }"  placeholder="Please enter here.." (keyup.enter)="sendMessages()" [(ngModel)]="chatMsg" #input (click)="openhideKeyboard()"></ion-input>\n\n						<img [ngClass]="{\'hide\': !gifloading, \'unhide\': gifloading }" src="assets/imgs/chat_loading.gif">\n\n					</div>\n\n					<div [ngClass]="{\'send-btn\':animationstart, \'hide\':sendBtnVisibility }">\n\n						<img src="assets/imgs/text-inactive-chatbot.svg" (click)="startAnimationText()">\n\n					</div>\n\n					<button icon-only ion-button clear type="button" [disabled]="!chatMsg" item-right (click)="sendMessages()" class="sendIcon" [ngClass]="{\'hide\': !sendBtnVisibility || gifloading, \'unhide\': sendBtnVisibility}">\n\n						<img src="assets/imgs/send_icon.png">\n\n					</button>\n\n					<!--button icon-only ion-button clear type="button" item-right (click)="closeListening()" [ngClass]="{\'hide\': !gifloading, \'unhide\': gifloading }">\n\n						<img src="assets/imgs/close_btn.svg" style="width:24px !important;">\n\n					</button-->\n\n			</div>\n\n	</ion-card>\n\n\n\n\n\n	\n\n</div>`/*ion-inline-end:"D:\Projects\TFG_Next_Step\workspace\TFGNextStep\src\pages\chatbot\chatbot.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Keyboard */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_text_to_speech__["a" /* TextToSpeech */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ToastController */], __WEBPACK_IMPORTED_MODULE_5__utilities_constants__["a" /* Constants */], __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["j" /* ChangeDetectorRef */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_speech_recognition__["a" /* SpeechRecognition */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Platform */], __WEBPACK_IMPORTED_MODULE_3__utilities_common__["a" /* Utills */], __WEBPACK_IMPORTED_MODULE_4__providers_network_networkcalls__["a" /* NetworkProvider */]])
    ], ChatbotPage);
    return ChatbotPage;
}());

//# sourceMappingURL=chatbot.js.map

/***/ }),

/***/ 163:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PayslipPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__utilities_constants__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__utilities_common__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_PayslipListModel__ = __webpack_require__(321);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_in_app_browser__ = __webpack_require__(322);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_opener__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_network_networkcalls__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_storage__ = __webpack_require__(47);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var PayslipPage = (function () {
    function PayslipPage(file, fileOpener, utils, networkService, storage, constants, navCtrl, navParams, alert, theInAppBrowser) {
        this.file = file;
        this.fileOpener = fileOpener;
        this.utils = utils;
        this.networkService = networkService;
        this.storage = storage;
        this.constants = constants;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alert = alert;
        this.theInAppBrowser = theInAppBrowser;
        this.payslipDataList = [];
        this.authToken = this.navParams.get('AuthToken');
        this.options = {
            location: 'yes',
            hidden: 'no',
            clearcache: 'yes',
            clearsessioncache: 'yes',
            zoom: 'yes',
            hardwareback: 'yes',
            mediaPlaybackRequiresUserAction: 'no',
            shouldPauseOnSuspend: 'no',
            closebuttoncaption: 'Close',
            disallowoverscroll: 'no',
            toolbar: 'yes',
            enableViewportScale: 'no',
            allowInlineMediaPlayback: 'no',
            presentationstyle: 'pagesheet',
            fullscreen: 'yes',
        };
        this.months = [];
        this.years = [];
        this.getPayslipLocalData();
    }
    PayslipPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    PayslipPage.prototype.getPayslipLocalData = function () {
        var payslipObjs = __WEBPACK_IMPORTED_MODULE_4__models_PayslipListModel__["a" /* PayslipListModels */].getPayslipDataList();
        for (var j = 0; j < payslipObjs.length; j++) {
            var _item = this.utils.formatPayslipTimestampForPayslipFilter(payslipObjs[j].EffectiveDate);
            var mitem = _item.split(",")[0];
            this.months.push(mitem);
            var yitem = _item.split(",")[1];
            if (this.years.indexOf(yitem) === -1) {
                this.years.push(yitem);
            }
        }
    };
    PayslipPage.prototype.monthPopup = function () {
        var _this = this;
        if (this.months.length <= 0) {
            this.utils.showAlert("No data available", this.constants.COMMON_APP_MESSAGE.ALERT);
        }
        else {
            var alert_1 = this.alert.create();
            alert_1.setTitle('Months');
            for (var i = 0; i < this.months.length; i++) {
                alert_1.addInput({
                    type: 'radio',
                    label: this.months[i],
                    value: this.months[i] + "," + i,
                    checked: false
                });
            }
            alert_1.addButton('Cancel');
            alert_1.addButton({
                text: 'OK',
                handler: function (data) {
                    _this.payslipDataList.slice(0, _this.payslipDataList.length);
                    console.log("Clicked data: " + data);
                    _this.monthSelected = data.split(",")[0];
                    _this.selectedIndex = data.split(",")[1];
                    var __payslipObjs = __WEBPACK_IMPORTED_MODULE_4__models_PayslipListModel__["a" /* PayslipListModels */].getPayslipDataList();
                    for (var j = 0; j < __payslipObjs.length; j++) {
                        if (__payslipObjs[j].Month === _this.monthSelected)
                            _this.payslipDataList.push(__payslipObjs[j]);
                    }
                }
            });
            alert_1.present();
        }
    };
    PayslipPage.prototype.yearPopup = function () {
        var _this = this;
        if (this.years.length <= 0) {
            this.utils.showAlert("No data available", this.constants.COMMON_APP_MESSAGE.ALERT);
        }
        else if (this.monthSelected === '' || this.monthSelected === null || this.monthSelected === undefined) {
            this.utils.showAlert("Select month first", this.constants.COMMON_APP_MESSAGE.ALERT);
        }
        else {
            var alert_2 = this.alert.create();
            alert_2.setTitle('Year');
            for (var i = 0; i < this.years.length; i++) {
                alert_2.addInput({
                    type: 'radio',
                    label: this.years[i],
                    value: this.years[i],
                    checked: false
                });
            }
            alert_2.addButton('Cancel');
            alert_2.addButton({
                text: 'OK',
                handler: function (data) {
                    console.log("Clicked data: " + data);
                    _this.yearSelected = data;
                    for (var j = 0; j < _this.payslipDataList.length; j++) {
                        if (_this.payslipDataList[j].Year === _this.yearSelected)
                            _this.actionContextId = _this.payslipDataList[j].ActionContextId;
                    }
                }
            });
            alert_2.present();
        }
    };
    PayslipPage.prototype.exportPayslip = function () {
        if (this.monthSelected === '' || this.monthSelected === null || this.monthSelected === undefined)
            this.utils.showAlert("Please select month", this.constants.COMMON_APP_MESSAGE.ALERT);
        else if (this.yearSelected === '' || this.yearSelected === null || this.yearSelected === undefined)
            this.utils.showAlert("Please select year", this.constants.COMMON_APP_MESSAGE.ALERT);
        else
            this.getPayslipBlobDataFromServer();
        //this.downloadPayslip();
        //this.utils.showAlert("Work in progress", this.constants.COMMON_APP_MESSAGE.ALERT);
    };
    PayslipPage.prototype.downloadBlobToPDF = function () {
        var _this = this;
        var name = this.monthSelected + "_" + this.yearSelected + ".pdf";
        console.log("Downloaded Blob - " + this.downloadedBlob);
        //let downloadPDF: any = "JVBERi0xLjQNCjUgMCBvYmoNCjw8DQovVHlwZSAvWE9iamVjdA0KL1N1YnR5cGUgL0ltYWdlDQovRmlsdGVyIC9GbGF0ZURlY29kZQ0KL0xlbmd0aCAyOTA3DQovV2lkdGggNDMyDQovSGVpZ2h0IDcxDQovQml0c1BlckNvbXBvbmVudCA4DQovQ29sb3JTcGFjZSAvRGV2aWNlUkdCDQovTWFzayBbMjU1IDI1NSAyNTUgMjU1IDI1NSAyNTVdDQo+Pg0Kc3RyZWFtDQp4nO1dS3IrKwz1nnojWUVWkUG24T15/LZwqzJKZqnKoB807U67P3AQAiRHp1SuXF/bzUccJCFgHA0Gg+FJ8fPZuwQGg8FgMBgMBoPBYDAYDAaDwWAwGAwGg8FgMHDg59PJNze0l19E1ci43cbrdZbbTUeZJ8wtPHXo/Mr6fviv42evP3xPC1HUdLnYN1GoLO/7pw3oVNRJj/Sbj48PPy7e3sfX13EYvFyG+Q+aXFa/EP8p90T33Ovt6+uLrS6b5xLqgpd//8X1Hy8vvoLXK1ftGOBa2xfv8iDuHacAXXCi8L7FrrcHnWws7ulrxIvRq/X2yCcQz0hhfuzX2mtW9EPYFWOtnLdbpPxsuN2OR0cvcexRTh2uUt0rciiui9t0awTxvn556W/thIHZXSddATbEEi+SG0cK4ceaBAZYtd5sz4Q318pQc8bxD3WP69sI0fYhs6KvWvfyR8Rxfi9WdBqFFK8XXLO4p3fvoCD7PnoyPnQVXNtgHeXROJx1YKHBYByGolYYOP7REuZfoJWIM4JY+3Atzd2r32k3Ke3pWhQTXk7ILV5CRXwohwn3+hYGb2jMwITBoXa2eo1GDr/fvQVwybempNuHizSmRHwIuE+2hLT5a2OuLHgOPkR8hJayUbZQvLDA55gq+PLBZwmBTSb435fsIMclizqkja9YvVpZYrlt0sxEFKiTZ3VXzoc+ACXKCL8cTT13PvQGYViUvFzm0BkjH6pwkFNNBw5S33rdS4tLg3FEmSDqr+jJdFUiM69qPvSGVu+2BXRsHryO+hwfBt/wcl+IvDB5LtIs5BIBBqkaf3mRmo7z7HcQSnXmNrJApk7G15L08qGoaGFS7YPLMOXg+X+G2HLQ4fI5+qrHeQQl2SaK/OUgw1AxO7HEIa1E1FI7yM+kEWjhw02akEwyPJ96fv169xryIbm0kaZ4wxBSiH1hqkoICBCKF6fEZK1r1yuI71O4dpVGUznzsHvNZJ0Mo6OeJGuqhQ/XoM2Gw32XRDVJTD3jY4iPI3E3I7/icmfs5tso5sT4t8yM0GjacNpfbrUDaNphgUZu0xqSCd9E5fFz1nRE3yBZHR1yroRsJ1THh7lTz8QA7HpYAs7dr/hY6JghvEbWok9E/VJq0Jjz0XU9ducUidEhDc5YMNx3m7bLiaDBBer4EDcOJeycqgp8apCz7zI3KeisB5N1b971kKnGaolBi0pLvmvqYzw2Ax7KfnuXxYQBuvhQJwPUAsgqMicFsCvPFt9TX+/iESDrvJyGK2SRTr2P0FRxngO6zD1I1clRGx+C7uEfIENU9yQ3BebrHVoRadNI7PZhrvRspPXWFIeMncJGA513sWQ4quJD0KKQ3NqMQHRPMhkGILGmow4Vy4fpRHGOToHW0YZHIxkZPgXpiOgELXx4KuJD3Dt4esz7XKJN0fEMExjQIDqsiDR/eb2c3WBMIfPInniRAAu5eMgELZwMRz18iMxuckpbG8nW2B/sJhZIz+7Hkbz1lF/Eaad8OxLSYmezYT0TLmmuND5BggYtfJic2iScctkMSfNAvqe8RrJzd9UR6y97VOVDxDWIVB/zmnMLBW0nl28cjjr4EPKqVLQ2F+KtwXdgThukR9Pe2pFsH8Znq8IxVe7zIr52biEBZ1nQZQoRaODDtPKrMMW5IJkKyEgN863xLy1+CBesyHQHF3CjQLe0ZJUz+YNaHBYVfBgfLFV3ygtEqjU0xg2AZdlHkhc7KSStL2rB0CPOEOZhT9VomXVZFSr4MDH7DHX3gOfK62td4yQ+4vSayvEx9TjMhcYPgXUuOjMUpGseADERMV2ihDvEQjwf0s926yW1LTTxXUZETthNIh8i2+KosxV/dh+yYwULRP/791+lWneA+MGl7CTkBiMx3mVaAjV7ZC3LCjvPAT2ngqwbNRZBEBMR+c2qIdPGEM+Hyk5CbtBi8XGncTElgJUPN+3wfYfPV/n5XP7+JmH54nzYF7iNlOw4VNpawhVFND5sCE182GYtw/gQ0Ip1CPfX2QyX1yyvZFm+nqUeJFqouw+OJSZpfNgQmviwDRcl/OU/wYfZ68vdLzYirykgCYfkAB2Y3R0nNOPDhlATP2yWBW3xw5GSf5h9ljiv0OZKyKUtyzfDvOZYygRyd4MWiOdDNfZhMyKquv2hI7JUkZZ/2OuSI7JutDlzG1msid8NFP+urS/zwccu8Ct7eknLLXJxO0pRrtcK6SgZV/5he8eZzFdIcI9j2/5EaHSveRqhYkZHIcTzoYf8/MOWqxipYaJmL8AamfZeUf5hS0qkkiHq3XMpHrSwcq5aT3O8gAo+TNjz0/1cHaVxXz9T+HpBatVgOxLL9i/zXEuXlBI/sUbCYRwlO1aSpRXCJEmo4MPcwfLUmO5zjOqeIvdkQjoksq9R8f5l/9B6t3gXnoqPpcE4TVjualy85vU7B5mT5+8XpSMmC6xFJ1XwoZ1vs4Gdf8i1Xy/r/lOEBl3XFDY+6M7fbrNPzShgHXdtCy16qnCZNfAhlJKqorW5QDtTWiYA0+LA/uc73wa9OnkRx3j3gzvmP6aYCY+TgiYE3muHWJLscpihnZyjVZy8pIEPPYBRI+rS+epIDmEVVwYgxH5o/POef+gaKusW9XpzDSH7up7Xn8UMz3GjhxI+hAxynakmRLRKxqiHKRBKXUKtcf4heJ81+feTwLKvN1TfLUF33wJtsiWrQgkfeiDz4J8JJKLbWiXPEcjwOYnD1zrvC3c/uS9VRzv0kE+6eM372RZckZFMiYr4EGxtUWWuCnAUyGwQ0Bg7KXzF8w/xPSy8jjPSIJHZrUH6EMJsYDFcZWUGcxTx4QgPor9hJWbs7Ha9LGd5JSMd+jwmXPX+lCzHmcXaKT96q9NWxG0795pNcnHGxnHNFMaHqE8xMcCfWF7JGrnuw31Z0T09K/gfOaun9v0pWR5o+TAp20FMKTOX7EuVVQxXcTkz9ajwsJSsCE93BqgMyj4L9/mwp6a+wzLn+rpn4YemrkfKOVrcF4CcqL9qVfLSVVHkcIMuO1vJXvPvUB3CUA0Z5vtzesM74y6ZvPD9cZXHDpX87T2e1p5Meie/f1CXLOXZjKx7hliD8j+8NuEc+ijgzeYlp/gS6KXN/XpZO53JDiAWOUw0yFJg3svREGbbmYho5kAvndzIuteQNDYhskxDJaGSxmVus7zb/bBTXhmAvV3N7l+uTYnsV3/yAswP3xdPi05uJpouy1IE2WxS6BIqIUizgINXv96VZepoiMoa3r/8nRuUyOp0hDT6hvGxTNeDL6qgxI2eaOHDfWhdPiU23qCkQv3i8vIC7nprfd9o1gaWC8xgvFcq1wM50Vq4Tu67SQUfnoXWvZEguLWb+zjZW3FFyesrPvD73L+ctZqfrI5wTzm/qIeG/XfVc4RK5DAsI5m97xKzGcROQB19HMJibl/x6ZGZgYVm8cN92+L6dp72ic5cchLeCveH8p4jVC5n24vkD5x4NObn06uWUxtRre1atXv6vetu+Z1LTj9rGD/cwOtb+eEPJYev9kLZhllPlXJYMXKdRPeyRQQOTU+tLcNWHCQdLCOWFcu2zCT35lTPxs9LLH9UYyyzUdopx+mDiIPEu9UNje7+S3yikendX6g5rt0ZQELAZ4OQliaBGJds8PIade2F7EXnezhxOsQV+LzArRBjxsHd6Z9asvQb2zCp9Sm5Ac8SlQ4M0N5clObjPMIratDD5UTT2sJ+YuoEzyrLiaz717f3BsaVHzj7py+13ryzRAbCpsVkIrRMOOsuWfLMGMj3soMpqCXSOGTByxYIpGphsoRpftwyQGWR5uMYDAaDwWAwGAwGg8FgMBgMBoPBYDAYDAaDwWAwGAwGg8FgMBgMBoPBYDAY/ib+B2eP9zBlbmRzdHJlYW0NCmVuZG9iag0KNiAwIG9iag0KPDwNCi9UeXBlIC9QYWdlDQovUGFyZW50IDMgMCBSDQovUmVzb3VyY2VzIDQgMCBSDQovQ29udGVudHMgNyAwIFINCi9NZWRpYUJveFsgMCAwIDYxMi4wIDc5Mi4wIF0NCi9Dcm9wQm94WyAwIDAgNjEyLjAgNzkyLjAgXQ0KL1JvdGF0ZSAwDQo+Pg0KZW5kb2JqDQo3IDAgb2JqDQo8PCAvTGVuZ3RoIDI5OTggL0ZpbHRlciAvRmxhdGVEZWNvZGUgPj4NCnN0cmVhbQ0KeJytW99z2zYSfveM/wc83bhzDkv8JvJmx3aaTJPmbPfmOtMXRqJtXiTSJaWmvr/+PpCgREqAIjJNJkqs5WKx2P12PwDMH0SJKCExftM4UpIIQbQQkZaKzJbkx3fLmFyV5F+nJxEjkSCRIrdvyebf1ePpyeX96QltR8AYCUbrBrhfnp78eENPTwhl5P7h9OTsU/pSlYvFD/f/PT2JGx2M1v7DDtUNwxIRGTsRFTHjhmF2mHgzTL3In5thOh2hecRgVAsTCUF7xnWn9JgNNGQcR2Lw/BltHsDPGt5xSiKdiMbh4Td2rtdwG35iyTSVkaISs8N4khj7UWXkwevicLXgX8IwQBLFtO+om/L18nlRvmTDaVMZR4lslJjUHj+fSmgVX7LqnHyoIvKhfEqXy2xOPqZfs81f4xzllMFYz1MWxWM85RKKNrW+4WmF2S2zSUHAWlI1IQiJimKloY/MdTM7ezfPilW+eiEf18vPWeVd/thgCTzLnxjKKJWMAwti2ipvXBm7ykpGWqmBL4NV/qV6TIv8f+kqL4sheFhsTQWd4rGOY8rJK/LT7bsP5O6lXmXLekqclFGRpmp8nLSOEgN1E3F1ACoHQmZ1VezxjlJqOJWTgrX1Z2ywtIyEVmGPUOTIZVrntS9SQV/ub94C8cXqafFC7tJFWr1MilKiI6YmRImii1CUNJWYyPhS8Odytp9+mwBB2Zd+1qn7p4zclrMv5HKdL+Z58TgtXBvHxoYrSSJ+wK+3VTrP/JEKONVoEDMpPBYMU0BkwyNR7VQCRV/SvS8/k/t8tfC2HKulfUl3UaQLlITXqA7TakMXnY1fo6MTR4bLsF+fyjoPVb2gW5QqkURbn4jzc1LIFPJnSn+ylEoh8TT1d86fcgxzla78EYOWlyQw+up9WrxiKOrTIrVxZ2ykLN1iB/z5pcofc6wz8TvWxexvd8yFSSbA94Qwaay3AbAUBcA8bl3Udf5YLMErDnUoaCd/e4fauDQaVDJisQz71Gfzu/EJeWJL+U1Zz57yIidvq3L93LWrSeGy7YaKLlxAaxKzIwOmIs6xOAwDHOITF/N5ldW1N1xQ5r4kvFtVWdYF+jURXmV4wOBCQzidQtnyX1I+kJu0Wr4m/y7Lqn4uwdpvy3ROvONwsEXN23HeYpjiKf0z8/dXBvaduK3O3frzuvpsTV7l9arKZyjfF4hDWWTn5L78WljRG5Dg1+RN+pz5bVN0w8SRZqvjf4g20GgeQh1eAdtvynn2GpROTWPI27A3KT0m7Bx7SpCnbwa+8ga+y+5Q4Lki77PiS17UZUHaoHr1+7H/lFblV3Kd1v5H++HV2LK2C2aQu5HBUkQJ2JZdsOE33YJJyzKJAq9PFOSSoBjImLq1clJXyyHF9izB4gykjjFBivRB+R5Iux1hZAuLdiJ4F7bZCgMmW2HAYiMMG9RMNORBcpQrK9+bjNiVRbpZL6SB3VvYVRx8YRdR2OnLqB2XYK78uDyT9mxBYnNpBA9WTvKueCir5ZYQRwZumwR/jItq7+dN6cOU2upFtR49JZ0g710+fcqqvJyT62K+bbRHTYGhg0+aA2N6dxL36V+kncjx9jkyZpJ9EYu9RUhfmrZ8U2V/rLNi9rJzSGSfVyaSzHOmxOmrD2ll2YYZaDk3e2ruaGlnHv0H3qSLrJinVdsP27XQSP7tR5Ogg2963VAjKxSsJkkDHOxKJetB9YCYMbojphqVpBNzkxwSO9MIRpx4sNfT9AsNhycBmT0GpBNBi8BJMw21trprT2uwmToZsYI2ZzNTICt4JIXZouVula7W9Ui0TrLv8nh3ArfZQ1YBKhn5WI4E7aRpOLT0p2FPR/Y2B+1ycWSNrzl/tIFb+HDa0ziLOY1NLKmRPsD2n7Tgv7jdgn8sWjkDjMQGjnQHrQFxh9atuIEj3UFrQOxMSwNS4WuVukUOWFJA6Mb1CxFcud+BjwQslU1NmgJYuCi53Afs3Xq5TKsXy6ddlR+H2Vg1hW5cslKUO4PyIbFx17HsyLGlvddpVeTF41joTpoGB3FMqPZN4yqbr2e2go2YSAPeSRORSF97MeOZyEfseRCY4a4BbEtYqhbHmx493OEn5wy5wtlAjSuwgcbKVu0MT9q9wbD1Sm72DJxRc24wgqRTkCyM3VeF2m5A6nC8lfqabkDq4IZVkDHzY9Fp+nEKpsx5QNEGWek94XEgFhqtUxCm7ZnnWBgLLSLKPGx5PGzsVBS6BSp8PLrfCiUjZlxiXGX1rMqfx/V7hig7+/gctRIs4RGndDiJi2W5Llb7rU5IZFTs4aVv0vqpf/y/GR3EV9Gh4hmj5zH26DuAag0APmiHDrNV+me2IHezp2w57LssYchENXxcnzP0zG7DahN/+9Hkz+CbHURhlx7rZgvIAXUhOlhQ9N+glEm6o8wspPkQGIgIU02Kqn3UONWAFNkN7wJCKSJsa0NSe0DN9oRHoIqz9l7eIUvC4TH5xLEE9ogkhK1JvcBNqc1wati0KX03zGxoNzAbg3LEhKpvY6ybaAhl4OEPD+RiNttHpwDBMjs4E0J2cNi10MfNp4vfrncG401rGzwlznmCviq8w6HDgRM5v+bLbH4xn//TP8H+o8wEJ8jj5myprQP2ijGb5zN08It8Tq4D0+3r0HPb0QKDMyQQ59vBb7NVXmXtpnxdhA309ei5bbeJ9Bqgdo/povDruxvifa9isyT9pynWXLNxJUxS1kAeNdVeKwHsKm4P5Qa0oBE3vV2ZSNNN52dN4T0sdTVMWo+31cQJuxrmlzJ73sZCUmc2IHX1zy/s6p9f6uqfX8jtAYEIzhj7kT3ZcXyEG4GdgZpISLgBp/SdEd9UoCMZucyK7CFf1SRFkgJizQ1j2p7qjeIq9qZFo39OICs8sWtnvpOsdBOYylYGswixFa6TzUXAkK2UxapKZyt7zYEtPXnOKvJTua52OEbStPr+KGcUABfGZwo50+01esPbUetm/O0h276Bni7FpmAaheEqiRh4dEdS+IDChKSOwvTEHgpjo4XOEqAanW5ACkSwJKirVXNOHiAxMdKD7usez2I2eJxIY4KI/O3+yjbi9XK9aM7mavIPcgn+W8yy8cSmQ8NUZvP9mLQR32JyArX5FiC7mYYgyZVlLelf6edFhno2K5cZub15591IDxDp30xszPWgJSiabL8bkwao+ef13osaXWseQnPbmnfNKICo29nvmqmOMNPXP2BGAg7d6b2I8U2fFR2w0tKXgfohfoSiFEn3HuwZT2g8ZF+3nSm/lb7yQSuoQ4brbtEA886KPfF9U+GHFfm1dm+O7i1ZX1vZq0u/EaZR7FwAue7lmNtcXiwW5VeLWr8zffXB/nLXDrXHJ7xbMti5qGt72LSuZk8pnCA3l34/+ooM7N1o//hxA7Mu9JR8yooake4I6+GY9LUPEVceb9+8sJHXO2a+Efq+Njvn9kbEu19gqHNGb0IfsybkKKneYYdP97chYxkyMyCFoj2kRmtJxM4ReF8ODaXpgBh+Q8wFAzcI8WDO0IFoiJHyGFstFiKzm/7rlXbTCuh23dkvRfuPk6BdziMTB0fuOrtfSrETDZllxmzaske1IwV+et6RgoBDJvJsCo5j70xjb8/FlCsBpm149T5RcEfO7qWXXnk+lqszBS6EIFDe5PDYeYFPiqR7GcbdPH/IVk9jbr+p1g08ps6EJg3NH8zlMi2+kMsKpfepeTtnBGmy75ROnwyn6BzDubgTlf6LcsdFR9j/AvIdEXLkaTiZwG6GSftCutinTld5lc1W5Cp7tu+c7txttAvfU0WTiZXcqf7tkvSf+k/7yzDBvRxs8PDuZcbR+xa3zcYS+G8celLfTUdIbPNVBoe2+cPCYmc5Yc0bX94rC1snWBISIhsEOrpf2BideNnBuH3FQE8qT0hNnuxXpw9ZXaeP2e9n9e8/eFKO8UgnZj/lPpakr0lukLLzKZvW9lJK7V0bdwEOiJ0ytxfHgTunVlUGhNzeR+td4f8BrDZa5g0KZW5kc3RyZWFtDQplbmRvYmoNCjEgMCBvYmoNCjw8DQovVHlwZSAvQ2F0YWxvZw0KL1BhZ2VzIDMgMCBSDQo+Pg0KZW5kb2JqDQoyIDAgb2JqDQo8PA0KL1R5cGUgL0luZm8NCi9Qcm9kdWNlciAoT3JhY2xlIEJJIFB1Ymxpc2hlciAxMC4xLjMuNC4yKQ0KPj4NCmVuZG9iag0KMyAwIG9iag0KPDwNCi9UeXBlIC9QYWdlcw0KL0tpZHMgWw0KNiAwIFINCl0NCi9Db3VudCAxDQo+Pg0KZW5kb2JqDQo0IDAgb2JqDQo8PA0KL1Byb2NTZXQgWyAvUERGIC9UZXh0IF0NCi9Gb250IDw8IA0KL0YxIDggMCBSDQovRjIgOSAwIFINCj4+DQovWE9iamVjdCA8PCANCi9JbTAgNSAwIFINCj4+DQo+Pg0KZW5kb2JqDQo4IDAgb2JqDQo8PA0KL1R5cGUgL0ZvbnQNCi9TdWJ0eXBlIC9UeXBlMQ0KL0Jhc2VGb250IC9IZWx2ZXRpY2ENCi9FbmNvZGluZyAvV2luQW5zaUVuY29kaW5nDQo+Pg0KZW5kb2JqDQo5IDAgb2JqDQo8PA0KL1R5cGUgL0ZvbnQNCi9TdWJ0eXBlIC9UeXBlMQ0KL0Jhc2VGb250IC9IZWx2ZXRpY2EtQm9sZA0KL0VuY29kaW5nIC9XaW5BbnNpRW5jb2RpbmcNCj4+DQplbmRvYmoNCjEwIDAgb2JqDQpbIDYgMCBSIC9YWVogMzYuMCAyMDMuMjYgbnVsbCBdDQplbmRvYmoNCjExIDAgb2JqDQpbIDYgMCBSIC9YWVogMzYuMCAyMDMuMjYgbnVsbCBdDQplbmRvYmoNCnhyZWYNCjAgMTINCjAwMDAwMDAwMDAgNjU1MzUgZg0KMDAwMDAwNjM2NyAwMDAwMCBuDQowMDAwMDA2NDIyIDAwMDAwIG4NCjAwMDAwMDY1MDQgMDAwMDAgbg0KMDAwMDAwNjU3MiAwMDAwMCBuDQowMDAwMDAwMDEwIDAwMDAwIG4NCjAwMDAwMDMxMzIgMDAwMDAgbg0KMDAwMDAwMzI5MCAwMDAwMCBuDQowMDAwMDA2Njg5IDAwMDAwIG4NCjAwMDAwMDY3OTQgMDAwMDAgbg0KMDAwMDAwNjkwNCAwMDAwMCBuDQowMDAwMDA2OTU1IDAwMDAwIG4NCnRyYWlsZXINCjw8DQovU2l6ZSAxMg0KL1Jvb3QgMSAwIFINCi9JbmZvIDIgMCBSDQovSUQgWzxhMjllYTY3NGFmMGI5N2FhNTBkOGFhMDg1YmFmNjVkYz48YTI5ZWE2NzRhZjBiOTdhYTUwZDhhYTA4NWJhZjY1ZGM+XQ0KPj4NCnN0YXJ0eHJlZg0KNzAwNg0KJSVFT0YNCg==";
        fetch('data:application/pdf;base64,' + this.downloadedBlob, {
            method: "GET"
        }).then(function (res) { return res.blob(); }).then(function (blob) {
            _this.file.writeFile(_this.file.externalApplicationStorageDirectory, name, blob, { replace: true }).then(function (res) {
                _this.fileOpener.open(res.toInternalURL(), 'application/pdf').then(function (res) {
                }).catch(function (err) {
                    console.log("open error");
                    _this.utils.showAlert("Error in opening payslip pdf", _this.constants.COMMON_APP_MESSAGE.ERROR);
                });
            }).catch(function (err) {
                _this.utils.showAlert("Error in saving payslip pdf", _this.constants.COMMON_APP_MESSAGE.ERROR);
                console.log("save error");
            });
        }).catch(function (err) {
            _this.utils.showAlert("Error occured", _this.constants.COMMON_APP_MESSAGE.ERROR);
            console.log("error");
        });
    };
    PayslipPage.prototype.getPayslipBlobDataFromServer = function () {
        var _this = this;
        var payslipObjs = __WEBPACK_IMPORTED_MODULE_4__models_PayslipListModel__["a" /* PayslipListModels */].getPayslipDataList();
        this.storage.get("UserInfoObject").then(function (_userData) {
            if (_userData != null) {
                //object = UserdataModel.fromJSON(_userData);
                _this.userName = _userData.UserName;
            }
        });
        this.options = {
            "RESTHeader": {
                "xmlns": "http://xmlns.oracle.com/apps/fnd/rest/header",
                "Responsibility": "GLOBAL_HRMS_MANAGER",
                "RespApplication": "PER",
                "SecurityGroup": "STANDARD",
                "NLSLanguage": "AMERICAN",
                "Org_Id": 81
            },
            "InputParameters": {
                //"personId": "546749",  
                "personId": this.userName,
                "legCode": "ZA",
                "bgID": "107",
                "actContextId": this.actionContextId
                //"actContextId": payslipObjs[this.selectedIndex].ActionContextId
                //"actContextId": "754197223"
            }
        };
        this.networkService.isNetworkConnectionAvailable()
            .then(function (isOnline) {
            if (isOnline) {
                _this.utils.presentLoading();
                _this.networkService.getPayslipBlobData(_this.options, _this.authToken).then(function (data) {
                    var _data = JSON.parse(JSON.stringify(data));
                    _this.utils.closeLoading();
                    _this.downloadedBlob = _data.getPayslipPDFBlob_Output.OutputParameters.Output.PayslipPDFBlobBean[0].PayslipPDFBlob;
                    setTimeout(function () {
                        _this.downloadBlobToPDF();
                    }, 500);
                }, function (err) {
                    console.log(err);
                    _this.utils.closeLoading();
                    _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.SERVER_DATA_ERROR, _this.constants.COMMON_APP_MESSAGE.ERROR);
                });
            }
            else {
                _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.NO_NETWORK_TITLE, _this.constants.COMMON_APP_MESSAGE.NO_NETWORK_MESSAGE);
            }
        });
    };
    PayslipPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-payslip',template:/*ion-inline-start:"D:\Projects\TFG_Next_Step\workspace\TFGNextStep\src\pages\payslip\payslip.html"*/`<ion-content class="bg-img" no-bounce>\n\n  	<div class="back-btn" (click)="goBack()">\n\n        <img src="assets/imgs/payslip_back.jpg" alt="Chatbot-Icon">\n\n      \n\n      </div>\n\n  <div class="rectangle-1">\n\n  </div>\n\n  <div class="rectangle-2">\n\n  </div>\n\n  <div class="card-align">\n\n    <ion-card class="login-card">\n\n      <ion-card-header>\n\n        <p class="login-style" text-center>Payslip</p>\n\n      </ion-card-header>\n\n      <ion-card-content>\n\n        <p class="payslip-text-style">Select specific period to\n\n          <br/> download payslip</p>\n\n        <div class="payslip-drop">\n\n          <!--ion-select multiple="false" placeholder="Month" disabled="true"-->\n\n         <div class="payslip-dropdown">\n\n           <img src="assets/imgs/drop_down.svg">\n\n         </div>\n\n          <ion-input readonly text-wrap class="inputBox" type="text" name="Month" placeholder="Month" [(ngModel)]="monthSelected" (ionFocus)="monthPopup()">{{monthSelected}}</ion-input>\n\n            <!--ion-option *ngFor="let month of months">{{month.value}}</ion-option-->\n\n          <!--/ion-select-->\n\n          <!--ion-select multiple="false" placeholder="Year" disabled="true"-->\n\n          <div class="payslip-dropdown">\n\n              <img src="assets/imgs/drop_down.svg">\n\n            </div>\n\n          <ion-input readonly text-wrap class="inputBox" type="text" name="Year" placeholder="Year" [(ngModel)]="yearSelected" (ionFocus)="yearPopup()"></ion-input>\n\n            <!--ion-option *ngFor="let year of years">{{year.value}}</ion-option-->\n\n          <!--/ion-select-->\n\n        </div>\n\n        <button class="export-btn" ion-button color="primary" block (click)="exportPayslip()">\n\n          <img src="assets/imgs/payslip_download_inactive.png">\n\n        </button>\n\n        <ion-row>\n\n          <br/>\n\n          <br/>\n\n        </ion-row>\n\n      </ion-card-content>\n\n    </ion-card>\n\n  </div>\n\n</ion-content>`/*ion-inline-end:"D:\Projects\TFG_Next_Step\workspace\TFGNextStep\src\pages\payslip\payslip.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__ionic_native_file__["a" /* File */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_opener__["a" /* FileOpener */], __WEBPACK_IMPORTED_MODULE_3__utilities_common__["a" /* Utills */], __WEBPACK_IMPORTED_MODULE_8__providers_network_networkcalls__["a" /* NetworkProvider */], __WEBPACK_IMPORTED_MODULE_9__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2__utilities_constants__["a" /* Constants */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_in_app_browser__["a" /* InAppBrowser */]])
    ], PayslipPage);
    return PayslipPage;
}());

//# sourceMappingURL=payslip.js.map

/***/ }),

/***/ 175:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 175;

/***/ }),

/***/ 220:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/chatbot/chatbot.module": [
		699,
		3
	],
	"../pages/login/login.module": [
		700,
		2
	],
	"../pages/payslip/payslip.module": [
		702,
		1
	],
	"../pages/signup/signup.module": [
		701,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 220;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 320:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_UserdataModel__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__chatbot_chatbot__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__utilities_common__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__utilities_constants__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__payslip_payslip__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_app_version__ = __webpack_require__(323);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__models_DeviceConfiguration__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_network_networkcalls__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__models_PayslipListModel__ = __webpack_require__(321);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_storage__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_file__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_file_opener__ = __webpack_require__(140);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};















var HomePage = (function () {
    function HomePage(navParams, file, fileOpener, appVersion, networkService, platform, navCtrl, app, alertCtrl, utils, constants, storage) {
        var _this = this;
        this.navParams = navParams;
        this.file = file;
        this.fileOpener = fileOpener;
        this.appVersion = appVersion;
        this.networkService = networkService;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.app = app;
        this.alertCtrl = alertCtrl;
        this.utils = utils;
        this.constants = constants;
        this.storage = storage;
        this.recthide = false;
        this.listCardsItems = [];
        this._appVersion = "";
        this.authToken = this.navParams.get('AuthToken');
        /*this.appVersion.getAppName();
        this.appVersion.getPackageName();
        this.appVersion.getVersionCode();
        this.appVersion.getVersionNumber();*/
        this.deviceDataObj = __WEBPACK_IMPORTED_MODULE_9__models_DeviceConfiguration__["a" /* DeviceConfiguration */].getDeviceConfiguration();
        console.log("Device Platform - " + this.deviceDataObj.platform);
        if (this.deviceDataObj.platform === 'iOS' || this.deviceDataObj.platform === 'Android' || this.deviceDataObj.platform === 'android') {
            this.appVersion.getVersionNumber().then(function (version) { _this._appVersion = version; });
        }
        __WEBPACK_IMPORTED_MODULE_3__models_UserdataModel__["a" /* UserdataModel */].setAutoLogin(true);
        localStorage.setItem("isAutoLogin", "true");
        this.getPayslipDataFromServer();
        //this.getPayslipLocalData();      
        /*this.listCardsItems.push({title:"January'19"});
        this.listCardsItems.push({title:"February'19"});
        this.listCardsItems.push({title:"March'19"});
        this.listCardsItems.push({title:"April'19"});
        this.listCardsItems.push({title:"May'19"});
        this.listCardsItems.push({title:"June'19"});*/
    }
    HomePage.prototype.hidediv = function () {
        if (this.recthide)
            this.recthide = false;
        else
            this.recthide = true;
    };
    HomePage.prototype.ionViewWillEnter = function () {
    };
    HomePage.prototype.ionViewWillLeave = function () {
        if (this.recthide)
            this.recthide = false;
    };
    HomePage.prototype.logout = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Exit',
            message: 'Do you want to exit app?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Ok',
                    handler: function () {
                        localStorage.removeItem('Authenticated');
                        __WEBPACK_IMPORTED_MODULE_3__models_UserdataModel__["a" /* UserdataModel */].setAutoLogin(false);
                        localStorage.setItem("isAutoLogin", "false");
                        //Api Token Logout 
                        var root = _this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* Login */]);
                        console.log('Ok clicked');
                    }
                }
            ]
        });
        alert.present();
        //root.popToRoot();    
        //this.navCtrl.setRoot(Login);
        //this.navCtrl.popToRoot();
    };
    HomePage.prototype.getPayslipLocalData = function () {
        var _this = this;
        this.networkService.getPayslipStaticData().then(function (payslipData) {
            var _data = JSON.parse(JSON.stringify(payslipData));
            __WEBPACK_IMPORTED_MODULE_11__models_PayslipListModel__["a" /* PayslipListModels */].fromJSON(_data.getPayslipListDetails_Output.OutputParameters.Output);
            var payslipObjs = __WEBPACK_IMPORTED_MODULE_11__models_PayslipListModel__["a" /* PayslipListModels */].getPayslipDataList();
            _this.utils.closeLoading();
            for (var j = 0; j < payslipObjs.length; j++) {
                _this.listCardsItems.push({ title: _this.utils.formatPayslipTimestamp(payslipObjs[j].EffectiveDate) });
            }
        }, function (err) {
            console.log(err);
            _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.SERVER_DATA_ERROR, _this.constants.COMMON_APP_MESSAGE.ERROR);
        });
    };
    HomePage.prototype.getPayslipDataFromServer = function () {
        var _this = this;
        this.storage.get("UserInfoObject").then(function (_userData) {
            if (_userData != null) {
                //object = UserdataModel.fromJSON(_userData);
                _this.userName = _userData.UserName;
            }
        });
        this.options = {
            "RESTHeader": {
                "xmlns": "http://xmlns.oracle.com/apps/fnd/rest/header",
                "Responsibility": "GLOBAL_HRMS_MANAGER",
                "RespApplication": "PER",
                "SecurityGroup": "STANDARD",
                "NLSLanguage": "AMERICAN",
                "Org_Id": 81
            },
            "InputParameters": {
                //"personId": "546749",  
                "personId": this.userName,
                "legCode": "ZA",
                "bgID": "107"
            }
        };
        this.networkService.isNetworkConnectionAvailable()
            .then(function (isOnline) {
            if (isOnline) {
                _this.utils.presentLoading();
                _this.networkService.getPayslipData(_this.options, _this.authToken).then(function (payslipData) {
                    var _data = JSON.parse(JSON.stringify(payslipData));
                    __WEBPACK_IMPORTED_MODULE_11__models_PayslipListModel__["a" /* PayslipListModels */].fromJSON(_data.getPayslipListDetails_Output.OutputParameters.Output);
                    var payslipObjs = __WEBPACK_IMPORTED_MODULE_11__models_PayslipListModel__["a" /* PayslipListModels */].getPayslipDataList();
                    _this.utils.closeLoading();
                    for (var j = 0; j < payslipObjs.length; j++) {
                        _this.listCardsItems.push({ title: _this.utils.formatPayslipTimestamp(payslipObjs[j].EffectiveDate) });
                    }
                }, function (err) {
                    console.log(err);
                    _this.utils.closeLoading();
                    _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.SERVER_DATA_ERROR, _this.constants.COMMON_APP_MESSAGE.ERROR);
                });
            }
            else {
                _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.NO_NETWORK_TITLE, _this.constants.COMMON_APP_MESSAGE.NO_NETWORK_MESSAGE);
            }
        });
    };
    HomePage.prototype.callPayslipPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__payslip_payslip__["a" /* PayslipPage */], { AuthToken: this.authToken });
    };
    HomePage.prototype.callChatbotPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__chatbot_chatbot__["a" /* ChatbotPage */]);
    };
    HomePage.prototype.downloadPlayslip = function (index) {
        this.selectedIndex = index;
        this.getPayslipBlobDataFromServer();
        //this.utils.showAlert("Work in progress",  this.constants.COMMON_APP_MESSAGE.ALERT);
    };
    HomePage.prototype.searchClick = function () {
        this.utils.showAlert("Work in progress", this.constants.COMMON_APP_MESSAGE.ALERT);
    };
    HomePage.prototype.downloadBlobToPDF = function () {
        var _this = this;
        var name = this.listCardsItems[this.selectedIndex] + ".pdf";
        console.log("Downloaded Blob - " + this.downloadedBlob);
        //let downloadPDF: any = "JVBERi0xLjQNCjUgMCBvYmoNCjw8DQovVHlwZSAvWE9iamVjdA0KL1N1YnR5cGUgL0ltYWdlDQovRmlsdGVyIC9GbGF0ZURlY29kZQ0KL0xlbmd0aCAyOTA3DQovV2lkdGggNDMyDQovSGVpZ2h0IDcxDQovQml0c1BlckNvbXBvbmVudCA4DQovQ29sb3JTcGFjZSAvRGV2aWNlUkdCDQovTWFzayBbMjU1IDI1NSAyNTUgMjU1IDI1NSAyNTVdDQo+Pg0Kc3RyZWFtDQp4nO1dS3IrKwz1nnojWUVWkUG24T15/LZwqzJKZqnKoB807U67P3AQAiRHp1SuXF/bzUccJCFgHA0Gg+FJ8fPZuwQGg8FgMBgMBoPBYDAYDAaDwWAwGAwGg8FgMHDg59PJNze0l19E1ci43cbrdZbbTUeZJ8wtPHXo/Mr6fviv42evP3xPC1HUdLnYN1GoLO/7pw3oVNRJj/Sbj48PPy7e3sfX13EYvFyG+Q+aXFa/EP8p90T33Ovt6+uLrS6b5xLqgpd//8X1Hy8vvoLXK1ftGOBa2xfv8iDuHacAXXCi8L7FrrcHnWws7ulrxIvRq/X2yCcQz0hhfuzX2mtW9EPYFWOtnLdbpPxsuN2OR0cvcexRTh2uUt0rciiui9t0awTxvn556W/thIHZXSddATbEEi+SG0cK4ceaBAZYtd5sz4Q318pQc8bxD3WP69sI0fYhs6KvWvfyR8Rxfi9WdBqFFK8XXLO4p3fvoCD7PnoyPnQVXNtgHeXROJx1YKHBYByGolYYOP7REuZfoJWIM4JY+3Atzd2r32k3Ke3pWhQTXk7ILV5CRXwohwn3+hYGb2jMwITBoXa2eo1GDr/fvQVwybempNuHizSmRHwIuE+2hLT5a2OuLHgOPkR8hJayUbZQvLDA55gq+PLBZwmBTSb435fsIMclizqkja9YvVpZYrlt0sxEFKiTZ3VXzoc+ACXKCL8cTT13PvQGYViUvFzm0BkjH6pwkFNNBw5S33rdS4tLg3FEmSDqr+jJdFUiM69qPvSGVu+2BXRsHryO+hwfBt/wcl+IvDB5LtIs5BIBBqkaf3mRmo7z7HcQSnXmNrJApk7G15L08qGoaGFS7YPLMOXg+X+G2HLQ4fI5+qrHeQQl2SaK/OUgw1AxO7HEIa1E1FI7yM+kEWjhw02akEwyPJ96fv169xryIbm0kaZ4wxBSiH1hqkoICBCKF6fEZK1r1yuI71O4dpVGUznzsHvNZJ0Mo6OeJGuqhQ/XoM2Gw32XRDVJTD3jY4iPI3E3I7/icmfs5tso5sT4t8yM0GjacNpfbrUDaNphgUZu0xqSCd9E5fFz1nRE3yBZHR1yroRsJ1THh7lTz8QA7HpYAs7dr/hY6JghvEbWok9E/VJq0Jjz0XU9ducUidEhDc5YMNx3m7bLiaDBBer4EDcOJeycqgp8apCz7zI3KeisB5N1b971kKnGaolBi0pLvmvqYzw2Ax7KfnuXxYQBuvhQJwPUAsgqMicFsCvPFt9TX+/iESDrvJyGK2SRTr2P0FRxngO6zD1I1clRGx+C7uEfIENU9yQ3BebrHVoRadNI7PZhrvRspPXWFIeMncJGA513sWQ4quJD0KKQ3NqMQHRPMhkGILGmow4Vy4fpRHGOToHW0YZHIxkZPgXpiOgELXx4KuJD3Dt4esz7XKJN0fEMExjQIDqsiDR/eb2c3WBMIfPInniRAAu5eMgELZwMRz18iMxuckpbG8nW2B/sJhZIz+7Hkbz1lF/Eaad8OxLSYmezYT0TLmmuND5BggYtfJic2iScctkMSfNAvqe8RrJzd9UR6y97VOVDxDWIVB/zmnMLBW0nl28cjjr4EPKqVLQ2F+KtwXdgThukR9Pe2pFsH8Znq8IxVe7zIr52biEBZ1nQZQoRaODDtPKrMMW5IJkKyEgN863xLy1+CBesyHQHF3CjQLe0ZJUz+YNaHBYVfBgfLFV3ygtEqjU0xg2AZdlHkhc7KSStL2rB0CPOEOZhT9VomXVZFSr4MDH7DHX3gOfK62td4yQ+4vSayvEx9TjMhcYPgXUuOjMUpGseADERMV2ihDvEQjwf0s926yW1LTTxXUZETthNIh8i2+KosxV/dh+yYwULRP/791+lWneA+MGl7CTkBiMx3mVaAjV7ZC3LCjvPAT2ngqwbNRZBEBMR+c2qIdPGEM+Hyk5CbtBi8XGncTElgJUPN+3wfYfPV/n5XP7+JmH54nzYF7iNlOw4VNpawhVFND5sCE182GYtw/gQ0Ip1CPfX2QyX1yyvZFm+nqUeJFqouw+OJSZpfNgQmviwDRcl/OU/wYfZ68vdLzYirykgCYfkAB2Y3R0nNOPDhlATP2yWBW3xw5GSf5h9ljiv0OZKyKUtyzfDvOZYygRyd4MWiOdDNfZhMyKquv2hI7JUkZZ/2OuSI7JutDlzG1msid8NFP+urS/zwccu8Ct7eknLLXJxO0pRrtcK6SgZV/5he8eZzFdIcI9j2/5EaHSveRqhYkZHIcTzoYf8/MOWqxipYaJmL8AamfZeUf5hS0qkkiHq3XMpHrSwcq5aT3O8gAo+TNjz0/1cHaVxXz9T+HpBatVgOxLL9i/zXEuXlBI/sUbCYRwlO1aSpRXCJEmo4MPcwfLUmO5zjOqeIvdkQjoksq9R8f5l/9B6t3gXnoqPpcE4TVjualy85vU7B5mT5+8XpSMmC6xFJ1XwoZ1vs4Gdf8i1Xy/r/lOEBl3XFDY+6M7fbrNPzShgHXdtCy16qnCZNfAhlJKqorW5QDtTWiYA0+LA/uc73wa9OnkRx3j3gzvmP6aYCY+TgiYE3muHWJLscpihnZyjVZy8pIEPPYBRI+rS+epIDmEVVwYgxH5o/POef+gaKusW9XpzDSH7up7Xn8UMz3GjhxI+hAxynakmRLRKxqiHKRBKXUKtcf4heJ81+feTwLKvN1TfLUF33wJtsiWrQgkfeiDz4J8JJKLbWiXPEcjwOYnD1zrvC3c/uS9VRzv0kE+6eM372RZckZFMiYr4EGxtUWWuCnAUyGwQ0Bg7KXzF8w/xPSy8jjPSIJHZrUH6EMJsYDFcZWUGcxTx4QgPor9hJWbs7Ha9LGd5JSMd+jwmXPX+lCzHmcXaKT96q9NWxG0795pNcnHGxnHNFMaHqE8xMcCfWF7JGrnuw31Z0T09K/gfOaun9v0pWR5o+TAp20FMKTOX7EuVVQxXcTkz9ajwsJSsCE93BqgMyj4L9/mwp6a+wzLn+rpn4YemrkfKOVrcF4CcqL9qVfLSVVHkcIMuO1vJXvPvUB3CUA0Z5vtzesM74y6ZvPD9cZXHDpX87T2e1p5Meie/f1CXLOXZjKx7hliD8j+8NuEc+ijgzeYlp/gS6KXN/XpZO53JDiAWOUw0yFJg3svREGbbmYho5kAvndzIuteQNDYhskxDJaGSxmVus7zb/bBTXhmAvV3N7l+uTYnsV3/yAswP3xdPi05uJpouy1IE2WxS6BIqIUizgINXv96VZepoiMoa3r/8nRuUyOp0hDT6hvGxTNeDL6qgxI2eaOHDfWhdPiU23qCkQv3i8vIC7nprfd9o1gaWC8xgvFcq1wM50Vq4Tu67SQUfnoXWvZEguLWb+zjZW3FFyesrPvD73L+ctZqfrI5wTzm/qIeG/XfVc4RK5DAsI5m97xKzGcROQB19HMJibl/x6ZGZgYVm8cN92+L6dp72ic5cchLeCveH8p4jVC5n24vkD5x4NObn06uWUxtRre1atXv6vetu+Z1LTj9rGD/cwOtb+eEPJYev9kLZhllPlXJYMXKdRPeyRQQOTU+tLcNWHCQdLCOWFcu2zCT35lTPxs9LLH9UYyyzUdopx+mDiIPEu9UNje7+S3yikendX6g5rt0ZQELAZ4OQliaBGJds8PIade2F7EXnezhxOsQV+LzArRBjxsHd6Z9asvQb2zCp9Sm5Ac8SlQ4M0N5clObjPMIratDD5UTT2sJ+YuoEzyrLiaz717f3BsaVHzj7py+13ryzRAbCpsVkIrRMOOsuWfLMGMj3soMpqCXSOGTByxYIpGphsoRpftwyQGWR5uMYDAaDwWAwGAwGg8FgMBgMBoPBYDAYDAaDwWAwGAwGg8FgMBgMBoPBYDAY/ib+B2eP9zBlbmRzdHJlYW0NCmVuZG9iag0KNiAwIG9iag0KPDwNCi9UeXBlIC9QYWdlDQovUGFyZW50IDMgMCBSDQovUmVzb3VyY2VzIDQgMCBSDQovQ29udGVudHMgNyAwIFINCi9NZWRpYUJveFsgMCAwIDYxMi4wIDc5Mi4wIF0NCi9Dcm9wQm94WyAwIDAgNjEyLjAgNzkyLjAgXQ0KL1JvdGF0ZSAwDQo+Pg0KZW5kb2JqDQo3IDAgb2JqDQo8PCAvTGVuZ3RoIDI5OTggL0ZpbHRlciAvRmxhdGVEZWNvZGUgPj4NCnN0cmVhbQ0KeJytW99z2zYSfveM/wc83bhzDkv8JvJmx3aaTJPmbPfmOtMXRqJtXiTSJaWmvr/+PpCgREqAIjJNJkqs5WKx2P12PwDMH0SJKCExftM4UpIIQbQQkZaKzJbkx3fLmFyV5F+nJxEjkSCRIrdvyebf1ePpyeX96QltR8AYCUbrBrhfnp78eENPTwhl5P7h9OTsU/pSlYvFD/f/PT2JGx2M1v7DDtUNwxIRGTsRFTHjhmF2mHgzTL3In5thOh2hecRgVAsTCUF7xnWn9JgNNGQcR2Lw/BltHsDPGt5xSiKdiMbh4Td2rtdwG35iyTSVkaISs8N4khj7UWXkwevicLXgX8IwQBLFtO+om/L18nlRvmTDaVMZR4lslJjUHj+fSmgVX7LqnHyoIvKhfEqXy2xOPqZfs81f4xzllMFYz1MWxWM85RKKNrW+4WmF2S2zSUHAWlI1IQiJimKloY/MdTM7ezfPilW+eiEf18vPWeVd/thgCTzLnxjKKJWMAwti2ipvXBm7ykpGWqmBL4NV/qV6TIv8f+kqL4sheFhsTQWd4rGOY8rJK/LT7bsP5O6lXmXLekqclFGRpmp8nLSOEgN1E3F1ACoHQmZ1VezxjlJqOJWTgrX1Z2ywtIyEVmGPUOTIZVrntS9SQV/ub94C8cXqafFC7tJFWr1MilKiI6YmRImii1CUNJWYyPhS8Odytp9+mwBB2Zd+1qn7p4zclrMv5HKdL+Z58TgtXBvHxoYrSSJ+wK+3VTrP/JEKONVoEDMpPBYMU0BkwyNR7VQCRV/SvS8/k/t8tfC2HKulfUl3UaQLlITXqA7TakMXnY1fo6MTR4bLsF+fyjoPVb2gW5QqkURbn4jzc1LIFPJnSn+ylEoh8TT1d86fcgxzla78EYOWlyQw+up9WrxiKOrTIrVxZ2ykLN1iB/z5pcofc6wz8TvWxexvd8yFSSbA94Qwaay3AbAUBcA8bl3Udf5YLMErDnUoaCd/e4fauDQaVDJisQz71Gfzu/EJeWJL+U1Zz57yIidvq3L93LWrSeGy7YaKLlxAaxKzIwOmIs6xOAwDHOITF/N5ldW1N1xQ5r4kvFtVWdYF+jURXmV4wOBCQzidQtnyX1I+kJu0Wr4m/y7Lqn4uwdpvy3ROvONwsEXN23HeYpjiKf0z8/dXBvaduK3O3frzuvpsTV7l9arKZyjfF4hDWWTn5L78WljRG5Dg1+RN+pz5bVN0w8SRZqvjf4g20GgeQh1eAdtvynn2GpROTWPI27A3KT0m7Bx7SpCnbwa+8ga+y+5Q4Lki77PiS17UZUHaoHr1+7H/lFblV3Kd1v5H++HV2LK2C2aQu5HBUkQJ2JZdsOE33YJJyzKJAq9PFOSSoBjImLq1clJXyyHF9izB4gykjjFBivRB+R5Iux1hZAuLdiJ4F7bZCgMmW2HAYiMMG9RMNORBcpQrK9+bjNiVRbpZL6SB3VvYVRx8YRdR2OnLqB2XYK78uDyT9mxBYnNpBA9WTvKueCir5ZYQRwZumwR/jItq7+dN6cOU2upFtR49JZ0g710+fcqqvJyT62K+bbRHTYGhg0+aA2N6dxL36V+kncjx9jkyZpJ9EYu9RUhfmrZ8U2V/rLNi9rJzSGSfVyaSzHOmxOmrD2ll2YYZaDk3e2ruaGlnHv0H3qSLrJinVdsP27XQSP7tR5Ogg2963VAjKxSsJkkDHOxKJetB9YCYMbojphqVpBNzkxwSO9MIRpx4sNfT9AsNhycBmT0GpBNBi8BJMw21trprT2uwmToZsYI2ZzNTICt4JIXZouVula7W9Ui0TrLv8nh3ArfZQ1YBKhn5WI4E7aRpOLT0p2FPR/Y2B+1ycWSNrzl/tIFb+HDa0ziLOY1NLKmRPsD2n7Tgv7jdgn8sWjkDjMQGjnQHrQFxh9atuIEj3UFrQOxMSwNS4WuVukUOWFJA6Mb1CxFcud+BjwQslU1NmgJYuCi53Afs3Xq5TKsXy6ddlR+H2Vg1hW5cslKUO4PyIbFx17HsyLGlvddpVeTF41joTpoGB3FMqPZN4yqbr2e2go2YSAPeSRORSF97MeOZyEfseRCY4a4BbEtYqhbHmx493OEn5wy5wtlAjSuwgcbKVu0MT9q9wbD1Sm72DJxRc24wgqRTkCyM3VeF2m5A6nC8lfqabkDq4IZVkDHzY9Fp+nEKpsx5QNEGWek94XEgFhqtUxCm7ZnnWBgLLSLKPGx5PGzsVBS6BSp8PLrfCiUjZlxiXGX1rMqfx/V7hig7+/gctRIs4RGndDiJi2W5Llb7rU5IZFTs4aVv0vqpf/y/GR3EV9Gh4hmj5zH26DuAag0APmiHDrNV+me2IHezp2w57LssYchENXxcnzP0zG7DahN/+9Hkz+CbHURhlx7rZgvIAXUhOlhQ9N+glEm6o8wspPkQGIgIU02Kqn3UONWAFNkN7wJCKSJsa0NSe0DN9oRHoIqz9l7eIUvC4TH5xLEE9ogkhK1JvcBNqc1wati0KX03zGxoNzAbg3LEhKpvY6ybaAhl4OEPD+RiNttHpwDBMjs4E0J2cNi10MfNp4vfrncG401rGzwlznmCviq8w6HDgRM5v+bLbH4xn//TP8H+o8wEJ8jj5myprQP2ijGb5zN08It8Tq4D0+3r0HPb0QKDMyQQ59vBb7NVXmXtpnxdhA309ei5bbeJ9Bqgdo/povDruxvifa9isyT9pynWXLNxJUxS1kAeNdVeKwHsKm4P5Qa0oBE3vV2ZSNNN52dN4T0sdTVMWo+31cQJuxrmlzJ73sZCUmc2IHX1zy/s6p9f6uqfX8jtAYEIzhj7kT3ZcXyEG4GdgZpISLgBp/SdEd9UoCMZucyK7CFf1SRFkgJizQ1j2p7qjeIq9qZFo39OICs8sWtnvpOsdBOYylYGswixFa6TzUXAkK2UxapKZyt7zYEtPXnOKvJTua52OEbStPr+KGcUABfGZwo50+01esPbUetm/O0h276Bni7FpmAaheEqiRh4dEdS+IDChKSOwvTEHgpjo4XOEqAanW5ACkSwJKirVXNOHiAxMdKD7usez2I2eJxIY4KI/O3+yjbi9XK9aM7mavIPcgn+W8yy8cSmQ8NUZvP9mLQR32JyArX5FiC7mYYgyZVlLelf6edFhno2K5cZub15591IDxDp30xszPWgJSiabL8bkwao+ef13osaXWseQnPbmnfNKICo29nvmqmOMNPXP2BGAg7d6b2I8U2fFR2w0tKXgfohfoSiFEn3HuwZT2g8ZF+3nSm/lb7yQSuoQ4brbtEA886KPfF9U+GHFfm1dm+O7i1ZX1vZq0u/EaZR7FwAue7lmNtcXiwW5VeLWr8zffXB/nLXDrXHJ7xbMti5qGt72LSuZk8pnCA3l34/+ooM7N1o//hxA7Mu9JR8yooake4I6+GY9LUPEVceb9+8sJHXO2a+Efq+Njvn9kbEu19gqHNGb0IfsybkKKneYYdP97chYxkyMyCFoj2kRmtJxM4ReF8ODaXpgBh+Q8wFAzcI8WDO0IFoiJHyGFstFiKzm/7rlXbTCuh23dkvRfuPk6BdziMTB0fuOrtfSrETDZllxmzaske1IwV+et6RgoBDJvJsCo5j70xjb8/FlCsBpm149T5RcEfO7qWXXnk+lqszBS6EIFDe5PDYeYFPiqR7GcbdPH/IVk9jbr+p1g08ps6EJg3NH8zlMi2+kMsKpfepeTtnBGmy75ROnwyn6BzDubgTlf6LcsdFR9j/AvIdEXLkaTiZwG6GSftCutinTld5lc1W5Cp7tu+c7txttAvfU0WTiZXcqf7tkvSf+k/7yzDBvRxs8PDuZcbR+xa3zcYS+G8celLfTUdIbPNVBoe2+cPCYmc5Yc0bX94rC1snWBISIhsEOrpf2BideNnBuH3FQE8qT0hNnuxXpw9ZXaeP2e9n9e8/eFKO8UgnZj/lPpakr0lukLLzKZvW9lJK7V0bdwEOiJ0ytxfHgTunVlUGhNzeR+td4f8BrDZa5g0KZW5kc3RyZWFtDQplbmRvYmoNCjEgMCBvYmoNCjw8DQovVHlwZSAvQ2F0YWxvZw0KL1BhZ2VzIDMgMCBSDQo+Pg0KZW5kb2JqDQoyIDAgb2JqDQo8PA0KL1R5cGUgL0luZm8NCi9Qcm9kdWNlciAoT3JhY2xlIEJJIFB1Ymxpc2hlciAxMC4xLjMuNC4yKQ0KPj4NCmVuZG9iag0KMyAwIG9iag0KPDwNCi9UeXBlIC9QYWdlcw0KL0tpZHMgWw0KNiAwIFINCl0NCi9Db3VudCAxDQo+Pg0KZW5kb2JqDQo0IDAgb2JqDQo8PA0KL1Byb2NTZXQgWyAvUERGIC9UZXh0IF0NCi9Gb250IDw8IA0KL0YxIDggMCBSDQovRjIgOSAwIFINCj4+DQovWE9iamVjdCA8PCANCi9JbTAgNSAwIFINCj4+DQo+Pg0KZW5kb2JqDQo4IDAgb2JqDQo8PA0KL1R5cGUgL0ZvbnQNCi9TdWJ0eXBlIC9UeXBlMQ0KL0Jhc2VGb250IC9IZWx2ZXRpY2ENCi9FbmNvZGluZyAvV2luQW5zaUVuY29kaW5nDQo+Pg0KZW5kb2JqDQo5IDAgb2JqDQo8PA0KL1R5cGUgL0ZvbnQNCi9TdWJ0eXBlIC9UeXBlMQ0KL0Jhc2VGb250IC9IZWx2ZXRpY2EtQm9sZA0KL0VuY29kaW5nIC9XaW5BbnNpRW5jb2RpbmcNCj4+DQplbmRvYmoNCjEwIDAgb2JqDQpbIDYgMCBSIC9YWVogMzYuMCAyMDMuMjYgbnVsbCBdDQplbmRvYmoNCjExIDAgb2JqDQpbIDYgMCBSIC9YWVogMzYuMCAyMDMuMjYgbnVsbCBdDQplbmRvYmoNCnhyZWYNCjAgMTINCjAwMDAwMDAwMDAgNjU1MzUgZg0KMDAwMDAwNjM2NyAwMDAwMCBuDQowMDAwMDA2NDIyIDAwMDAwIG4NCjAwMDAwMDY1MDQgMDAwMDAgbg0KMDAwMDAwNjU3MiAwMDAwMCBuDQowMDAwMDAwMDEwIDAwMDAwIG4NCjAwMDAwMDMxMzIgMDAwMDAgbg0KMDAwMDAwMzI5MCAwMDAwMCBuDQowMDAwMDA2Njg5IDAwMDAwIG4NCjAwMDAwMDY3OTQgMDAwMDAgbg0KMDAwMDAwNjkwNCAwMDAwMCBuDQowMDAwMDA2OTU1IDAwMDAwIG4NCnRyYWlsZXINCjw8DQovU2l6ZSAxMg0KL1Jvb3QgMSAwIFINCi9JbmZvIDIgMCBSDQovSUQgWzxhMjllYTY3NGFmMGI5N2FhNTBkOGFhMDg1YmFmNjVkYz48YTI5ZWE2NzRhZjBiOTdhYTUwZDhhYTA4NWJhZjY1ZGM+XQ0KPj4NCnN0YXJ0eHJlZg0KNzAwNg0KJSVFT0YNCg==";
        fetch('data:application/pdf;base64,' + this.downloadedBlob, {
            method: "GET"
        }).then(function (res) { return res.blob(); }).then(function (blob) {
            _this.file.writeFile(_this.file.externalApplicationStorageDirectory, name, blob, { replace: true }).then(function (res) {
                _this.fileOpener.open(res.toInternalURL(), 'application/pdf').then(function (res) {
                }).catch(function (err) {
                    console.log("open error");
                    _this.utils.showAlert("Error in opening payslip pdf", _this.constants.COMMON_APP_MESSAGE.ERROR);
                });
            }).catch(function (err) {
                _this.utils.showAlert("Error in saving payslip pdf", _this.constants.COMMON_APP_MESSAGE.ERROR);
                console.log("save error");
            });
        }).catch(function (err) {
            _this.utils.showAlert("Error occured", _this.constants.COMMON_APP_MESSAGE.ERROR);
            console.log("error");
        });
    };
    HomePage.prototype.getPayslipBlobDataFromServer = function () {
        var _this = this;
        var payslipObjs = __WEBPACK_IMPORTED_MODULE_11__models_PayslipListModel__["a" /* PayslipListModels */].getPayslipDataList();
        this.storage.get("UserInfoObject").then(function (_userData) {
            if (_userData != null) {
                //object = UserdataModel.fromJSON(_userData);
                _this.userName = _userData.UserName;
            }
        });
        this.options = {
            "RESTHeader": {
                "xmlns": "http://xmlns.oracle.com/apps/fnd/rest/header",
                //"Responsibility":"GLOBAL_HRMS_MANAGER",
                "Responsibility": "ZA_SHRMS_MANAGER",
                "RespApplication": "PER",
                "SecurityGroup": "STANDARD",
                "NLSLanguage": "AMERICAN",
                "Org_Id": 81
            },
            "InputParameters": {
                //"personId": "546749",  
                "personId": this.userName,
                "legCode": "ZA",
                "bgID": "107",
                "actContextId": payslipObjs[this.selectedIndex].ActionContextId
                //"actContextId": "754197223"
            }
        };
        this.networkService.isNetworkConnectionAvailable()
            .then(function (isOnline) {
            if (isOnline) {
                _this.utils.presentLoading();
                _this.networkService.getPayslipBlobData(_this.options, _this.authToken).then(function (data) {
                    var _data = JSON.parse(JSON.stringify(data));
                    _this.utils.closeLoading();
                    _this.downloadedBlob = _data.getPayslipPDFBlob_Output.OutputParameters.Output.PayslipPDFBlobBean[0].PayslipPDFBlob;
                    setTimeout(function () {
                        _this.downloadBlobToPDF();
                    }, 500);
                }, function (err) {
                    console.log(err);
                    _this.utils.closeLoading();
                    _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.SERVER_DATA_ERROR, _this.constants.COMMON_APP_MESSAGE.ERROR);
                });
            }
            else {
                _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.NO_NETWORK_TITLE, _this.constants.COMMON_APP_MESSAGE.NO_NETWORK_MESSAGE);
            }
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"D:\Projects\TFG_Next_Step\workspace\TFGNextStep\src\pages\home\home.html"*/`<ion-content>\n\n    <div  [ngClass]="{\'rectangle-1\':!recthide, \'rectangle-1-open\': recthide }">\n\n      <div class="tfg-logo" style="text-align: center; margin-top:5px;" *ngIf="!recthide">\n\n        <img src="assets/imgs/TFG_Logo.png" alt="TFG-Logo">\n\n      </div>\n\n      <div class="menu-item" *ngIf="recthide">\n\n        <div class="user-info">\n\n          <span>\n\n            <img src="assets/imgs/User-Pic.png">\n\n          </span>\n\n          <p>Elna de Lille</p>\n\n          <span>Customer Analyst</span>\n\n        </div>\n\n  \n\n        <button class="accordion">\n\n          <img src="assets/imgs/list-menu.png"><span>Personal Details</span></button>\n\n        <div class="panel">\n\n        </div>\n\n  \n\n        <button (click)="showHide = !showHide" class="accordion">\n\n          <img src="assets/imgs/list-menu.png"><span>Manager Self Service</span> </button>\n\n        <div class="panel">\n\n  \n\n        </div>\n\n  \n\n        <button class="accordion active">\n\n          <img src="assets/imgs/list-menu.png"><span>Employee Self Service</span> </button>\n\n        <div class="panel accordion:hover">\n\n          <ul style="border-top: 2px solid #4A134A;">\n\n            <li (click)="callPayslipPage()">Payslip</li>\n\n            <li>Salary Certificate</li>\n\n            <li>Leave</li>\n\n          </ul>\n\n        </div>\n\n        <div class="version">Ver. {{_appVersion}}</div>\n\n      </div>\n\n  \n\n  \n\n      <div class="toggle-btn">\n\n          <img *ngIf="!recthide" src="assets/imgs/bot-icon-home.svg" alt="Chatbot-Icon" (click)="callChatbotPage()">\n\n          <img *ngIf="!recthide" src="assets/imgs/search-tgl.svg" alt="Search-Icon" (click)="searchClick()">\n\n          <img *ngIf="!recthide" src="assets/imgs/logout.svg" alt="Logout-Icon" style="width: 22px; margin-bottom: 30px;" (click)="logout()">\n\n        <!--img src="assets/imgs/chatbot-tgl.png" alt="Chatbot-Icon" (click)="callChatbotPage()">\n\n        <img src="assets/imgs/search-tgl.png" alt="Search-Icon" (click)="searchClick()">\n\n        <img src="assets/imgs/logout.png" alt="Logout-Icon" style="width: 22px;" (click)="logout()"-->\n\n      </div>\n\n    </div>\n\n    <div class="t-btn" (click)="hidediv()">\n\n      <div class="menuButton">\n\n        <img src="assets/imgs/tgl-btn.png" alt="Toggle-Button">\n\n        <!-- <ion-icon name="menu"></ion-icon>  -->\n\n      </div>\n\n    </div>\n\n    <!-- <ion-menu [content]="mycontent">\n\n      <ion-content>\n\n        \n\n      </ion-content>\n\n    </ion-menu>  -->\n\n    <!-- \n\n    <ion-nav #mycontent type="overlay"></ion-nav>  -->\n\n    <!-- <div class="rectangle-2">\n\n    </div> -->\n\n  \n\n  \n\n  \n\n    <div class="bg-img">\n\n      <div style="width: 80%;margin-left: 80px; padding-top: 5px;">\n\n        <p class="title">Employee Self Service Hub</p>\n\n        <img src="assets/imgs/User-Pic.png" style="height:52px; width:52px; float: right; margin-top: -72px; margin-right: 20px;">\n\n      </div>\n\n  \n\n      <div style="padding-right: 15px; padding-top: 15px; padding-left: 91px; width: 100%;">\n\n        <p style="color:#4A134A; font-size: 16px; margin-left: 10px; margin-top: -10px;">Recent visits</p>\n\n        <ion-scroll class="ionscroll" scrollX="true" direction="x">\n\n          <ion-slides pager="true">\n\n            <ion-slide *ngFor="let item of listCardsItems; let i=index;">\n\n              <ion-card class="ioncard">\n\n                <ion-card-content text-center>\n\n                  <ion-card-title style="font-size:14px; font-weight: bold;">\n\n                    {{ item.title }}\n\n                  </ion-card-title>\n\n                  <p style="font-size:12px;">View payslip</p>\n\n                  <button ion-item text-center (click)="downloadPlayslip(i)">\n\n                    <img style="width: 40px;height: 30px;margin-left: 30%;" src="assets/imgs/download_inactive.svg">\n\n                    <p style="font-size: 11px; color: #4A134A;">Download</p>\n\n                  </button>\n\n                </ion-card-content>\n\n              </ion-card>\n\n            </ion-slide>\n\n  \n\n          </ion-slides>\n\n        </ion-scroll>\n\n  \n\n        <p style="color:#4A134A; font-size: 16px; margin-left: 10px;">Worklist</p>\n\n        <ion-card class="section-card" (click)="searchClick()">\n\n          <ion-card-header style="font-size: 13px; padding-bottom: 5px;">\n\n            Pending\n\n          </ion-card-header>\n\n          <hr style="margin-left: 15px;margin-right: 15px; background-color:#808080;">\n\n          <ion-card-content class="card-content" style="padding-top: 5px;">\n\n            <ion-row>\n\n              <ion-col col-3>\n\n                <p style="font-size:11px;">Leave approval</p>\n\n              </ion-col>\n\n              <ion-col col-1 style="padding-top: 0px;">\n\n                <hr style="height: 30px; background-color: #4A134A; width: 1px;">\n\n              </ion-col>\n\n              <ion-col col-4>\n\n                <p style="font-size:11px; font-weight: bolder;">Start date</p>\n\n                <p style="font-size:11px;">XX-XX-XX</p>\n\n              </ion-col>\n\n              <ion-col col-4>\n\n                <p style="font-size:11px; font-weight: bolder;">End date</p>\n\n                <p style="font-size:11px;">XX-XX-XX</p>\n\n              </ion-col>\n\n            </ion-row>\n\n          </ion-card-content>\n\n        </ion-card>\n\n  \n\n        <ion-card class="section-card" (click)="searchClick()">\n\n          <ion-card-header style="font-size: 13px; padding-bottom: 5px;">\n\n            Completed\n\n          </ion-card-header>\n\n          <hr style="margin-left: 15px;margin-right: 15px; background-color:#808080;">\n\n          <ion-card-content class="card-content" style="padding-top: 5px;">\n\n            <ion-row>\n\n              <ion-col col-3>\n\n                <p style="font-size:11px;">HR query</p>\n\n              </ion-col>\n\n              <ion-col col-1 style="padding-top: 0px;">\n\n                <hr style="height: 30px; background-color: #4A134A; width: 1px;">\n\n              </ion-col>\n\n              <ion-col col-4>\n\n                <p style="font-size:11px; font-weight: bolder;">Assigned</p>\n\n                <p style="font-size:11px;">XX-XX-XX</p>\n\n              </ion-col>\n\n              <ion-col col-4>\n\n                <p style="font-size:11px; font-weight: bolder;">Resolved</p>\n\n                <p style="font-size:11px;">XX-XX-XX</p>\n\n              </ion-col>\n\n            </ion-row>\n\n          </ion-card-content>\n\n        </ion-card>\n\n  \n\n      </div>\n\n    </div>\n\n  </ion-content>`/*ion-inline-end:"D:\Projects\TFG_Next_Step\workspace\TFGNextStep\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_13__ionic_native_file__["a" /* File */], __WEBPACK_IMPORTED_MODULE_14__ionic_native_file_opener__["a" /* FileOpener */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_app_version__["a" /* AppVersion */], __WEBPACK_IMPORTED_MODULE_10__providers_network_networkcalls__["a" /* NetworkProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_5__utilities_common__["a" /* Utills */], __WEBPACK_IMPORTED_MODULE_6__utilities_constants__["a" /* Constants */], __WEBPACK_IMPORTED_MODULE_12__ionic_storage__["b" /* Storage */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 321:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PayslipListModels; });
var PayslipListModels = (function () {
    function PayslipListModels() {
    }
    PayslipListModels.fromJSON = function (json) {
        if (json != null && json != undefined) {
            if (json.PayslipListBean != null && json.PayslipListBean != undefined) {
                this.payslipDataList.splice(0, this.payslipDataList.length);
                for (var i = 0; i < json.PayslipListBean.length; i++) {
                    var object = Object.create(PayslipListModels.prototype);
                    Object.assign(object, json.PayslipListBean[i]);
                    var str = void 0;
                    var d = new Date(json.PayslipListBean[i].EffectiveDate);
                    var n = d.getMonth();
                    var y = ("" + json.PayslipListBean[i].EffectiveDate).slice(0, 4);
                    switch (n) {
                        case 0:
                            str = "January";
                            break;
                        case 1:
                            str = "February";
                            break;
                        case 2:
                            str = "March";
                            break;
                        case 3:
                            str = "April";
                            break;
                        case 4:
                            str = "May";
                            break;
                        case 5:
                            str = "June";
                            break;
                        case 6:
                            str = "July";
                            break;
                        case 7:
                            str = "August";
                            break;
                        case 8:
                            str = "September";
                            break;
                        case 9:
                            str = "October";
                            break;
                        case 10:
                            str = "November";
                            break;
                        case 11:
                            str = "December";
                            break;
                    }
                    object.Month = str;
                    object.Year = y;
                    this.payslipDataList.push(object);
                }
            }
        }
        return this.payslipDataList;
    };
    PayslipListModels.getPayslipDataList = function () {
        return this.payslipDataList;
    };
    PayslipListModels.payslipDataList = [];
    return PayslipListModels;
}());

//# sourceMappingURL=PayslipListModel.js.map

/***/ }),

/***/ 366:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Signup; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(30);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the Signup page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var Signup = (function () {
    function Signup(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    Signup.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Signup');
    };
    Signup.prototype.signup = function () {
        //Api connections
    };
    Signup = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-signup',template:/*ion-inline-start:"D:\Projects\TFG_Next_Step\workspace\TFGNextStep\src\pages\signup\signup.html"*/`<ion-content padding no-bounce>\n<ion-list>\n\n  <ion-item>\n    <ion-label fixed>Name</ion-label>\n    <ion-input type="text" value=""></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label fixed>Email</ion-label>\n    <ion-input type="text" value=""></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label fixed>Username</ion-label>\n    <ion-input type="text" value=""></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label fixed>Password</ion-label>\n    <ion-input type="password"></ion-input>\n  </ion-item>\n\n  <button ion-button block color="primary" (click)="signup()">Signup</button>\n\n</ion-list>\n</ion-content>`/*ion-inline-end:"D:\Projects\TFG_Next_Step\workspace\TFGNextStep\src\pages\signup\signup.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */]])
    ], Signup);
    return Signup;
}());

//# sourceMappingURL=signup.js.map

/***/ }),

/***/ 367:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(368);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(372);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 372:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_in_app_browser__ = __webpack_require__(322);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(697);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(227);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_login_login__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_signup_signup__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_home_home__ = __webpack_require__(320);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_chatbot_chatbot__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_network__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_status_bar__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_splash_screen__ = __webpack_require__(365);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_network_networkcalls__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__utilities_constants__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__utilities_common__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_screen_orientation__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_storage__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_fabric__ = __webpack_require__(324);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_device__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_touch_id__ = __webpack_require__(225);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_fingerprint_aio__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_speech_recognition__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_android_permissions__ = __webpack_require__(698);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_text_to_speech__ = __webpack_require__(228);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_payslip_payslip__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_native_app_version__ = __webpack_require__(323);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__ionic_native_file__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__ionic_native_file_opener__ = __webpack_require__(140);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





























var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* Login */],
                __WEBPACK_IMPORTED_MODULE_7__pages_signup_signup__["a" /* Signup */],
                __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_chatbot_chatbot__["a" /* ChatbotPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_payslip_payslip__["a" /* PayslipPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], { swipeBackEnabled: false, scrollPadding: false, scrollAssist: false, autoFocusAssist: false }, {
                    links: [
                        { loadChildren: '../pages/chatbot/chatbot.module#ChatbotPageModule', name: 'ChatbotPage', segment: 'chatbot', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginModule', name: 'Login', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/signup/signup.module#SignupModule', name: 'Signup', segment: 'signup', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/payslip/payslip.module#PayslipPageModule', name: 'PayslipPage', segment: 'payslip', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_17__ionic_storage__["a" /* IonicStorageModule */].forRoot()
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* Login */],
                __WEBPACK_IMPORTED_MODULE_7__pages_signup_signup__["a" /* Signup */],
                __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_chatbot_chatbot__["a" /* ChatbotPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_payslip_payslip__["a" /* PayslipPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_26__ionic_native_app_version__["a" /* AppVersion */],
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_speech_recognition__["a" /* SpeechRecognition */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_android_permissions__["a" /* AndroidPermissions */],
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_text_to_speech__["a" /* TextToSpeech */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_in_app_browser__["a" /* InAppBrowser */],
                __WEBPACK_IMPORTED_MODULE_27__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_28__ionic_native_file_opener__["a" /* FileOpener */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_13__providers_network_networkcalls__["a" /* NetworkProvider */], __WEBPACK_IMPORTED_MODULE_14__utilities_constants__["a" /* Constants */], __WEBPACK_IMPORTED_MODULE_15__utilities_common__["a" /* Utills */], __WEBPACK_IMPORTED_MODULE_10__ionic_native_network__["a" /* Network */], __WEBPACK_IMPORTED_MODULE_16__ionic_native_screen_orientation__["a" /* ScreenOrientation */], __WEBPACK_IMPORTED_MODULE_18__ionic_native_fabric__["a" /* Crashlytics */], __WEBPACK_IMPORTED_MODULE_19__ionic_native_device__["a" /* Device */], __WEBPACK_IMPORTED_MODULE_20__ionic_native_touch_id__["a" /* TouchID */], __WEBPACK_IMPORTED_MODULE_21__ionic_native_fingerprint_aio__["a" /* FingerprintAIO */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 399:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatbotResponseModel; });
var ChatbotResponseModel = (function () {
    function ChatbotResponseModel() {
    }
    ChatbotResponseModel.fromJSON = function (json) {
        //this.objArr.splice(0,this.objArr.length);
        var object = Object.create(ChatbotResponseModel.prototype);
        Object.assign(object, json);
        this.chatbotResponseObj = object;
        return object;
    };
    return ChatbotResponseModel;
}());

//# sourceMappingURL=ChatbotResponseModel.js.map

/***/ }),

/***/ 44:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Utills; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__utilities_constants__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_device__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_DeviceConfiguration__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_screen_orientation__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_touch_id__ = __webpack_require__(225);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_fingerprint_aio__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__models_UserdataModel__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_storage__ = __webpack_require__(47);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var Utills = (function () {
    function Utills(alertCtrl, app, loadingController, constants, platform, device, screenorientation, touchId, toastCtrl, loadingCtrl, 
        //public viewCtrl: ViewController,
        faio, storage) {
        this.alertCtrl = alertCtrl;
        this.app = app;
        this.loadingController = loadingController;
        this.constants = constants;
        this.platform = platform;
        this.device = device;
        this.screenorientation = screenorientation;
        this.touchId = touchId;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.faio = faio;
        this.storage = storage;
        this.displayLogsForPages = [];
        this.isDisplayLog = true;
    }
    Utills.prototype.presentLoading = function () {
        this.loader = this.loadingCtrl.create({ content: "Please wait ...", cssClass: "loadingCssClass" });
        this.loader.present();
    };
    Utills.prototype.closeLoading = function () {
        this.loader.dismiss();
    };
    Utills.prototype.goToPage = function (toPageName, props) {
        try {
            this.app.getActiveNav().push(toPageName, { props: (props !== null || props !== "" || props !== undefined) ? props : null });
        }
        catch (error) {
            error.catchError = true;
            error.pageName = "Utills";
            error.message = "goToPage : " + error.message;
            this.errorDetails(error);
        }
    };
    Utills.prototype.goToBack = function () {
        try {
            this.app.getActiveNav().pop();
        }
        catch (error) {
            error.catchError = true;
            error.pageName = "Utills";
            error.message = "goToBack : " + error.message;
            this.errorDetails(error);
        }
    };
    Utills.prototype.goToRoot = function () {
        try {
            this.app.getActiveNav().popToRoot();
        }
        catch (error) {
            error.catchError = true;
            error.pageName = "Utills";
            error.message = "goToRoot : " + error.message;
            this.errorDetails(error);
        }
    };
    Utills.prototype.showLoader = function (message) {
        if (message === void 0) { message = ""; }
        if (this.loading == null) {
            this.loading = this.loadingController.create({
                content: message,
                enableBackdropDismiss: false
                //dismissOnPageChange: true
            });
            this.loading.present();
        }
        else {
            this.loading.data.content = message;
        }
    };
    Utills.prototype.hideLoader = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            try {
                if (_this.loading != null) {
                    _this.loading.dismiss()
                        .then(function () {
                        resolve();
                    })
                        .catch(function (error) {
                        reject(error);
                    });
                    _this.loading = null;
                }
            }
            catch (e) {
                reject(e);
            }
        });
    };
    Utills.prototype.showAlert = function (message, title) {
        try {
            //alert(message);
            var alert_1 = this.alertCtrl.create({
                title: title,
                subTitle: message,
                buttons: ['OK'],
                enableBackdropDismiss: false
            });
            //alert.present(prompt);
            alert_1.present();
        }
        catch (error) {
            error.catchError = true;
            error.pageName = "Utills";
            error.message = "showAlert : " + error.message;
            this.errorDetails(error);
        }
    };
    Utills.prototype.errorDetails = function (error) {
        try {
            this.hideLoader();
            if (!this.isEmpty(error)) {
                this.log(error.pageName, "Error data : " + JSON.stringify(error));
                if (!this.isEmpty(error.status)) {
                    switch (error.status) {
                        case 408:
                            this.showAlert(this.constants.COMMON_APP_MESSAGE.TIMEOUT_ERROR, "Error");
                            break;
                        case 404:
                            this.showAlert(this.constants.COMMON_APP_MESSAGE.SERVICE_NOT_FOUND, "Error");
                            break;
                        default:
                            this.showAlert(error.statusText, "Error");
                            break;
                    }
                }
                else if (error.catchError == true) {
                    this.log("Utills", error.message);
                    this.showAlert(this.constants.COMMON_APP_MESSAGE.UNKNOWN_ERROR, "Error");
                }
                else if (error.sqlError == true) {
                    this.showAlert(this.constants.COMMON_APP_MESSAGE.UNKNOWN_ERROR, "Error");
                }
                else if (error.serverDataError == true) {
                    this.showAlert(this.constants.COMMON_APP_MESSAGE.SERVER_DATA_ERROR, "Error");
                }
                else if (error.customError == true) {
                    if (error.message == this.constants.COMMON_APP_MESSAGE.NO_NETWORK_MESSAGE)
                        this.showAlert(error.message, this.constants.COMMON_APP_MESSAGE.NO_NETWORK_TITLE);
                    else
                        this.showAlert(error.message, "Error");
                }
                else if (error.name == "TimeoutError") {
                    this.showAlert(this.constants.COMMON_APP_MESSAGE.TIMEOUT_ERROR, "Error");
                }
                else {
                    this.log("Utills", "Error : " + error.message);
                    this.showAlert(this.constants.COMMON_APP_MESSAGE.UNKNOWN_ERROR, "Error");
                }
            }
            else {
                this.showAlert(this.constants.COMMON_APP_MESSAGE.UNKNOWN_ERROR, "Error");
            }
        }
        catch (error) {
            error.message = "errorDetails : " + error.message;
            error.pageName = "Utills";
            error.catchError = true;
            this.showAlert(this.constants.COMMON_APP_MESSAGE.UNKNOWN_ERROR, "Error");
        }
    };
    Utills.prototype.log = function (pageName, message) {
        try {
            if (this.isDisplayLog) {
                var index;
                index = this.displayLogsForPages.indexOf(pageName);
                if (index > -1) {
                    console.log(pageName + " -> " + message);
                }
            }
        }
        catch (error) {
            console.log(pageName + " -> " + message);
        }
    };
    Utills.prototype.isEmpty = function (variable) {
        try {
            if ((variable == null) || (variable == undefined) || (variable == "null") || (variable == "undefined")) {
                return true;
            }
            else if (typeof variable == 'object' && (Object.keys(variable).length === 0 && variable.constructor === Object)) {
                return true;
            }
            else if (variable == '') {
                return true;
            }
            else
                return false;
        }
        catch (error) {
            error.message = "isEmpty : " + error.message;
            error.pageName = "Utills";
            error.catchError = true;
            this.errorDetails(error);
        }
    };
    Utills.prototype.isEmptyObject = function (obj) {
        try {
            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                    return false;
                }
            }
            return JSON.stringify(obj) === JSON.stringify({});
        }
        catch (error) {
            error.message = "isEmptyObject : " + error.message;
            error.pageName = "Utills";
            error.catchError = true;
            this.errorDetails(error);
        }
    };
    Utills.prototype.updateDeviceDetails = function () {
        if (this.platform.is('tablet')) {
            this.devicetype = 'tablet';
        }
        else if (this.platform.is('ipad')) {
            this.devicetype = 'ipad';
        }
        else {
            this.devicetype = 'phone';
        }
        if (this.screenorientation.type.includes("landscape")) {
            this._screenOrientation = "landscape";
        }
        else {
            this._screenOrientation = "portrait";
        }
        return __WEBPACK_IMPORTED_MODULE_4__models_DeviceConfiguration__["a" /* DeviceConfiguration */].updateDeviceConfiguration(this.device.version, this.device.platform, this._screenOrientation, this.devicetype, this.platform.width(), this.platform.height());
        //console.log('Device version is: ' + this.device.version);
        //console.log('Device platform is: ' + this.device.platform);
    };
    Utills.prototype.checkTouchIDAvailability = function () {
        var _this = this;
        /*this.touchId.isAvailable()
            .then(
                res => console.log('TouchID is available!'),
                err => console.error('TouchID is not available', err)
            );*/
        this.touchId.isAvailable()
            .then(function (res) {
            _this.showAlert("TouchID is available!", "Alert");
            __WEBPACK_IMPORTED_MODULE_4__models_DeviceConfiguration__["a" /* DeviceConfiguration */].setTouchIDAvailabilityStatus(true);
        }, function (err) {
            _this.showAlert("TouchID is not available!", "Alert");
            __WEBPACK_IMPORTED_MODULE_4__models_DeviceConfiguration__["a" /* DeviceConfiguration */].setTouchIDAvailabilityStatus(false);
        });
    };
    Utills.prototype.verifyFingerPrint = function () {
        this.touchId.verifyFingerprint('Scan your fingerprint to unlock')
            .then(function (res) {
            //this.viewCtrl.dismiss();
        }, function (err) {
            console.error('Error', err);
        });
    };
    Utills.prototype.checkBiometricAvailable = function () {
        //Check if Fingerprint or Face  is available
        this.faio.isAvailable()
            .then(function (result) {
            if (result === "finger") {
                //Fingerprint Auth is available
                __WEBPACK_IMPORTED_MODULE_4__models_DeviceConfiguration__["a" /* DeviceConfiguration */].setTouchIDAvailabilityStatus(true);
            }
            else {
                __WEBPACK_IMPORTED_MODULE_4__models_DeviceConfiguration__["a" /* DeviceConfiguration */].setTouchIDAvailabilityStatus(false);
            }
            if (result === "face") {
                //Face Auth is available
                __WEBPACK_IMPORTED_MODULE_4__models_DeviceConfiguration__["a" /* DeviceConfiguration */].setFaceIDAvailabilityStatus(true);
            }
            else {
                __WEBPACK_IMPORTED_MODULE_4__models_DeviceConfiguration__["a" /* DeviceConfiguration */].setFaceIDAvailabilityStatus(false);
            }
            /*else {
                //Fingerprint or Face Auth is not available
                this.utility.presentAlert("Fingerprint/Face Auth is not available   on this device!");
            }*/
        }, function (err) {
            //Fingerprint authentication not ready
            __WEBPACK_IMPORTED_MODULE_4__models_DeviceConfiguration__["a" /* DeviceConfiguration */].setTouchIDAvailabilityStatus(false);
            __WEBPACK_IMPORTED_MODULE_4__models_DeviceConfiguration__["a" /* DeviceConfiguration */].setFaceIDAvailabilityStatus(false);
        });
    };
    Utills.prototype.verifyBiometric = function () {
        var status;
        this.faio.show({
            clientId: 'TFGNextStep',
            clientSecret: 'tfgNextStep',
            disableBackup: true,
            localizedFallbackTitle: 'Use Pin',
            localizedReason: 'Please Authenticate' //Only for iOS(optional)
        })
            .then(function (result) {
            console.log("FingerPrint - " + JSON.stringify(result));
            if (result.withFingerprint != null && result.withFingerprint != undefined) {
                //if(result == "OK" || result == "Success"){
                //Fingerprint/Face was successfully verified
                //Go to dashboard
                //this.setAndGet.UserName = this.data.userName;
                //this.navCtrl.push("DashboardPage")
                status = true;
            }
            else {
                //Fingerprint/Face was not successfully verified
                //this.showAlert("Fingerprint/Face was not successfully verified", "ALERT");
                status = false;
            }
        })
            .catch(function (error) {
            //Fingerprint/Face was not successfully verified
            //this.utility.presentAlert(error);
            //this.showAlert(error, "ERROR");
            status = false;
        });
    };
    Utills.prototype.saveUserInfoToLocalStorage = function () {
        var userDataObj = __WEBPACK_IMPORTED_MODULE_8__models_UserdataModel__["a" /* UserdataModel */].getUserInfo();
        this.userInfoData = {
            UserName: userDataObj.UserName,
            Password: userDataObj.Password,
            Authenticated: userDataObj.Authenticated,
            isOracleUser: userDataObj.isOracleUser,
            isAutoLogin: userDataObj.isAutoLogin
        };
        this.storage.set("UserInfoObject", this.userInfoData).then(function (successData) {
            //console.log(successData);
            localStorage.setItem('Authenticated', "1");
            //localStorage.setItem('Username', userDataObj.UserName);
            //localStorage.setItem('Password', userDataObj.Password);
        });
    };
    Utills.prototype.retriveUserInfoFromLocalStorage = function () {
        var object;
        this.storage.get("UserInfoObject").then(function (_userData) {
            //object = UserdataModel.fromJSON(_userData);
            object = _userData;
        });
        return object;
    };
    Utills.prototype.saveDeviceConfigurationToLocalStorage = function () {
        var deviceDataObj = __WEBPACK_IMPORTED_MODULE_4__models_DeviceConfiguration__["a" /* DeviceConfiguration */].getDeviceConfiguration();
        this.deivceData = {
            version: deviceDataObj.version,
            platform: deviceDataObj.platform,
            screenorientation: deviceDataObj.screenorientation,
            devicetype: deviceDataObj.devicetype,
            deviceWidth: deviceDataObj.deviceWidth,
            deviceHeight: deviceDataObj.deviceHeight,
            isTouchIDEnabled: deviceDataObj.isTouchIDEnabled,
            isFaceIDAvailable: deviceDataObj.isFaceIDAvailable
        };
        this.storage.set("DeviceDataObject", this.deivceData).then(function (successData) {
            //console.log(successData);
        });
    };
    Utills.prototype.retriveDeviceConfigurationFromLocalStorage = function () {
        var object;
        this.storage.get("DeviceDataObject").then(function (_deviceData) {
            //object = DeviceConfiguration.fromJSON(_deviceData);
            object = _deviceData;
        });
        return object;
    };
    Utills.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2000
        });
        toast.present();
    };
    Utills.prototype.formatPayslipTimestamp = function (timestamp) {
        var str;
        var d = new Date(timestamp);
        var n = d.getMonth();
        var y = "'" + ("" + timestamp).slice(2, 4);
        switch (n) {
            case 0:
                str = "January" + y;
                break;
            case 1:
                str = "February" + y;
                break;
            case 2:
                str = "March" + y;
                break;
            case 3:
                str = "April" + y;
                break;
            case 4:
                str = "May" + y;
                break;
            case 5:
                str = "June" + y;
                break;
            case 6:
                str = "July" + y;
                break;
            case 7:
                str = "August" + y;
                break;
            case 8:
                str = "September" + y;
                break;
            case 9:
                str = "October" + y;
                break;
            case 10:
                str = "November" + y;
                break;
            case 11:
                str = "December" + y;
                break;
        }
        return str;
    };
    Utills.prototype.formatPayslipTimestampForPayslipFilter = function (timestamp) {
        var str;
        var d = new Date(timestamp);
        var n = d.getMonth();
        var y = "," + ("" + timestamp).slice(0, 4);
        switch (n) {
            case 0:
                str = "January" + y;
                break;
            case 1:
                str = "February" + y;
                break;
            case 2:
                str = "March" + y;
                break;
            case 3:
                str = "April" + y;
                break;
            case 4:
                str = "May" + y;
                break;
            case 5:
                str = "June" + y;
                break;
            case 6:
                str = "July" + y;
                break;
            case 7:
                str = "August" + y;
                break;
            case 8:
                str = "September" + y;
                break;
            case 9:
                str = "October" + y;
                break;
            case 10:
                str = "November" + y;
                break;
            case 11:
                str = "December" + y;
                break;
        }
        return str;
    };
    Utills = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2__utilities_constants__["a" /* Constants */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_device__["a" /* Device */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_screen_orientation__["a" /* ScreenOrientation */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_touch_id__["a" /* TouchID */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_fingerprint_aio__["a" /* FingerprintAIO */], __WEBPACK_IMPORTED_MODULE_9__ionic_storage__["b" /* Storage */]])
    ], Utills);
    return Utills;
}());

//# sourceMappingURL=common.js.map

/***/ }),

/***/ 45:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Constants; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//Constants for getting type references

var Constants = (function () {
    function Constants() {
        /*
       *** Stage and production URL
        */
        this.PORT_NO = ''; //Regular Instance
        this.PRODUCTION_URL = '' + this.PORT_NO + '';
        this.STAGE_URL = '';
        this.DEVBASEURL = this.STAGE_URL;
        this.CHATBOTURL = "https://3.209.127.229:8443/v1/";
        this.LOGINURL = "https://core-apps-dev-api-management.azure-api.net/validate/validate_login/";
        this.TOKEN_URL = "https://3.209.127.229:8443/v1/credential";
        this.GET_PAYSLIP_DATA_URL = "https://core-apps-dev-api-management.azure-api.net/payslipgeneration/getPayslipListDetails/";
        this.GET_PAYSLIP_PDF_URL = "https://core-apps-dev-api-management.azure-api.net/payslipgeneration/getPayslipPDFBlob/";
        /*public ApiUrl={
            LOGINURL:this.DEVBASEURL+'login'
        
        }*/
        this.COMMON_APP_MESSAGE = {
            APP_TITLE: "TFGNextStage",
            TIMEOUT_IN_MILLISECOND: 3000,
            LOADING_LOADER: "Loading",
            LOGIN_ERROR: "Unable to login on server, please contact system administrator",
            UNABLE_TO_CONNECT: "Unable to connect, please try later",
            SERVICE_NOT_FOUND: "Unable to connect, please contact system administrator",
            TIMEOUT_ERROR: "Request Timeout, please try again later",
            UNKNOWN_ERROR: "Something went wrong, please try again later",
            SERVER_ERROR: "Server error occurred, please try later",
            WEBSERVICE_FAILED: "Please check your internet connection or contact system administrator",
            SERVER_DATA_ERROR: "Unable to get valid data from server, please contact system administrator",
            INVALID_AUTHORIZATION_TOKEN: "Authorization failed. Please try again.",
            NO_NETWORK_MESSAGE: "Network unavailable, please check your internet connection",
            NO_NETWORK_TITLE: "No Internet Connection",
            NETWORK_ONLINE: "You are now online",
            NETWORK_OFFLINE: "You are now offline",
            NO_NETWORK_MESSAGE_AND_OFFLINE_WORK: "You are currently offline, for latest data please check your internet connection and re-launch application",
            CONFIRM_EXIT_TITLE: 'Confirm Exit',
            CONFIRM_EXIT_MESSAGE: 'Do you want to exit the App?',
            INVALID_CREDENTIALS: "Invalid Username/Password.",
            INVALID_CREDENTIALS_AFTR_BIOMETRIC: "Invalid Username/Password. Login using username and password.",
            EMPTY_CREDENTIALS: "Please Enter Valid Username and Password.",
            EMPTY_USERNAME: "Username cannot be empty.",
            EMPTY_PASSWORD: "Password cannot be empty.",
            LOGOUT_MESSAGE: 'Are you sure you wish to logout?',
            CONFIRM_TEXT: 'Confirm',
            YES: "Yes",
            NO: "No",
            PROCEED_BTN_TEXT: 'Proceed',
            USERNAME_PLACEHOLDER: 'Username',
            PASSWORD_PLACEHOLDER: 'Password',
            LOGIN_BTN_TEXT: 'Login',
            SECTION_TEXT: 'Sections',
            BACK_TEXT: 'Back',
            SUBMIT_BTN_TEXT: "Submit",
            CANCEL_BTN_TEXT: "Cancel",
            ALERT: 'Alert!',
            ERROR: 'Error'
        };
        this.VALIDATION_ERROR = {
            LOGIN: {
                USERNAME_REQUIRED: 'Username is mandatory',
                PASSWORD_REQUIRED: 'Password is mandatory'
            }
        };
    }
    Constants = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], Constants);
    return Constants;
}());

//# sourceMappingURL=constants.js.map

/***/ }),

/***/ 48:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NetworkProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(227);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__utilities_constants__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_network__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__utilities_common__ = __webpack_require__(44);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
  Generated class for the NetworkProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var NetworkProvider = (function () {
    function NetworkProvider(constants, http, network, utils) {
        this.constants = constants;
        this.http = http;
        this.network = network;
        this.utils = utils;
        //console.log('Hello NetworkProvider Provider');
    }
    NetworkProvider.prototype.postData = function (data, type) {
        var _this = this;
        //console.log('Data : '+JSON.stringify(data));
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/json');
            headers.append('Accept', 'application/json');
            headers.append('X-Requested-With', 'com.zensar.zencmo');
            _this.http.post(_this.constants.DEVBASEURL + type, JSON.stringify(data), { headers: headers }).
                subscribe(function (res) {
                resolve(res.json());
            }, function (err) {
                reject(err);
            });
        });
    };
    NetworkProvider.prototype.getAuthToken = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(_this.constants.TOKEN_URL).
                subscribe(function (res) {
                console.log('Auth Data : ' + JSON.stringify(res));
                resolve(res.json());
            }, function (err) {
                console.log('Error : ' + JSON.stringify(err));
                reject(err);
            });
        });
    };
    NetworkProvider.prototype.authenticateUser = function (data, authToken) {
        var _this = this;
        //console.log('Data : '+JSON.stringify(data));
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/json');
            headers.append('Accept', 'application/json');
            headers.append('Authorization', authToken);
            headers.append('Ocp-Apim-Subscription-Key', 'e60230df791c4dbc95fc4018225af953');
            _this.http.post(_this.constants.LOGINURL, JSON.stringify(data), { headers: headers }).
                subscribe(function (res) {
                resolve(res.json());
            }, function (err) {
                reject(err);
            });
        });
    };
    NetworkProvider.prototype.getPayslipStaticData = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get('assets/data/payslipdata.json').subscribe(function (res) {
                console.log('Auth Data : ' + JSON.stringify(res));
                resolve(res.json());
            }, function (err) {
                console.log('Error : ' + JSON.stringify(err));
                reject(err);
            });
        });
    };
    NetworkProvider.prototype.getPayslipData = function (data, authToken) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/json');
            headers.append('Accept', 'application/json');
            headers.append('Authorization', authToken);
            headers.append('Ocp-Apim-Subscription-Key', 'e60230df791c4dbc95fc4018225af953');
            _this.http.post(_this.constants.GET_PAYSLIP_DATA_URL, JSON.stringify(data), { headers: headers }).
                subscribe(function (res) {
                resolve(res.json());
            }, function (err) {
                reject(err);
            });
        });
    };
    NetworkProvider.prototype.getPayslipBlobData = function (data, authToken) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/json');
            headers.append('Accept', 'application/json');
            headers.append('Authorization', authToken);
            headers.append('Ocp-Apim-Subscription-Key', 'e60230df791c4dbc95fc4018225af953');
            _this.http.post(_this.constants.GET_PAYSLIP_PDF_URL, JSON.stringify(data), { headers: headers }).
                subscribe(function (res) {
                resolve(res.json());
            }, function (err) {
                reject(err);
            });
        });
    };
    NetworkProvider.prototype.postResponse = function (data, type) {
        var _this = this;
        //console.log('Data : '+data);
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/json');
            headers.append('Accept', 'application/json');
            headers.append('X-Requested-With', 'com.zensar.zencmo');
            _this.http.post(_this.constants.DEVBASEURL + type, data, { headers: headers }).
                subscribe(function (res) {
                resolve(res.json());
            }, function (err) {
                reject(err);
            });
        });
    };
    NetworkProvider.prototype.chatQuery = function (serviceUrl, data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/json');
            headers.append('Accept', 'application/json');
            _this.http.post(_this.constants.CHATBOTURL + serviceUrl, data, { headers: headers })
                .subscribe(function (res) {
                if (res.status == 200) {
                    resolve(res.json());
                    // this.responsedata=JSON.stringify(res);
                }
                else if (res.status == 400) {
                    _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.UNKNOWN_ERROR, _this.constants.COMMON_APP_MESSAGE.ERROR);
                }
                else if (res.status == 500) {
                    _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.SERVER_ERROR, _this.constants.COMMON_APP_MESSAGE.ERROR);
                }
                else
                    _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.SERVER_DATA_ERROR, _this.constants.COMMON_APP_MESSAGE.ERROR);
            }, function (err) {
                reject(err);
            });
        });
    };
    NetworkProvider.prototype.isNetworkConnectionAvailable = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            try {
                if (_this.network.type == "none") {
                    resolve(false);
                }
                else {
                    resolve(true);
                }
            }
            catch (e) {
                reject(e);
            }
        });
    };
    NetworkProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__utilities_constants__["a" /* Constants */], __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_network__["a" /* Network */], __WEBPACK_IMPORTED_MODULE_4__utilities_common__["a" /* Utills */]])
    ], NetworkProvider);
    return NetworkProvider;
}());

//# sourceMappingURL=networkcalls.js.map

/***/ }),

/***/ 697:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(365);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_network_networkcalls__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__utilities_common__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_device__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_screen_orientation__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_login_login__ = __webpack_require__(89);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, modalCtrl, utils, device, screenorientation) {
        var _this = this;
        this.platform = platform;
        this.utils = utils;
        this.device = device;
        this.screenorientation = screenorientation;
        /*if((localStorage.getItem('Authenticated') != null)){
          if(localStorage.getItem('Authenticated') === '1'){
            this.rootPage = HomePage;
          } else {
            this.rootPage = Login;
          }
        } else {
          this.rootPage = Login;
        }*/
        this.rootPage = __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* Login */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
            if (_this.platform.is('android')) {
                console.log("running on Android device!");
                console.log("Details- " + _this.device.version + "--" + _this.device.platform + "--" + _this.screenorientation.type + "--" + _this.platform.width() + "--" + _this.platform.height());
            }
            if (_this.platform.is('ios')) {
                console.log("running on iOS device!");
            }
            if (_this.platform.is('tablet')) {
                console.log("running on tablet device!");
            }
            if (_this.platform.is('ipad')) {
                console.log("running on ipad device!");
            }
            var object = utils.updateDeviceDetails();
            _this.utils.checkBiometricAvailable();
            _this.utils.saveDeviceConfigurationToLocalStorage();
            localStorage.setItem("BiometricCancelled", "0");
            /*platform.resume.subscribe(() => {
              this.utils.verifyFingerPrint();
            });*/
            platform.registerBackButtonAction(function () {
                var view = _this.nav.getActive();
                if (view.component.name === 'Login') {
                    platform.exitApp();
                }
                else if (view.component.name === 'HomePage') {
                    platform.exitApp();
                }
                else if (view.component.name === 'ChatbotPage') {
                    _this.nav.pop({});
                }
                else if (view.component.name === 'PayslipPage') {
                    _this.nav.pop({});
                }
                else if (view.component.name === 'Signup') {
                    platform.exitApp();
                }
                else {
                    _this.nav.pop({});
                }
            }, 5);
        });
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"D:\Projects\TFG_Next_Step\workspace\TFGNextStep\src\app\app.html"*/`<ion-nav [root]="rootPage"></ion-nav>\n`/*ion-inline-end:"D:\Projects\TFG_Next_Step\workspace\TFGNextStep\src\app\app.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_4__providers_network_networkcalls__["a" /* NetworkProvider */]],
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */], __WEBPACK_IMPORTED_MODULE_5__utilities_common__["a" /* Utills */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_device__["a" /* Device */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_screen_orientation__["a" /* ScreenOrientation */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 76:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeviceConfiguration; });
var DeviceConfiguration = (function () {
    function DeviceConfiguration() {
        this.isTouchIDEnabled = false;
        this.isFaceIDAvailable = false;
    }
    DeviceConfiguration.updateDeviceConfiguration = function (_version, _platform, _screenorientation, _devicetype, _deviceWidth, _deviceHeight) {
        this.deviceConfiguration.version = _version;
        this.deviceConfiguration.platform = _platform;
        this.deviceConfiguration.screenorientation = _screenorientation;
        this.deviceConfiguration.devicetype = _devicetype;
        this.deviceConfiguration.deviceWidth = _deviceWidth;
        this.deviceConfiguration.deviceHeight = _deviceHeight;
        return this.deviceConfiguration;
    };
    DeviceConfiguration.setTouchIDAvailabilityStatus = function (status) {
        this.deviceConfiguration.isTouchIDEnabled = status;
        if (status)
            localStorage.setItem("TouchID", "true");
        else
            localStorage.setItem("TouchID", "false");
    };
    DeviceConfiguration.setFaceIDAvailabilityStatus = function (status) {
        this.deviceConfiguration.isFaceIDAvailable = status;
        if (status)
            localStorage.setItem("FaceID", "true");
        else
            localStorage.setItem("FaceID", "false");
    };
    DeviceConfiguration.getTouchIDAvailabilityStatus = function () {
        //return this.deviceConfiguration.isTouchIDEnabled;
        return localStorage.getItem("TouchID");
    };
    DeviceConfiguration.getFaceIDAvailabilityStatus = function () {
        //return this.deviceConfiguration.isFaceIDAvailable;
        return localStorage.getItem("FaceID");
    };
    DeviceConfiguration.getDeviceConfiguration = function () {
        return this.deviceConfiguration;
    };
    DeviceConfiguration.fromJSON = function (json) {
        var object = Object.create(DeviceConfiguration.prototype);
        Object.assign(object, json);
        return object;
    };
    DeviceConfiguration.deviceConfiguration = new DeviceConfiguration();
    return DeviceConfiguration;
}());

//# sourceMappingURL=DeviceConfiguration.js.map

/***/ }),

/***/ 89:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Login; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(320);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_network__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_fabric__ = __webpack_require__(324);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__utilities_common__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__models_DeviceConfiguration__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__utilities_constants__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__models_UserdataModel__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_fingerprint_aio__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_network_networkcalls__ = __webpack_require__(48);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














/**
 * Generated class for the Login page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var Login = (function () {
    function Login(faio, networkService, constants, platform, crashlytics, toast, network, storage, utils, navCtrl, navParams, fb) {
        var _this = this;
        this.faio = faio;
        this.networkService = networkService;
        this.constants = constants;
        this.crashlytics = crashlytics;
        this.toast = toast;
        this.network = network;
        this.storage = storage;
        this.utils = utils;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fb = fb;
        this._authToken = "";
        this.userData = { "userName": "", "password": "" };
        this.faceIDEnabled = false;
        this.touchIDEnabled = false;
        this._loginCaptchaStatus = false;
        this.captcha = "";
        this._captcha = "";
        this.passwordType = 'password';
        this.passwordIcon = 'eye-off';
        this.dataToStore = {
            userName: 'Guestuser',
            password: 'Temp@1234'
        };
        this.authForm = fb.group({
            'username': [null, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].minLength(5), __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].maxLength(255)])],
            'password': [null, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].minLength(8)])]
        });
        try {
            throw new Error("this is javascriptError");
        }
        catch (e) {
            this.crashlytics.addLog("Error while loading data");
            this.crashlytics.sendNonFatalCrash(e.message || e);
        }
        if (__WEBPACK_IMPORTED_MODULE_8__models_DeviceConfiguration__["a" /* DeviceConfiguration */].getFaceIDAvailabilityStatus() === "true")
            this.faceIDEnabled = true;
        else
            this.faceIDEnabled = false;
        if (__WEBPACK_IMPORTED_MODULE_8__models_DeviceConfiguration__["a" /* DeviceConfiguration */].getTouchIDAvailabilityStatus() === "true")
            this.touchIDEnabled = true;
        else
            this.touchIDEnabled = false;
        if (this.faceIDEnabled)
            this.nonclickableFaceID = true;
        else
            this.nonclickableFaceID = false;
        if (this.touchIDEnabled)
            this.nonclickableTouchID = true;
        else
            this.nonclickableTouchID = false;
        this.storage.get("UserInfoObject").then(function (_userData) {
            if (_userData != null) {
                //object = UserdataModel.fromJSON(_userData);
                _this._username = _userData.UserName;
                _this._password = _userData.Password;
                if (_this._username == null || _this._username == undefined) {
                    _this.touchIDEnabled = false;
                    _this.faceIDEnabled = false;
                    _this.nonclickableFaceID = false;
                    _this.nonclickableTouchID = false;
                }
            }
        });
    }
    Login.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Login');
    };
    Login.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.storage.get("UserInfoObject").then(function (_userData) {
            if (_userData != null) {
                //object = UserdataModel.fromJSON(_userData);
                _this._username = _userData.UserName;
                _this._password = _userData.Password;
            }
        });
        this.connected = this.network.onConnect().subscribe(function (data) {
            //console.log(data)
            _this.displayNetworkUpdate(data.type);
        }, function (error) { return console.error(error); });
        this.disconnected = this.network.onDisconnect().subscribe(function (data) {
            //console.log(data)
            _this.displayNetworkUpdate(data.type);
        }, function (error) { return console.error(error); });
    };
    Login.prototype.ionViewWillEnter = function () {
        var _this = this;
        if ((localStorage.getItem('Authenticated') != null)) {
            if (localStorage.getItem('Authenticated') === '1') {
                if ((localStorage.getItem('isAutoLogin') != null)) {
                    if (localStorage.getItem('isAutoLogin') === "true") {
                        if (__WEBPACK_IMPORTED_MODULE_8__models_DeviceConfiguration__["a" /* DeviceConfiguration */].getTouchIDAvailabilityStatus() === "true") {
                            this.nonclickableTouchID = true;
                            setTimeout(function () {
                                _this.callBiometric('2');
                            }, 500);
                        }
                        else if (__WEBPACK_IMPORTED_MODULE_8__models_DeviceConfiguration__["a" /* DeviceConfiguration */].getFaceIDAvailabilityStatus() === "true") {
                            this.nonclickableFaceID = true;
                            setTimeout(function () {
                                _this.callBiometric('1');
                            }, 500);
                        }
                        else {
                            /*let currentIndex = this.navCtrl.getActive().index;
                                    this.navCtrl.push(HomePage).then(() => {
                                      this.navCtrl.remove(currentIndex);
                                });*/
                            localStorage.removeItem('Authenticated');
                            __WEBPACK_IMPORTED_MODULE_10__models_UserdataModel__["a" /* UserdataModel */].setAutoLogin(false);
                            localStorage.setItem("isAutoLogin", "false");
                        }
                    }
                }
            }
        }
    };
    Login.prototype.ionViewWillLeave = function () {
        this.connected.unsubscribe();
        this.disconnected.unsubscribe();
        this._loginCaptchaStatus = false;
    };
    Login.prototype.generateCaptcha = function (length) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < length; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    };
    Login.prototype.login = function () {
        var _this = this;
        if (this._loginCaptchaStatus) {
            if (this.captcha.length > 0) {
                if (this.captcha === this._captcha) {
                    this._loginCaptchaStatus = false;
                }
                else {
                    this.utils.showAlert("Invalid Captcha. Try again with new captcha", "ERROR");
                    //this.utils.presentToast("Invalid Captcha");
                    this._loginCaptchaStatus = true;
                    this._captcha = this.generateCaptcha(8);
                    this.captcha = "";
                }
            }
            else {
                this.utils.presentToast("Captcha is empty");
            }
        }
        if (this._loginCaptchaStatus)
            return;
        else if (this.utils.isEmpty(this.userData.userName)) {
            this._loginCaptchaStatus = true;
            this.utils.presentToast(this.constants.COMMON_APP_MESSAGE.EMPTY_USERNAME);
        }
        else if (this.utils.isEmpty(this.userData.password)) {
            this._loginCaptchaStatus = true;
            this.utils.presentToast(this.constants.COMMON_APP_MESSAGE.EMPTY_PASSWORD);
        }
        else if (this.userData.userName && this.userData.password) {
            this.options = {
                "RESTHeader": {
                    "xmlns": "http://xmlns.oracle.com/apps/fnd/rest/header",
                    "Responsibility": "GLOBAL_HRMS_MANAGER",
                    "RespApplication": "PER",
                    "SecurityGroup": "STANDARD",
                    "NLSLanguage": "AMERICAN",
                    "Org_Id": 81
                },
                "InputParameters": {
                    "P_USER": this.userData.userName,
                    "P_PWD": this.userData.password
                }
            };
            this.networkService.isNetworkConnectionAvailable()
                .then(function (isOnline) {
                if (isOnline) {
                    /*  Guest user start */
                    if (_this.userData.userName === _this.dataToStore.userName || (_this.userData.password === _this.dataToStore.password)) {
                        if ((_this.userData.userName === _this.dataToStore.userName) && (_this.userData.password === _this.dataToStore.password)) {
                            __WEBPACK_IMPORTED_MODULE_10__models_UserdataModel__["a" /* UserdataModel */].updateUserInfo(true, _this.userData.userName, _this.userData.password, false);
                            _this.utils.saveUserInfoToLocalStorage();
                            if (__WEBPACK_IMPORTED_MODULE_8__models_DeviceConfiguration__["a" /* DeviceConfiguration */].getFaceIDAvailabilityStatus() === "true")
                                _this.faceIDEnabled = true;
                            else
                                _this.faceIDEnabled = false;
                            if (__WEBPACK_IMPORTED_MODULE_8__models_DeviceConfiguration__["a" /* DeviceConfiguration */].getTouchIDAvailabilityStatus() === "true")
                                _this.touchIDEnabled = true;
                            else
                                _this.touchIDEnabled = false;
                            console.log("TouchID status - " + _this.touchIDEnabled);
                            //Api connections
                            if ((_this.touchIDEnabled || _this.faceIDEnabled) && localStorage.getItem("BiometricCancelled") === "0") {
                                _this.faio.show({
                                    clientId: 'TFGNextStep',
                                    clientSecret: 'tfgNextStep',
                                    disableBackup: false,
                                    localizedFallbackTitle: 'Use Pin',
                                    localizedReason: 'Please Authenticate' //Only for iOS(optional)
                                })
                                    .then(function (result) {
                                    console.log("FingerPrint - " + JSON.stringify(result));
                                    if ((result === "Success") || (result.withFingerprint != null && result.withFingerprint != undefined) || result.withPassword || result.withBackup) {
                                        var currentIndex_1 = _this.navCtrl.getActive().index;
                                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]).then(function () {
                                            _this.navCtrl.remove(currentIndex_1);
                                        });
                                        if (__WEBPACK_IMPORTED_MODULE_8__models_DeviceConfiguration__["a" /* DeviceConfiguration */].getTouchIDAvailabilityStatus() === "true")
                                            _this.touchIDEnabled = true;
                                        else
                                            _this.touchIDEnabled = false;
                                        if (_this.touchIDEnabled)
                                            _this.nonclickableTouchID = true;
                                        else
                                            _this.nonclickableTouchID = false;
                                        if (__WEBPACK_IMPORTED_MODULE_8__models_DeviceConfiguration__["a" /* DeviceConfiguration */].getFaceIDAvailabilityStatus() === "true")
                                            _this.faceIDEnabled = true;
                                        else
                                            _this.faceIDEnabled = false;
                                        if (_this.faceIDEnabled)
                                            _this.nonclickableFaceID = true;
                                        else
                                            _this.nonclickableFaceID = false;
                                    }
                                    else {
                                        //Fingerprint/Face was not successfully verified
                                        //this.showAlert("Fingerprint/Face was not successfully verified", "ALERT");
                                        _this.utils.showAlert("Verification failed", _this.constants.COMMON_APP_MESSAGE.ALERT);
                                        localStorage.removeItem('Authenticated');
                                    }
                                })
                                    .catch(function (error) {
                                    //Fingerprint/Face was not successfully verified
                                    //this.utility.presentAlert(error);
                                    //this.showAlert(error, "ERROR");
                                    if (error === "Cancelled") {
                                        localStorage.setItem("BiometricCancelled", "1");
                                        var currentIndex_2 = _this.navCtrl.getActive().index;
                                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]).then(function () {
                                            _this.navCtrl.remove(currentIndex_2);
                                        });
                                    }
                                    else {
                                        _this.utils.showAlert("Verification failed", _this.constants.COMMON_APP_MESSAGE.ALERT);
                                        localStorage.removeItem('Authenticated');
                                    }
                                });
                            }
                            else {
                                var currentIndex_3 = _this.navCtrl.getActive().index;
                                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]).then(function () {
                                    _this.navCtrl.remove(currentIndex_3);
                                });
                            }
                        }
                        else {
                            _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.INVALID_CREDENTIALS, _this.constants.COMMON_APP_MESSAGE.ALERT);
                        }
                    }
                    else {
                        /*  Guest user stop */
                        _this.utils.presentLoading();
                        _this.networkService.getAuthToken().then(function (tokenData) {
                            var _tokenData = JSON.parse(JSON.stringify(tokenData));
                            _this._authToken = "Basic " + _tokenData.networkCredentials.authorization;
                            if (_this._authToken.length > 0) {
                                _this.networkService.authenticateUser(_this.options, _this._authToken).then(function (userInfo) {
                                    var _data = JSON.parse(JSON.stringify(userInfo));
                                    if (_data.OutputParameters.VALIDATE_LOGIN === "Y") {
                                        _this.utils.closeLoading();
                                        __WEBPACK_IMPORTED_MODULE_10__models_UserdataModel__["a" /* UserdataModel */].updateUserInfo(true, _this.userData.userName, _this.userData.password, false);
                                        _this.utils.saveUserInfoToLocalStorage();
                                        if (__WEBPACK_IMPORTED_MODULE_8__models_DeviceConfiguration__["a" /* DeviceConfiguration */].getFaceIDAvailabilityStatus() === "true")
                                            _this.faceIDEnabled = true;
                                        else
                                            _this.faceIDEnabled = false;
                                        if (__WEBPACK_IMPORTED_MODULE_8__models_DeviceConfiguration__["a" /* DeviceConfiguration */].getTouchIDAvailabilityStatus() === "true")
                                            _this.touchIDEnabled = true;
                                        else
                                            _this.touchIDEnabled = false;
                                        console.log("TouchID status - " + _this.touchIDEnabled);
                                        //Api connections
                                        if ((_this.touchIDEnabled || _this.faceIDEnabled) && localStorage.getItem("BiometricCancelled") === "0") {
                                            _this.faio.show({
                                                clientId: 'TFGNextStep',
                                                clientSecret: 'tfgNextStep',
                                                disableBackup: false,
                                                localizedFallbackTitle: 'Use Pin',
                                                localizedReason: 'Please Authenticate' //Only for iOS(optional)
                                            })
                                                .then(function (result) {
                                                console.log("FingerPrint - " + JSON.stringify(result));
                                                _this.__status = false;
                                                if (result === "Success")
                                                    _this.__status = true;
                                                else if (result.withFingerprint != null && result.withFingerprint != undefined)
                                                    _this.__status = true;
                                                else if (result.withPassword)
                                                    _this.__status = true;
                                                else if (result.withBackup)
                                                    _this.__status = true;
                                                else
                                                    _this.__status = false;
                                                if (_this.__status) {
                                                    var currentIndex_4 = _this.navCtrl.getActive().index;
                                                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */], { AuthToken: _this._authToken }).then(function () {
                                                        _this.navCtrl.remove(currentIndex_4);
                                                    });
                                                    if (__WEBPACK_IMPORTED_MODULE_8__models_DeviceConfiguration__["a" /* DeviceConfiguration */].getTouchIDAvailabilityStatus() === "true")
                                                        _this.touchIDEnabled = true;
                                                    else
                                                        _this.touchIDEnabled = false;
                                                    if (_this.touchIDEnabled)
                                                        _this.nonclickableTouchID = true;
                                                    else
                                                        _this.nonclickableTouchID = false;
                                                    if (__WEBPACK_IMPORTED_MODULE_8__models_DeviceConfiguration__["a" /* DeviceConfiguration */].getFaceIDAvailabilityStatus() === "true")
                                                        _this.faceIDEnabled = true;
                                                    else
                                                        _this.faceIDEnabled = false;
                                                    if (_this.faceIDEnabled)
                                                        _this.nonclickableFaceID = true;
                                                    else
                                                        _this.nonclickableFaceID = false;
                                                }
                                                else {
                                                    //Fingerprint/Face was not successfully verified
                                                    //this.showAlert("Fingerprint/Face was not successfully verified", "ALERT");
                                                    _this.utils.showAlert("Verification failed", _this.constants.COMMON_APP_MESSAGE.ALERT);
                                                    localStorage.removeItem('Authenticated');
                                                }
                                            })
                                                .catch(function (error) {
                                                //Fingerprint/Face was not successfully verified
                                                //this.utility.presentAlert(error);
                                                //this.showAlert(error, "ERROR");
                                                if (error === "Cancelled") {
                                                    localStorage.setItem("BiometricCancelled", "1");
                                                    var currentIndex_5 = _this.navCtrl.getActive().index;
                                                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */], { AuthToken: _this._authToken }).then(function () {
                                                        _this.navCtrl.remove(currentIndex_5);
                                                    });
                                                }
                                                else {
                                                    _this.utils.showAlert("Verification failed", _this.constants.COMMON_APP_MESSAGE.ALERT);
                                                    localStorage.removeItem('Authenticated');
                                                }
                                            });
                                        }
                                        else {
                                            var currentIndex_6 = _this.navCtrl.getActive().index;
                                            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */], { AuthToken: _this._authToken }).then(function () {
                                                _this.navCtrl.remove(currentIndex_6);
                                            });
                                        }
                                    }
                                    else {
                                        _this.utils.closeLoading();
                                        _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.INVALID_CREDENTIALS, _this.constants.COMMON_APP_MESSAGE.ALERT);
                                        _this._loginCaptchaStatus = true;
                                        _this._captcha = _this.generateCaptcha(8);
                                        _this.userData.userName = "";
                                        _this.userData.password = "";
                                        _this.captcha = "";
                                    }
                                }, function (err) {
                                    console.log(err);
                                    _this.utils.closeLoading();
                                    _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.SERVER_DATA_ERROR, _this.constants.COMMON_APP_MESSAGE.ERROR);
                                });
                            }
                            else {
                                _this._authToken = "";
                                _this.utils.closeLoading();
                                _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.INVALID_AUTHORIZATION_TOKEN, _this.constants.COMMON_APP_MESSAGE.ERROR);
                            }
                        }, function (err) {
                            _this._authToken = "";
                            console.log(err);
                            _this.utils.closeLoading();
                            _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.INVALID_AUTHORIZATION_TOKEN, _this.constants.COMMON_APP_MESSAGE.ERROR);
                        });
                    }
                }
                else {
                    _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.NO_NETWORK_TITLE, _this.constants.COMMON_APP_MESSAGE.NO_NETWORK_MESSAGE);
                }
            });
        }
        else {
            this.utils.presentToast(this.constants.COMMON_APP_MESSAGE.EMPTY_CREDENTIALS);
        }
    };
    Login.prototype.displayNetworkUpdate = function (connectionState) {
        var networkType = this.network.type;
        this.toast.create({
            message: "You are now " + connectionState + " via " + networkType,
            duration: 3000
        }).present();
    };
    Login.prototype.hideShowPassword = function () {
        this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
        this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    };
    Login.prototype.callBiometric = function (val) {
        var _this = this;
        if (val === '2' && this.nonclickableTouchID) {
            //if(this.nonclickableTouchID || this.nonclickableFaceID) {
            this.faio.show({
                clientId: 'TFGNextStep',
                clientSecret: 'tfgNextStep',
                disableBackup: false,
                localizedFallbackTitle: 'Use Pin',
                localizedReason: 'Please Authenticate' //Only for iOS(optional)
            })
                .then(function (result) {
                console.log("CallBiometric FingerPrint - " + JSON.stringify(result));
                _this.__status = false;
                if (result === "Success")
                    _this.__status = true;
                else if (result.withFingerprint != null && result.withFingerprint != undefined)
                    _this.__status = true;
                else if (result.withPassword)
                    _this.__status = true;
                else if (result.withBackup)
                    _this.__status = true;
                else
                    _this.__status = false;
                if (_this.__status) {
                    /*  Guest user start */
                    if (_this._username === _this.dataToStore.userName) {
                        if ((_this._username === _this.dataToStore.userName) && (_this._password === _this.dataToStore.password)) {
                            __WEBPACK_IMPORTED_MODULE_10__models_UserdataModel__["a" /* UserdataModel */].updateUserInfo(true, _this._username, _this._password, false);
                            _this.utils.saveUserInfoToLocalStorage();
                            _this.callHomePage();
                        }
                        else {
                            _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.INVALID_CREDENTIALS_AFTR_BIOMETRIC, _this.constants.COMMON_APP_MESSAGE.ALERT);
                        }
                    }
                    else {
                        /*  Guest user stop */
                        _this.storage.get("UserInfoObject").then(function (_userData) {
                            if (_userData != null) {
                                _this._username = _userData.UserName;
                                _this._password = _userData.Password;
                            }
                        });
                        _this.utils.presentLoading();
                        _this.options = {
                            "RESTHeader": {
                                "xmlns": "http://xmlns.oracle.com/apps/fnd/rest/header",
                                "Responsibility": "GLOBAL_HRMS_MANAGER",
                                "RespApplication": "PER",
                                "SecurityGroup": "STANDARD",
                                "NLSLanguage": "AMERICAN",
                                "Org_Id": 81
                            },
                            "InputParameters": {
                                "P_USER": _this._username,
                                "P_PWD": _this._password
                            }
                        };
                        _this.networkService.getAuthToken().then(function (tokenData) {
                            var _tokenData = JSON.parse(JSON.stringify(tokenData));
                            _this._authToken = "Basic " + _tokenData.networkCredentials.authorization;
                            if (_this._authToken.length > 0) {
                                _this.networkService.authenticateUser(_this.options, _this._authToken).then(function (userInfo) {
                                    var _data = JSON.parse(JSON.stringify(userInfo));
                                    if (_data.OutputParameters.VALIDATE_LOGIN === "Y") {
                                        _this.utils.closeLoading();
                                        __WEBPACK_IMPORTED_MODULE_10__models_UserdataModel__["a" /* UserdataModel */].updateUserInfo(true, _this._username, _this._password, false);
                                        _this.utils.saveUserInfoToLocalStorage();
                                        _this.callHomePage();
                                    }
                                    else {
                                        _this.utils.closeLoading();
                                        _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.INVALID_CREDENTIALS_AFTR_BIOMETRIC, _this.constants.COMMON_APP_MESSAGE.ALERT);
                                    }
                                }, function (err) {
                                    console.log(err);
                                    _this.utils.closeLoading();
                                    _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.SERVER_DATA_ERROR, _this.constants.COMMON_APP_MESSAGE.ERROR);
                                });
                            }
                            else {
                                _this._authToken = "";
                                _this.utils.closeLoading();
                                _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.INVALID_AUTHORIZATION_TOKEN, _this.constants.COMMON_APP_MESSAGE.ERROR);
                            }
                        }, function (err) {
                            _this._authToken = "";
                            console.log(err);
                            _this.utils.closeLoading();
                            _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.INVALID_AUTHORIZATION_TOKEN, _this.constants.COMMON_APP_MESSAGE.ERROR);
                        });
                    }
                }
                else {
                    //Fingerprint/Face was not successfully verified
                    //this.showAlert("Fingerprint/Face was not successfully verified", "ALERT");
                    _this.utils.showAlert("Verification failed", _this.constants.COMMON_APP_MESSAGE.ALERT);
                    localStorage.removeItem('Authenticated');
                }
            })
                .catch(function (error) {
                //Fingerprint/Face was not successfully verified
                //this.utility.presentAlert(error);
                if (error === "Cancelled") {
                    localStorage.setItem("BiometricCancelled", "1");
                    _this.utils.showAlert("Cancelled by user", _this.constants.COMMON_APP_MESSAGE.ALERT);
                }
                else
                    _this.utils.showAlert("Verification failed", _this.constants.COMMON_APP_MESSAGE.ALERT);
                localStorage.removeItem('Authenticated');
            });
        }
        else if (val === '1' && this.nonclickableFaceID) {
            //if(this.nonclickableTouchID || this.nonclickableFaceID) {
            this.faio.show({
                clientId: 'TFGNextStep',
                clientSecret: 'tfgNextStep',
                disableBackup: false,
                localizedFallbackTitle: 'Use Pin',
                localizedReason: 'Please Authenticate' //Only for iOS(optional)
            })
                .then(function (result) {
                console.log("CallBiometric FingerPrint - " + JSON.stringify(result));
                _this.__status = false;
                if (result === "Success")
                    _this.__status = true;
                else if (result.withFingerprint != null && result.withFingerprint != undefined)
                    _this.__status = true;
                else if (result.withPassword)
                    _this.__status = true;
                else if (result.withBackup)
                    _this.__status = true;
                else
                    _this.__status = false;
                if (_this.__status) {
                    /*  Guest user start */
                    if (_this._username === _this.dataToStore.userName) {
                        if ((_this._username === _this.dataToStore.userName) && (_this._password === _this.dataToStore.password)) {
                            __WEBPACK_IMPORTED_MODULE_10__models_UserdataModel__["a" /* UserdataModel */].updateUserInfo(true, _this._username, _this._password, false);
                            _this.utils.saveUserInfoToLocalStorage();
                            _this.callHomePage();
                        }
                        else {
                            _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.INVALID_CREDENTIALS_AFTR_BIOMETRIC, _this.constants.COMMON_APP_MESSAGE.ALERT);
                        }
                    }
                    else {
                        /*  Guest user stop */
                        _this.storage.get("UserInfoObject").then(function (_userData) {
                            if (_userData != null) {
                                _this._username = _userData.UserName;
                                _this._password = _userData.Password;
                            }
                        });
                        _this.utils.presentLoading();
                        _this.options = {
                            "RESTHeader": {
                                "xmlns": "http://xmlns.oracle.com/apps/fnd/rest/header",
                                "Responsibility": "GLOBAL_HRMS_MANAGER",
                                "RespApplication": "PER",
                                "SecurityGroup": "STANDARD",
                                "NLSLanguage": "AMERICAN",
                                "Org_Id": 81
                            },
                            "InputParameters": {
                                "P_USER": _this._username,
                                "P_PWD": _this._password
                            }
                        };
                        _this.networkService.getAuthToken().then(function (tokenData) {
                            var _tokenData = JSON.parse(JSON.stringify(tokenData));
                            _this._authToken = "Basic " + _tokenData.networkCredentials.authorization;
                            if (_this._authToken.length > 0) {
                                _this.networkService.authenticateUser(_this.options, _this._authToken).then(function (userInfo) {
                                    var _data = JSON.parse(JSON.stringify(userInfo));
                                    if (_data.OutputParameters.VALIDATE_LOGIN === "Y") {
                                        _this.utils.closeLoading();
                                        __WEBPACK_IMPORTED_MODULE_10__models_UserdataModel__["a" /* UserdataModel */].updateUserInfo(true, _this._username, _this._password, false);
                                        _this.utils.saveUserInfoToLocalStorage();
                                        _this.callHomePage();
                                    }
                                    else {
                                        _this.utils.closeLoading();
                                        _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.INVALID_CREDENTIALS_AFTR_BIOMETRIC, _this.constants.COMMON_APP_MESSAGE.ALERT);
                                    }
                                }, function (err) {
                                    console.log(err);
                                    _this.utils.closeLoading();
                                    _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.SERVER_DATA_ERROR, _this.constants.COMMON_APP_MESSAGE.ERROR);
                                });
                            }
                            else {
                                _this._authToken = "";
                                _this.utils.closeLoading();
                                _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.INVALID_AUTHORIZATION_TOKEN, _this.constants.COMMON_APP_MESSAGE.ERROR);
                            }
                        }, function (err) {
                            _this._authToken = "";
                            console.log(err);
                            _this.utils.closeLoading();
                            _this.utils.showAlert(_this.constants.COMMON_APP_MESSAGE.INVALID_AUTHORIZATION_TOKEN, _this.constants.COMMON_APP_MESSAGE.ERROR);
                        });
                    }
                }
                else {
                    //Fingerprint/Face was not successfully verified
                    //this.showAlert("Fingerprint/Face was not successfully verified", "ALERT");
                    _this.utils.showAlert("Verification failed", _this.constants.COMMON_APP_MESSAGE.ALERT);
                    localStorage.removeItem('Authenticated');
                }
            })
                .catch(function (error) {
                //Fingerprint/Face was not successfully verified
                //this.utility.presentAlert(error);
                if (error === "Cancelled") {
                    localStorage.setItem("BiometricCancelled", "1");
                    _this.utils.showAlert("Cancelled by user", _this.constants.COMMON_APP_MESSAGE.ALERT);
                }
                else
                    _this.utils.showAlert("Verification failed", _this.constants.COMMON_APP_MESSAGE.ALERT);
                localStorage.removeItem('Authenticated');
            });
        }
    };
    Login.prototype.callHomePage = function () {
        var _this = this;
        var currentIndex = this.navCtrl.getActive().index;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */], { AuthToken: this._authToken }).then(function () {
            _this.navCtrl.remove(currentIndex);
        });
    };
    Login.prototype.workinProgress = function () {
        this.utils.showAlert("Work in progress", this.constants.COMMON_APP_MESSAGE.ALERT);
    };
    Login = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"D:\Projects\TFG_Next_Step\workspace\TFGNextStep\src\pages\login\login.html"*/`\n\n\n\n<ion-content class="bg-img" no-bounce>\n\n\n\n\n\n  <div class="rectangle-1">\n\n\n\n    <div class="tfg-logo">\n\n\n\n      <img src="assets/imgs/TFG_Logo.png">\n\n      \n\n\n\n    </div>\n\n\n\n  </div>\n\n  <div class="rectangle-2">\n\n    <p>Employee Self Service Hub</p>\n\n      <!-- <img src="assets/imgs/Employee-Self-Servic.png"> -->\n\n  </div>\n\n<div class="card-align">\n\n  <ion-card class="login-card">\n\n    <ion-card-header style="margin-bottom: -15px;">\n\n      <p class="login-style" text-center>LOGIN</p>\n\n    </ion-card-header>\n\n\n\n    <ion-card-content>\n\n      <form>\n\n        <ion-list>\n\n          <ion-item>\n\n              <ion-label style="color:#828282" floating>Username</ion-label>\n\n            <ion-input type="Username" name="Username" [(ngModel)]="userData.userName" [formControl]="authForm.controls[\'username\']">\n\n            </ion-input>\n\n          </ion-item>\n\n          <div class="error" *ngIf="authForm.controls[\'username\'].hasError(\'required\') && authForm.controls[\'username\'].touched">* Username is required!</div>\n\n          <div class="error" *ngIf="authForm.controls[\'username\'].hasError(\'minlength\') && authForm.controls[\'username\'].touched">* Minimum username length is 6!</div>\n\n          <div class="error" *ngIf="authForm.controls[\'username\'].hasError(\'maxlength\') && authForm.controls[\'username\'].touched">* Maximum username length is 255!</div>\n\n          <ion-item>\n\n              <ion-label style="color:#828282" floating>Password</ion-label>\n\n              <ion-input [type]="passwordType" clearOnEdit="false" name="password" [(ngModel)]="userData.password" [formControl]="authForm.controls[\'password\']"> </ion-input>\n\n              <ion-icon *ngIf="userData.password" item-end [name]="passwordIcon" class="passwordIcon" (click)=\'hideShowPassword()\'></ion-icon>\n\n            </ion-item>\n\n          <div class="error" *ngIf="authForm.controls[\'password\'].hasError(\'required\') && authForm.controls[\'password\'].touched">* Password is required!</div>\n\n                      <div class="error" *ngIf="authForm.controls[\'password\'].hasError(\'minlength\') && authForm.controls[\'password\'].touched">* Minimum password length is 8!</div>\n\n          <ion-item *ngIf="_loginCaptchaStatus">\n\n              <ion-label style="color:#828282" floating>Captcha : <b>{{_captcha}}</b></ion-label>\n\n              <ion-input [type]="Captcha" clearOnEdit="false" name="captcha" [(ngModel)]="captcha"> </ion-input>\n\n          </ion-item>\n\n            <ion-row>\n\n            <ion-col col-8 (click)="workinProgress()">\n\n            <a href="#">Forgot Password?</a> \n\n          </ion-col>\n\n          <ion-col col-4 end (click)="workinProgress()">\n\n            <a href="#" class="signup-align">Signup</a>\n\n          </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n          <div class="login-btn">\n\n            <button class="login-btn" ion-button color="primary" block (click)="login()">Login</button>\n\n          </div>\n\n</ion-row>\n\n        </ion-list>\n\n      </form>\n\n\n\n\n\n      <ion-row class="security-row" style="margin-top: 35px;" >\n\n        <ion-col class="password-sec password-sec-bg">\n\n          <div class="ovel" style="padding-top: 14px;">     \n\n            <img src="assets/imgs/password.svg">\n\n            <p>Password</p>\n\n          \n\n          </div>\n\n\n\n        </ion-col>\n\n        <ion-col (click)="callBiometric(\'1\')" class="password-sec" [ngClass]="{ \'password-sec-bg\': faceIDEnabled ,\'password-sec-bg-transparent\': !faceIDEnabled  }">\n\n          <div [ngClass]="{ \'ovel\': faceIDEnabled ,\'ovel-opacity\': !faceIDEnabled  }" >  \n\n              <img src="assets/imgs/user.svg">\n\n              <p>Face</p>\n\n          </div>\n\n        </ion-col>\n\n        <ion-col (click)="callBiometric(\'2\')" class="password-sec" [ngClass]="{ \'password-sec-bg\': touchIDEnabled ,\'password-sec-bg-transparent\': !touchIDEnabled  }">\n\n          <div [ngClass]="{ \'ovel\': touchIDEnabled ,\'ovel-opacity\': !touchIDEnabled  }">\n\n              <img src="assets/imgs/finger.svg" style="width: 30px;">\n\n              <p>Finger</p>\n\n          </div>\n\n        </ion-col>\n\n        <ion-col class="password-sec" (click)="workinProgress()">\n\n          <div class="ovel-opacity">\n\n              <img src="assets/imgs/otp.png" style="width:35px; padding-top:7px">\n\n            <p style="margin-top: 6px;">Otp</p>\n\n          </div>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-card-content>\n\n  </ion-card>\n\n</div>\n\n</ion-content>\n\n\n\n`/*ion-inline-end:"D:\Projects\TFG_Next_Step\workspace\TFGNextStep\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_11__ionic_native_fingerprint_aio__["a" /* FingerprintAIO */], __WEBPACK_IMPORTED_MODULE_12__providers_network_networkcalls__["a" /* NetworkProvider */], __WEBPACK_IMPORTED_MODULE_9__utilities_constants__["a" /* Constants */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Platform */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_fabric__["a" /* Crashlytics */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ToastController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_network__["a" /* Network */], __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_7__utilities_common__["a" /* Utills */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */]])
    ], Login);
    return Login;
}());

//# sourceMappingURL=login.js.map

/***/ })

},[367]);
//# sourceMappingURL=main.js.map